<?php

App::uses('AppModel', 'Model');

/**
 * CompanyDocumentType Model
 *
 * @property CompanyDocument $CompanyDocument
 */
class CompanyDocumentType extends AppModel {

    var $name = 'CompanyDocumentType';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'CompanyDocument' => array(
            'className' => 'CompanyDocument',
            'foreignKey' => 'company_document_type_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
