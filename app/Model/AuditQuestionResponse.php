<?php

App::uses('AppModel', 'Model');

/**
 * AuditQuestionResponse Model
 *
 * @property AuditQuestion $AuditQuestion
 * @property Company $Company
 * @property AuditToQuestion $AuditToQuestion
 * @property CompanyAudit $CompanyAudit
 * @property CompanySupplier $CompanySupplier
 */
class AuditQuestionResponse extends AppModel {

    public $actsAs = array(
        'ChangeLog.Changeable' => array(
            'ignore' => array(
                'audit_question_id',
                'audit_date',
                'company_id',
                'audit_to_question_id',
                'created',
                'modified',
                'consumer_note_modified',
                'secondary_note_modified',
                'tertiary_note_modified',
                'opportunity_note_modified',
                'audit_trail_modified',
                'company_audit_id',
                'company_supplier_id',
                'action_plan_id'
            )
        ),
        'HtmlPurifier.HtmlPurifier' => array(
            'config' => 'SPGAllowedHTML',
            'fields' => array(
                'consumer_note', 'secondary_note', 'tertiary_note', 'opportunity_note', 'audit_trail'
            )
        ),
        'JsonColumn' => array(
            'fields' => array('response_options') /** add the fields you wanna encode here **/
        )
    );


//    public $_schema = array(
//        'secondary_note_modified' => array(
//            'type' => 'integer',
//            'length' => 1,
//            'default' => 0
//        ),
//    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'id' => array(
            'uuid' => array(
                'rule' => array('uuid'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'audit_question_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'company_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'consumer_note_modified' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'secondary_note_modified' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'tertiary_note_modified' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'opportunity_note_modified' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'audit_trail_modified' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed


    /**
     * hasOne associations
     *
     * @var array
     */
    //public $hasOne = array(
    // );


    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'AuditQuestion' => array(
            'className' => 'AuditQuestion',
            'foreignKey' => 'audit_question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AuditToQuestion' => array(
            'className' => 'AuditToQuestion',
            'foreignKey' => 'audit_to_question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CompanyAudit' => array(
            'className' => 'CompanyAudit',
            'foreignKey' => 'company_audit_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'company_supplier_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
       'ActionPlan' => array(
            'className' => 'CompanyAudit',
            'foreignKey' => 'action_plan_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /*
     * create_blank_responses method
     * 
     * This method will be called when a request is sent to a supplier.
     * It will take the supplier_id (user.id) and the audit_id and 
     * create empty responses in the db - (table : audit_question_responses)
     * The supplier will then be able to update these responses
     * 
     * @uses AuditToQuestion::get_ids_of_audit_to_questions
     * 
     * @param int $audit_id The company_id is read from the Session
     * @param int $company_id The company_id is read from the Session
     * @param int $supplier_id The id of the audit
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return boolean
     */

    public function create_blank_responses($audit_id = null, $company_id = null, $supplier_id = null) {
        App::import('model', 'AuditToQuestion');

        /*
         * Get the ids of audit_to_questions that we want to create responses for
         */
 //       $audit_to_questions = $this->AuditToQuestion->get_ids_of_audit_to_questions($audit_id, $company_id);
        $audit_to_questions = $this->AuditToQuestion->getAuditTosForAudit($audit_id);

      //  echo pr($audit_to_questions);
      //  die;


        /*
         * Use the ids from above to build an array with the required relationships between 
         * audit_to_questions table and audit_question_responses table
         * Save the array to audit_question_responses table
         */
        $audit_question_blank_responses = array();

        /*
         * each $audit_to_question will already have audit_to_question_id and audit_question_id
         */
        foreach ($audit_to_questions as $audit_to_question) {
  
            $blank_response = array();

            $blank_response['company_id'] = $company_id;
            $blank_response['company_audit_id'] = $audit_id;
            $blank_response['company_supplier_id'] = $supplier_id;
            $blank_response['audit_to_question_id'] = $audit_to_question['AuditToQuestion']['id'];
            $blank_response['audit_question_id'] = $audit_to_question['AuditToQuestion']['audit_question_id'];
            $blank_response['response_options'] = json_decode($audit_to_question['AuditQuestion']['response_options']);


            //$audit_to_question['company_id'] = $company_id;
            //$audit_to_question['company_audit_id'] = $audit_id;
            //$audit_to_question['company_supplier_id'] = $supplier_id;
//            array_push($audit_question_blank_responses, $audit_to_question);

            array_push($audit_question_blank_responses, $blank_response);
        };

       // echo pr($audit_question_blank_responses);
       // die;

        if ($this->saveMany($audit_question_blank_responses)) {
            return TRUE;
//            if ($supplier_id !== null) {

            /*
             * perform additional action of sending a request to supplier if supplier id is !null
             */
//                $this->send_requests_to_supplier($audit_id, $company_id, $supplier_id);
//            }
        } else {
            $validation_errors = $this->AuditQuestionResponse->validationErrors;

            $this->response->statusCode(400);

            $this->response->type('json');
            $this->response->body(json_encode(array('flash' => $validation_errors)));
            $this->response->send();
            $this->_stop();
        }
    }

    /*
     * create_copied_responses method
     * 
     * If an existing audit is chosen from the dropdown when creating an audit, 
     * then this will copy the responses associated the existing audit to the new audit's responses.     * It will take the supplier_id (user.id) and the audit_id and 
     * create empty responses in the db - (table : audit_question_responses)
     * 
     * @uses AuditToQuestion::get_ids_of_audit_to_questions
     * 
     * @param int $audit_id The company_id is read from the Session
     * @param int $company_id The company_id is read from the Session
     * @param int $supplier_id The id of the audit
     * @access public
     * @version 0.0.1
     * @author Keith Burton Ward <keith.burton@greenstreets.ie>
     * @return boolean
     */

    public function create_copied_responses($audit_template_id, $new_company_audit_id, $company_id, $sourceCompany_id = 1) {
        App::import('model', 'AuditToQuestion');
        $audit_to_questions = $this->AuditToQuestion->get_ids_of_audit_to_questions($new_company_audit_id, $company_id);

        /*
         * Get all company responses to the selected standard_audit_template
         */
        if (!$sourceCompany_id) {
            $sourceCompany_id = 1;
        }

        //refactored to used model method.
        $currentCompanyNames = $this->Company->getTemplateCompanyNames($company_id, $sourceCompany_id);

        $templateCompanyName = $currentCompanyNames['template_trading_name'];  //template name;
        $companyLegalName = $templateCompanyName;
        $companyTradingName = $templateCompanyName;

        if (count($templateCompanyName) > 0) {
            $companyLegalName = (isset($currentCompanyNames['company_legal_name']) ? $currentCompanyNames['company_legal_name'] : $templateCompanyName);
            $companyTradingName = (isset($currentCompanyNames['company_trading_name']) ? $currentCompanyNames['company_trading_name'] : $templateCompanyName);
        }

        $this->recursive = -1;
        $audit_template_company_responses = $this->find('all', array(
            'conditions' => array(
                'company_audit_id' => $audit_template_id,
                'company_id' => $sourceCompany_id, //default Template Company=1
                'company_supplier_id' => ''  //only copy company responses
            )
        ));

        /*
         * get amount of questions used by template
         */
        $template_responses_size = count($audit_template_company_responses);

        $audit_question_copied_responses = array();

        for ($i = 0; $i < $template_responses_size; $i++) {
            $consumer_note = str_replace($templateCompanyName, $companyTradingName, $audit_template_company_responses[$i]['AuditQuestionResponse']['consumer_note']);
            $secondary_note = str_replace($templateCompanyName, $companyTradingName, $audit_template_company_responses[$i]['AuditQuestionResponse']['secondary_note']);
            $tertiary_note = str_replace($templateCompanyName, $companyTradingName, $audit_template_company_responses[$i]['AuditQuestionResponse']['tertiary_note']);
            $opportunity_note = str_replace($templateCompanyName, $companyTradingName, $audit_template_company_responses[$i]['AuditQuestionResponse']['opportunity_note']);
            $audit_trail = str_replace($templateCompanyName, $companyTradingName, $audit_template_company_responses[$i]['AuditQuestionResponse']['audit_trail']);


            //added new function to ensure flag is set for each response type.
            $copied_response['consumer_note_modified'] = $this->_setFlagBasedOnFieldLength($consumer_note);
            $copied_response['secondary_note_modified'] = $this->_setFlagBasedOnFieldLength($secondary_note);
            $copied_response['tertiary_note_modified'] = $this->_setFlagBasedOnFieldLength($tertiary_note);
            $copied_response['opportunity_note_modified'] = $this->_setFlagBasedOnFieldLength($opportunity_note);
            $copied_response['audit_trail_modified'] = $this->_setFlagBasedOnFieldLength($audit_trail);

            $copied_response['company_id'] = $company_id;
            $copied_response['audit_to_question_id'] = $audit_to_questions[$i]['audit_to_question_id'];
            $copied_response['audit_question_id'] = $audit_to_questions[$i]['audit_question_id'];
            $copied_response['consumer_note'] = $consumer_note;
            $copied_response['secondary_note'] = $secondary_note;
            $copied_response['tertiary_note'] = $tertiary_note;
            $copied_response['opportunity_note'] = $opportunity_note;
            $copied_response['audit_trail'] = $audit_trail;
            $copied_response['company_audit_id'] = $new_company_audit_id;

            array_push($audit_question_copied_responses, $copied_response);
        }


        if (!$this->saveMany($audit_question_copied_responses)) {
            $validation_errors = $this->validationErrors;

            return $validation_errors;
        }
    }

    /*
     * _setFlagBasedOnFieldLength
     * 
     * Determines length of passed string and then sets this to 1 if not empty string
     * 

     * 
     * @param string $fieldValue string to determine length of.
     * @access private
     * @version 0.0.1
     * @author Keith Burton Ward <keith.burton@greenstreets.ie>
     * @return int 0 or 1
     */

    private function _setFlagBasedOnFieldLength($fieldValue = '') {

        if (strlen($fieldValue) > 0) {
            return 1;
        } else {
            return 0;
        }
    }


//this method has been refactored to the Company model
/*    private function _getTemplateCompanyName($company_id, $sourceCompany_id) {

        $template_company_names = array();
        if ($sourceCompany_id == 1) {
            $template_company_names['template_legal_name'] = '[TemplateCompanyNameLtd]';
            $template_company_names['template_trading_name'] = '[TemplateCompanyNameLtd]';
        } else {
            App::import('model', 'Company');
            $company = new Company();
            $templateCompany = $company->find('all', array('conditions' => array('id' => $sourceCompany_id),
                'recursive' => -1
            ));

            $template_company_names['template_legal_name'] = $templateCompany[0]['Company']['legal_name'];
            $template_company_names['template_trading_name'] = $templateCompany[0]['Company']['trading_name'];
        }

        if ($company_id > 1) {
            App::import('model', 'Company');
            $company = new Company();
            $templateCompany = $company->find('all', array('conditions' => array('id' => $company_id),
                'recursive' => -1
            ));

            $template_company_names['company_legal_name'] = $templateCompany[0]['Company']['legal_name'];
            $template_company_names['company_trading_name'] = $templateCompany[0]['Company']['trading_name'];
        }
        return $template_company_names;
    }
*/

    public function getResponsesByType($company_id = null, $audit_id = null, $audit_to_question_id = null, $responseType = null, $supplier_only = false) {
        $conditions = array();

        if ($company_id) {
            array_push($conditions, array('AuditQuestionResponse.company_id' => $company_id));
        }
        if ($audit_id) {
            array_push($conditions, array('AuditQuestionResponse.company_audit_id' => $audit_id));
        }
        if ($audit_to_question_id) {
            array_push($conditions, array('AuditQuestionResponse.audit_to_question_id' => $audit_to_question_id));
        }
        if ($supplier_only) {
            array_push($conditions, array('NOT' => array('AuditQuestionResponse.company_supplier_id' => 'Is Null')));
//            array_push($conditions, array('AuditQuestionResponse.company_supplier_id >' => 0));
        }

//        echo pr($conditions);

        $fields = $this->__SupplierResponsePartialFields();


        if($responseType !==null) {        
            switch ($responseType) {
                case 1://secondary
                    array_push($fields, 'AuditQuestionResponse.secondary_note as response_note');
                    array_push($conditions, array('NOT' => array('AuditQuestionResponse.secondary_note' => '')));
                    break;
                case 2://tertiary
                    array_push($fields, 'AuditQuestionResponse.tertiary_note as response_note');
                    array_push($conditions, array('NOT' => array('AuditQuestionResponse.tertiary_note' => '')));
                    break;
                case 3://opportunity
                    array_push($fields, 'AuditQuestionResponse.opportunity_note as response_note');
                    array_push($conditions, array('NOT' => array('AuditQuestionResponse.opportunity_note' => '')));
                    break;
                case 4://audit_trail
                    array_push($fields, 'AuditQuestionResponse.audit_trail as response_note');
                    array_push($conditions, array('NOT' => array('AuditQuestionResponse.audit_trail' => '')));
                    break;
                default: //0=primary, also default
                    array_push($fields, 'AuditQuestionResponse.consumer_note as response_note');
                    array_push($conditions, array('NOT' => array('AuditQuestionResponse.consumer_note' => '')));
                    break;
            }
        }
        $this->recursive = 2;
        $data = $this->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions));

			
        return $data;
    }

    public function getAllSupplierResponses($company_id = null, $audit_id = null, $audit_to_question_id = null) {

        $conditions = array();

        if ($company_id) {
            array_push($conditions, array('AuditQuestionResponse.company_id' => $company_id));
        }
        if ($audit_id) {
            array_push($conditions, array('AuditQuestionResponse.company_audit_id' => $audit_id));
        }
        if ($audit_to_question_id) {
            array_push($conditions, array('AuditQuestionResponse.audit_to_question_id' => $audit_to_question_id));
        }

//        array_push($conditions, array('NOT' => array('AuditQuestionResponse.company_supplier_id' => 'Is Null')));
        array_push($conditions, array('AuditQuestionResponse.company_supplier_id >' => 0));


        $fields = $this->__SupplierResponsePartialFields();

        array_push($fields, 'AuditQuestionResponse.consumer_note as response_primary_note', 'AuditQuestionResponse.secondary_note as response_secondary_note', 'AuditQuestionResponse.tertiary_note as response_tertiary_note', 'AuditQuestionResponse.opportunity_note as response_opportunity_note');

        $data = $this->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions));

        return $data;
    }

    private function __SupplierResponsePartialFields() {
        $fields = $this->__ResponsePartialFields();

        array_push($fields, 'AuditQuestionResponse.company_supplier_id as response_supplier_id', 'AuditQuestionResponse.consumer_note as response_primary_note', 'AuditQuestionResponse.secondary_note as response_secondary_note', 'AuditQuestionResponse.tertiary_note as response_tertiary_note', 'AuditQuestionResponse.opportunity_note as response_opportunity_note', 'AuditQuestionResponse.audit_trail as response_audit_trail', 'User.username as response_supplier_username', 'User.firstname as response_supplier_firstname', 'User.lastname as response_supplier_lastname', 'User.supplier_company as response_supplier_company');

        return $fields;
    }

    private function __ResponsePartialFields() {
        $fields = array(
            'AuditQuestionResponse.id AS response_id',
            'AuditQuestionResponse.created AS response_created',
            'AuditQuestionResponse.modified as response_modified',
            'AuditQuestionResponse.audit_to_question_id as audit_to_question_id',
            'AuditQuestionResponse.company_audit_id as company_audit_id',
            'AuditQuestionResponse.audit_question_id as audit_question_id',
            'AuditQuestion.reference as reference',
            'AuditQuestion.question as question',
            'AuditQuestion.audit_question_category_id as audit_question_category_id',
            'AuditQuestion.audit_question_category_type_id as audit_question_category_type_id',
            'AuditQuestionResponse.consumer_note_modified as response_primary_note_modified',
            'AuditQuestionResponse.consumer_note_empty as response_primary_note_empty',
            'AuditQuestionResponse.secondary_note_modified as response_secondary_note_modified',
            'AuditQuestionResponse.secondary_note_empty as response_secondary_note_empty',
            'AuditQuestionResponse.tertiary_note_modified as response_tertiary_note_modified',
            'AuditQuestionResponse.tertiary_note_empty as response_tertiary_note_empty',
            'AuditQuestionResponse.opportunity_note_modified as response_opportunity_note_modified',
            'AuditQuestionResponse.opportunity_note_empty as response_opportunity_note_empty',
            'AuditQuestionResponse.audit_trail_modified as response_audit_trail_modified',
            'AuditQuestionResponse.audit_trail_empty as response_audit_trail_empty',
            'AuditQuestionResponse.arac_emissions as response_arac_emissions',
            'AuditQuestionResponse.arac_water as response_arac_water',
            'AuditQuestionResponse.arac_waste as response_arac_waste',
            'AuditQuestionResponse.arac_land as response_arac_land',
            'AuditQuestionResponse.arac_resources as response_arac_resources',
            'AuditQuestionResponse.arac_other as response_arac_other',
            'AuditQuestionResponse.arac_potential as response_arac_potential',
            'AuditQuestionResponse.arac_legislation as response_arac_legislation',
            'AuditQuestionResponse.arac_stakeholders as response_arac_stakeholders',
            'AuditQuestionResponse.aris_quantifiable as response_aris_quantifiable',
            'AuditQuestionResponse.aris_committment as response_aris_committment',
            'AuditQuestionResponse.aris_previous as response_aris_previous',
            'AuditQuestionResponse.aris_feasible as response_aris_feasible',
            'AuditQuestionResponse.aris_potential as response_aris_potential',
            'AuditQuestionResponse.aris_significant as response_aris_significant',
            'AuditQuestionResponse.aris_rationale as response_aris_rationale',
            'AuditQuestionResponse.response_options as response_options',
            'AuditQuestionResponse.action_plan_id as action_plan_id'
        );

        return $fields;
    }

    public function getAllCompanyResponses($company_id = null, $audit_id = null) {

        if ($company_id) {
            $conditions = array(
                'AuditQuestionResponse.company_id' => $company_id,
                'AuditQuestionResponse.company_audit_id' => $audit_id,
                'AuditQuestionResponse.company_supplier_id' => Null
                    //'AuditQuestionResponse.audit_question_id <=' => 70
            );
        }

        $fields = $this->__ResponsePartialFields();

        $responses = $this->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'recursive' => 0
        ));

        $cat_ids = Hash::extract($responses, '{n}.AuditQuestion.audit_question_category_id');

        $categories = $this->AuditQuestion->AuditQuestionCategory->find('all', array(
            'recursive' => -1,
            'fields' => array('id', 'name','sort_order'),
            'conditions' => array('AuditQuestionCategory.id IN' => $cat_ids)
        ));


        $a1 = Hash::extract($responses, '{n}.AuditQuestionResponse');
        $a2 = Hash::extract($responses, '{n}.AuditQuestion');

        $responses_f1 = Hash::merge($a1, $a2);
        $categories_f1 = Hash::extract($categories, '{n}.AuditQuestionCategory');

        return $this->category_match($responses_f1, $categories_f1);
    }

    private function category_match($responses, $categories_f1) {
        $responses_f2 = array();
        foreach ($responses as $response) {

            $cat_id  = $response['audit_question_category_id'];
                        
            foreach ($categories_f1 as $cat) {
                if($cat['id'] === $cat_id) {
                    $response['category_name'] = $cat['name'];                    
                    $response['category_sort_order'] = $cat['sort_order'];                    
                }
            }
            array_push($responses_f2, $this->htmlDecodeResponse($response));
        }
        
        return $responses_f2;
    }

    /**
     * __htmlDecodeResponse method
     * 
     * ??? Description ???
     * 
     * @uses htmlDecoder
     * 
     * @param array $data Takes input data and compares it to existing.
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @access private
     * @version 0.0.1
     * @return array The response as an array
     */
    public function htmlDecodeResponse($responseData) {

        if (isset($responseData['response_note'])) {
            $responseData['response_note'] = $this->htmlDecoder($responseData['response_note']);
        }

        if (isset($responseData['response_primary_note'])) {
            $responseData['response_primay_note'] = $this->htmlDecoder($responseData['response_primary_note']);
        }

        if (isset($responseData['response_seondary_note'])) {
            $responseData['response_secondary_note'] = $this->htmlDecoder($responseData['response_secondary_note']);
        }

        if (isset($responseData['response_tertiary_note'])) {
            $responseData['response_tertiary_note'] = $this->htmlDecoder($responseData['response_tertiary_note']);
        }

        if (isset($responseData['response_opportunity_note'])) {
            $responseData['response_opportunity_note'] = $this->htmlDecoder($responseData['response_opportunity_note']);
        }

        if (isset($responseData['response_audit_trail'])) {
            $responseData['response_audit_trail'] = $this->htmlDecoder($responseData['response_audit_trail']);
        }


        if (isset($responseData['response_arac_other'])) {
            $responseData['response_arac_other'] = $this->htmlDecoder($responseData['response_arac_other']);
        }

        if (isset($responseData['response_arac_potential'])) {
            $responseData['response_arac_potential'] = $this->htmlDecoder($responseData['response_arac_potential']);
        }

        if (isset($responseData['response_arac_legislation'])) {
            $responseData['response_arac_legislation'] = $this->htmlDecoder($responseData['response_arac_legislation']);
        }

        if (isset($responseData['response_arac_stakeholders'])) {
            $responseData['response_arac_stakeholders'] = $this->htmlDecoder($responseData['response_arac_stakeholders']);
        }

       if (isset($responseData['response_aris_rationale'])) {
            $responseData['response_aris_rationale'] = $this->htmlDecoder($responseData['response_aris_rationale']);
        }

        return $responseData;
    }

    /**
     * htmlDecoder method
     * 
     * ??? Description ???
     * 
     * @param string $html_string to clean
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @access private
     * @version 0.0.1
     * @return string
     */
    public function htmlDecoder($html_string) {
        //temporary fix to clean out any dodgy formatting
        $cleanString = str_replace(array('&Acirc;&nbsp;', '&Atilde;?', '&Acirc;&cent;', '&Atilde;&cent;'), ' ', $html_string);

        return html_entity_decode($cleanString, ENT_QUOTES, "UTF-8");
    }




    public function findQuestionsWithResponses($company_id=NULL, $audit_id=NULL) {

        $conditions = array(
            'OR' => array(
                array(
                    'NOT' => array(
                        'AuditQuestionResponse.consumer_note' => '',
                    )
                ),
                array(
                    'NOT' => array(
                        'AuditQuestionResponse.secondary_note' => '',
                    )
                ),
                array(
                    'NOT' => array(
                        'AuditQuestionResponse.tertiary_note' => '',
                    )
                ),
                array(
                    'NOT' => array(
                        'AuditQuestionResponse.opportunity_note' => '',
                    )
                )
            ),
            'AND' => array(
                'AuditQuestionResponse.company_id' => $company_id,
                'AuditQuestionResponse.company_audit_id' => $audit_id,
                'NOT' => array(
                    'AuditQuestionResponse.company_supplier_id' => NULL
                )
            )
        );

        $fields = array(
            'DISTINCT (AuditQuestion.id) as audit_question_id',
//                'AuditQuestionResponse.id as audit_question_response_id',
            'AuditToQuestion.id as audit_to_question_id',
            'AuditQuestion.id as audit_question_id',
            'AuditQuestion.question as audit_question',
            'AuditQuestion.audit_question_category_id as audit_question_category_id',
        );


        return $this->find('all', array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive'=>0
        ));



    }


    public function findResponseToQuestion($audit_to_question_id = Null,
                                           $company_id = Null,
                                           $supplier_id = Null){


            $conditions = array(
                'AuditQuestionResponse.company_id' => $company_id,
                'AuditQuestionResponse.audit_to_question_id' => $audit_to_question_id,
                'AuditQuestionResponse.company_supplier_id' => $supplier_id
            );

            $fields = array(
                'AuditQuestionResponse.id AS response_id',
                'AuditQuestionResponse.created AS response_created',
                'AuditQuestionResponse.modified as response_modified',
                'AuditQuestionResponse.consumer_note as response_primary_note',
                'AuditQuestionResponse.consumer_note_modified as response_primary_note_modified',
                'AuditQuestionResponse.secondary_note as response_secondary_note',
                'AuditQuestionResponse.secondary_note_modified as response_secondary_note_modified',
                'AuditQuestionResponse.tertiary_note as response_tertiary_note',
                'AuditQuestionResponse.tertiary_note_modified as response_tertiary_note_modified',
                'AuditQuestionResponse.opportunity_note as response_opportunity_note',
                'AuditQuestionResponse.opportunity_note_modified as response_opportunity_note_modified',
                'AuditQuestionResponse.audit_trail as response_audit_trail',
                'AuditQuestionResponse.audit_trail_modified as response_audit_trail_modified',
                'AuditQuestionResponse.arac_emissions as response_arac_emissions',
                'AuditQuestionResponse.arac_water as response_arac_water',
                'AuditQuestionResponse.arac_waste as response_arac_waste',
                'AuditQuestionResponse.arac_land as response_arac_land',
                'AuditQuestionResponse.arac_resources as response_arac_resources',
                'AuditQuestionResponse.arac_other as response_arac_other',
                'AuditQuestionResponse.arac_potential as response_arac_potential',
                'AuditQuestionResponse.arac_legislation as response_arac_legislation',
                'AuditQuestionResponse.arac_stakeholders as response_arac_stakeholders',
                'AuditQuestionResponse.aris_quantifiable as response_aris_quantifiable',
                'AuditQuestionResponse.aris_committment as response_aris_committment',
                'AuditQuestionResponse.aris_previous as response_aris_previous',
                'AuditQuestionResponse.aris_feasible as response_aris_feasible',
                'AuditQuestionResponse.aris_potential as response_aris_potential',
                'AuditQuestionResponse.aris_significant as response_aris_significant',
                'AuditQuestionResponse.aris_rationale as response_aris_rationale',
                'AuditQuestion.audit_question_category_type_id as audit_question_category_type_id',
                'AuditQuestionResponse.response_options as response_options',
                'AuditQuestionResponse.action_plan_id as action_plan_id'
            );


    
    
            return $this->find('first', array(
                'conditions' => $conditions,
                'fields' => $fields,
                'recursive'=>1
            ));

        

    }



}
