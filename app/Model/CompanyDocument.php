<?php

App::uses('AppModel', 'Model');

/**
 * CompanyDocument Model
 *
 */
class CompanyDocument extends AppModel {

    var $name = 'CompanyDocument';
    var $actsAs = array('Media.Transfer', 'Media.Polymorphic', 'Media.Generator', 'Media.Coupler');
    var $belongsTo = array(
        'CompanyDocumentType' => array(
            'className' => 'CompanyDocumentType',
            'foreignKey' => 'company_document_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CompanyAudit' => array(
            'className' => 'CompanyAudit',
            'foreignKey' => 'audit_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public $hasMany = array(
        'AuditToDocument' => array(
            'className' => 'AuditToDocument',
            'foreignKey' => 'company_document_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'BatchemailToDocument' => array(
            'className' => 'BatchemailToDocument',
            'foreignKey' => 'company_document_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    function transferTo($via, $from) {
        extract($from);
        $irregular = array(
            'image' => 'img',
            'text' => 'txt'
        );
        $name = Mime_Type::guessName($mimeType ? $mimeType : $file);
        if (isset($irregular[$name])) {
            $short = $irregular[$name];
        } else {
            $short = substr($name, 0, 3);
        }
        $path = $short . DS;

        $path .= $this->getCompanyId() . DS;
        $path .= String::uuid();
        $path .=!empty($extension) ? '.' . strtolower($extension) : null;
        return $path;
    }

    private function getCompanyId() {

        if (isset($this->data['CompanyDocument']['company_id'])) {
            return $this->data['CompanyDocument']['company_id'];
        }
        if (isset($this->data['Photo']['company_id'])) {
            return $this->data['Photo']['company_id'];
        }
    }

//    public function beforeDelete($cascade = true) {
//        if (!$cascade) {
//            return true;
//        }
//
//        $result = $this->find('first', array(
//            'conditions' => array($this->primaryKey => $this->id),
//            'fields' => array('dirname', 'basename'),
//            'recursive' => -1
//        ));
//        if (empty($result)) {
//            return false;
//        }
//
//        $pattern = MEDIA_FILTER . "*/";
//        $pattern .= $result[$this->alias]['dirname'] . '/';
//        $pattern .= pathinfo($result[$this->alias]['basename'], PATHINFO_FILENAME);
//
//        $files = glob("{$pattern}.*");
//
//        $name = Mime_Type::guessName($result[$this->alias]['basename']);
//        $versions = array_keys(Configure::read('Media.filter.' . $name));
//
////        if (count($files) > count($versions)) {
////            $message = 'MediaFile::beforeDelete - ';
////            $message .= "Pattern `{$pattern}` matched more than number of versions. ";
////            $message .= "Failing deletion of versions and record for `Media@{$this->id}`.";
////            CakeLog::write('warning', $message);
////            return false;
////        }
//
//        foreach ($files as $file) {
//            $File = new File($file);
//
//            if (!$File->delete()) {
//                return false;
//            }
//        }
//        return true;
//    }

    /**
     *
     * @var array $default_fields The default feilds the each get() will return.
     */
    var $default_fields = array(
        'CompanyDocument.id AS id',
        'CompanyDocument.created AS created',
        'CompanyDocument.modified AS modified',
        'CompanyDocument.display_name AS display_name',
        'CompanyDocument.description AS description',
        'CompanyDocumentType.id AS document_type_id',
        'CompanyDocumentType.name AS document_type_name',
        'CompanyDocument.company_document_type_id AS company_document_type_id'
    );

    /**
     * get_documents method
     * return a list of Documents associated with a company.
     * 
     * @param array $additional_fields additional fields can be added to the find.
     * @param array $additional_conditions additional conditions can be added to the find.
     * 
     * @uses AppModel::beforeFind() Adds company_id to the find conditions
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array documents associated with a Company
     */
    public function get_documents($additional_fields = null, $additional_conditions = null) {
        $fields = $this->default_fields;
        $conditions = array();

        if ($additional_fields) {
            $fields = array_merge($this->default_fields, $additional_fields);
        }

        if ($additional_conditions) {
            $conditions = array_merge($conditions, $additional_conditions);
        }

        $this->recursive = 1;
        $company_documents_result = $this->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions
        ));

        //patch to remove file information which is being added by Media plugin
        $file_info = Hash::remove($company_documents_result, '{n}.CompanyDocument.file');
        $a1 = Hash::extract($file_info, '{n}.CompanyDocument');
        $a2 = Hash::extract($company_documents_result, '{n}.CompanyAudit');
        $a3 = Hash::extract($company_documents_result, '{n}.CompanyDocumentType');

        return Hash::merge($a1, $a2, $a3);
    }

    /**
     * get_document method
     * return an array of data for a single Document.
     * 
     * @param int $document_id The id of the document.
     * @param int $company_id is added to the find() method in appModel::beforeFind()
     * @param array $additional_fields additional fields can be added to the find.
     * @param array $additional_conditions additional conditions can be added to the find.
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object of data for the specified document
     */
    public function get_document($document_id = null, $additional_fields = null, $additional_conditions = null) {
        $fields = $this->default_fields;

        // restrict to only current company documents
        $conditions = array(
            'CompanyDocument.id' => $document_id
        );

        if ($additional_fields) {
            $fields = array_merge($this->default_fields, $additional_fields);
        }

        if ($additional_conditions) {
            $conditions = array_merge($conditions, $additional_conditions);
        }

        $this->recursive = 0;

        $company_document_info_result = $this->find('first', array(
            'fields' => $fields,
            'conditions' => $conditions
        ));

        if ($company_document_info_result) {
            $a1 = $company_document_info_result['CompanyDocument'];
            $a2 = $company_document_info_result['CompanyDocumentType'];
            return Hash::merge($a1, $a2);
        } else {
            return false;
        }
    }

    /**
     * get_file_path method
     * return the path and name of a file
     * 
     * @param int $document_id The id of the document.
     * @param string $size the size of the file, i.e. thumbnail, original etc.
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object of data for the specified document
     */
    public function get_file_path($document_id = null, $size) {
        $this->recursive = -1;

        $file = $this->findById($document_id);
        if ($file) {
            $fileArray[] = $file['CompanyDocument'];
            $basename = $fileArray['0']['basename'];
            $dirname = $fileArray['0']['dirname'];
            $filename = $fileArray['0']['display_name'];

            switch ($size) {
                case 'original';
                    $path = 'Media/transfer/' . $dirname . '/' . $basename;
                    break;
                case 'medium';
                    $path = 'Media/filter/medium/' . $dirname . '/' . $basename;
                    break;
                case 'large';
                    $path = 'Media/filter/large/' . $dirname . '/' . $basename;
                    break;
            }

            $file_path = array(
                'path' => $path,
                'name' => $filename
            );

            return $file_path;
        } else {
            return false;
        }
    }


/**
     * get_file_path_by_name method
     * return the path and name of a file
     * 
     * @param int $document_id The id of the document.
     * @param string $size the size of the file, i.e. thumbnail, original etc.
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object of data for the specified document
     */
    public function get_file_path_by_name($document_display_name, $size) {
        $this->recursive = -1;

        $file = $this->find('first',array('conditions'=>array('CompanyDocument.display_name'=>$document_display_name)));
        if ($file) {
            $fileArray[] = $file['CompanyDocument'];
            $basename = $fileArray['0']['basename'];
            $dirname = $fileArray['0']['dirname'];
            $filename = $fileArray['0']['display_name'];

            switch ($size) {
                case 'original';
                    $path = 'Media/transfer/' . $dirname . '/' . $basename;
                    break;
                case 'medium';
                    $path = 'Media/filter/medium/' . $dirname . '/' . $basename;
                    break;
                case 'large';
                    $path = 'Media/filter/large/' . $dirname . '/' . $basename;
                    break;
            }

            $file_path = array(
                'path' => $path,
                'name' => $filename
            );

            return $file_path;
        } else {
            return false;
        }
    }


    /**
     * delete_audit_documents method
     * 
     * Delete all documents associated with a specified audit
     * 
     * @param int $audit_id The id of the audit
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return boolean
     * 
     * @param int $audit_id
     * @return boolean
     */
    public function delete_audit_documents($audit_id) {
        $company_id = CakeSession::read('User.company_id');

        $conditions = array(
            'CompanyDocument.audit_id' => $audit_id,
            'CompanyDocument.company_id' => $company_id
        );

        if ($this->deleteAll(array($conditions), false, false)) {
            return true;
        } else {
            return false;
        }
    }

}
