<?php

App::uses('AppModel', 'Model');

/**
 * BatchEmail Model
 *
 * @property Company $Company
 * @property BatchemailToSupplier $BatchemailToSupplier
 */
class BatchEmail extends AppModel {

    public function beforeDelete($cascade = true) {

        $this->recursive = -1;

        // sent email cannot be deleted
        $is_draft = $this->find('first', array(
            "conditions" => array(
                'id' => $this->id,
                'email_sent' => null)
        ));

        if (!empty($is_draft)) {
            return true;
        } else {            
            return false;
        }
    }

    /**
     * Add Changeable behaviour.
     * All actions on this model will now be recorded to 
     * changes and change_deltas tables.
     * 
     * @var array  
     */
//    public $actsAs = array('ChangeLog.Changeable');

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'subject' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'message' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'company_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'BatchemailToSupplier' => array(
            'className' => 'BatchemailToSupplier',
            'foreignKey' => 'batch_email_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'BatchemailToDocument' => array(
            'className' => 'BatchemailToDocument',
            'foreignKey' => 'batch_email_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
