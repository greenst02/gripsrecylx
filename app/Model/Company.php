<?php

App::uses('AppModel', 'Model');

/**
 * Company Model
 *
 * @property AustralianState $AustralianState
 * @property IndustryType $IndustryType
 * @property IndustrySector $IndustrySector
 * @property CompanyEnabledStatus $CompanyEnabledStatus
 * @property AuditQuestionResponse $AuditQuestionResponse
 * @property AuditToQuestion $AuditToQuestion
 * @property CompanyAudit $CompanyAudit
 * @property CompanyBrand $CompanyBrand
 * @property CompanyContact $CompanyContact
 * @property CompanyDocument $CompanyDocument
 * @property CompanySite $CompanySite
 * @property Contact $Contact
 * @property Session $Session
 * @property User $User
 */
class Company extends AppModel {


    public $displayField = 'trading_name';


    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'trading_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
//        'legal_name' => array(
//            'notempty' => array(
//                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'address1' => array(
//            'notempty' => array(
//                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'AustralianState' => array(
            'className' => 'AustralianState',
            'foreignKey' => 'australian_state_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'IndustryType' => array(
            'className' => 'IndustryType',
            'foreignKey' => 'industry_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'IndustrySector' => array(
            'className' => 'IndustrySector',
            'foreignKey' => 'industry_sector_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CompanyEnabledStatus' => array(
            'className' => 'CompanyEnabledStatus',
            'foreignKey' => 'company_enabled_status_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'AuditQuestionResponse' => array(
            'className' => 'AuditQuestionResponse',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'AuditToQuestion' => array(
            'className' => 'AuditToQuestion',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CompanyAudit' => array(
            'className' => 'CompanyAudit',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CompanyBrand' => array(
            'className' => 'CompanyBrand',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CompanyContact' => array(
            'className' => 'CompanyContact',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CompanyDocument' => array(
            'className' => 'CompanyDocument',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CompanySite' => array(
            'className' => 'CompanySite',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Session' => array(
            'className' => 'Session',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'company_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );



    public function getTemplateCompanyNames($company_id, $sourceCompany_id) {

        $template_company_names = array();
        if ($sourceCompany_id == 1) {
            $template_company_names['template_legal_name'] = '[TemplateCompanyNameLtd]';
            $template_company_names['template_trading_name'] = '[TemplateCompanyNameLtd]';
        } else {
            $templateCompany = $this->find('all', array('conditions' => array('id' => $sourceCompany_id),
                'recursive' => -1
            ));

            $template_company_names['template_legal_name'] = $templateCompany[0]['Company']['legal_name'];
            $template_company_names['template_trading_name'] = $templateCompany[0]['Company']['trading_name'];
        }

        if ($company_id > 1) {
            $templateCompany = $this->find('all', array('conditions' => array('id' => $company_id),
                'recursive' => -1
            ));

            $template_company_names['company_legal_name'] = $templateCompany[0]['Company']['legal_name'];
            $template_company_names['company_trading_name'] = $templateCompany[0]['Company']['trading_name'];
        }
        return $template_company_names;
    }



}
