<?php

App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
App::uses('CakeEmail', 'Network/Email');

/**
 * User Model
 *
 * @property Group $Group
 * @property Company $Company
 * @property UserEnabledStatus $UserEnabledStatus
 * @property AuditToSupplier $AuditToSupplier
 * @property Session $Session
 */
class User extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'username' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ), 'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Email address already exists.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'group_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
//        'tokenhash' => array(
//            'notempty' => array(
//                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'UserEnabledStatus' => array(
            'className' => 'UserEnabledStatus',
            'foreignKey' => 'user_enabled_status_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'AuditToSupplier' => array(
            'className' => 'AuditToSupplier',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Session' => array(
            'className' => 'Session',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Change' => array(
            'className' => 'Change',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));

    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;

        if (empty($this->data)) {
            $data = $this->read();
        }
        if (!$data['User']['group_id']) {
            return null;
        } else {
            return array('Group' => array('id' => $data['User']['group_id']));
        }
    }

    public function bindNode($user) {
        return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
    }

    /**
     * createTokenParams method
     * 
     * Uses Security compnent to create a token that is stored in the database.
     * 
     * @param array $user
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return string A tokenised key that will be appended to url 
     */
    public function createTokenParams($user) {
        return Security::hash(String::uuid(), 'sha512', true);
    }

    /**
     * public sendEmail method
     *
     * @param string $template_name default is resetpw
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param array $view_variables array of url and message used to populate the view
     * @param string $company_name, 
     * @param string $bcc
     * 
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * 
     * @return void
     */
    public function sendEmail($template_name = 'resetpw', $from = 'spg@greenstreets.ie', $to, $subject, $view_variables, $company_name = NULL, $bcc = NULL) {

        if (empty($view_variables['message'])) {
            $view_variables['message'] = '';
        }

        $Email = new CakeEmail();

        /* SMTP Options */
        $Email->config(array(
            'port' => '25',
            'timeout' => '30',
            'host' => 'smtp.greenstreets.ie',
            'username' => 'greenst29',
            'password' => 'NanVoip5',
            'transport' => 'Smtp'
        ));

        $Email->template($template_name);
        $Email->from($from);
        $Email->to($to);
        $Email->bcc($bcc);
        $Email->subject($subject);
//        $Email->message($message);

        $Email->emailFormat('both');
        $Email->viewVars(array(
            'url' => $view_variables['url'],
            'company_name' => $company_name,
            'message' => $view_variables['message']
        ));

        $Email->send();
    }
	
	
	/**
     * public setEmailError method
     *
	 * set a user account email_error flag
     *
     * @param string $emailAddress email address of the user to update
     * @param string $error  - boolean flag to set to true/false
     * 
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * 
     * @return void
     */
    public function setEmailError($emailAddress,$error) {
		
        $conditions = array('User.username'=>$emailAddress
                                 );

        $this->recursive = -1;
        $user = $this->find('first',array(
            'conditions'=>$conditions
            ));
		
		if($user) {
			$user->saveField('email_error',$error);
			return true;
		}
		else {
			return false;
		}

	}

}
