<?php

App::uses('AppModel', 'Model');

/**
 * BatchemailToSupplier Model
 *
 * @property BatchEmail $BatchEmail
 * @property User $User
 * @property Company $Company
 */
class BatchemailToSupplier extends AppModel {

    var $name = 'BatchemailToSupplier';

    /**
     * Add Changeable behaviour.
     * All actions on this model will now be recorded to 
     * changes and change_deltas tables.
     * 
     * @var array  
     */
    public $actsAs = array(
        'ChangeLog.Changeable' => array(
            'ignore' => array(
                'batch_email_id',
                'user_id',
                'created',
                'company_id'
            )
        )
    );

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'batchemail_to_supplier';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'batch_email_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'company_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'BatchEmail' => array(
            'className' => 'BatchEmail',
            'foreignKey' => 'batch_email_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * batch_email_to_supplier_exists method
     * 
     * called by BatchEmailsController::modify_batchemail_to_suppliers.
     * Checks if a relationship exists and retruns true or false
     * 
     * @param int $batch_email_id The id of the document to check associations against.
     * @param int $supplier_id The id of the audit to check associations against.
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return boolean
     */
    public function batch_email_to_supplier_exists($batch_email_id, $supplier_id) {
        $this->recursive = -1;
        $conditions = array(
            'user_id' => $supplier_id,
            'batch_email_id' => $batch_email_id
        );

        $result = $this->find('first', array(
            'conditions' => $conditions,
            'fields' => 'id'
        ));

        if ($result) {
            $id = $result['BatchemailToSupplier']['id'];
        }

        return ($result) ? $id : false;
    }

    /**
     * save_many method
     * 
     * called by BatchEmailsController::modify_batchemail_to_suppliers
     * creates relationships in BatchemailToSupplier
     * 
     * @see BatchEmailsController::modify_batchemail_to_suppliers
     * 
     * @param array $create_many an array containing batch_email_id, user_id(s) and the company_id
     * @access private
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     */
    public function save_many($create_many) {
        return $this->saveMany($create_many);
    }

    /**
     * delete_many method
     * 
     * called by BatchEmailsController::modify_batchemail_to_suppliers
     * deletes relationships in BatchemailToSupplier
     * 
     * @see BatchEmailsController::modify_batchemail_to_suppliers
     * @uses BatchemailToSupplier::delete
     * 
     * @param array $delete_many an array containing document_id(s), audit(s) and the company_id
     * @access private
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     */
    public function delete_many($delete_many) {
        foreach ($delete_many as $id) {
            $this->delete($id);
        }
    }


    /**
     * existsEmailAndTracker method
     * returns true if a match found for email address and tracker code passed.
     * 
     * 
     * @param string $emailAddress The email address of user for request we're looking for
     * @param string $trackerCode The tracker code used to track the request email
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return boolean true if record exists
     */

    public function existsEmailAndTracker($emailAddress='',$trackerCode='') {

        $conditions = array('User.username'=>$emailAddress,
                            'BatchemailToSupplier.tracker'=>$trackerCode
                                 );

        return (boolean) $this->findByEmailAndTracker($emailAddress,$trackerCode);


    }


 
    /**
     * findByEmailAndTracker method
     * returns record array if a match found for email address and tracker code passed.
     * 
     * 
     * @param string $emailAddress The email address of user for request we're looking for
     * @param string $trackerCode The tracker code used to track the request email
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return array
     */

    public function findByEmailAndTracker($emailAddress='',$trackerCode='',$recursive=1) {

		
        $conditions = array('User.username'=>$emailAddress,
                            'BatchemailToSupplier.tracker'=>$trackerCode    
                                 );


        //$this->recursive = $recursive;
        $this->recursive = 1;

        return $this->find('first',array(
            'conditions'=>$conditions
            ));


    }


    /**
     * updateByEmailAndTracker method
     * returns if update successful.
     * 
     * 
     * @param string $emailAddress The email address of user for request we're looking for
     * @param string $trackerCode The tracker code used to track the request email
     * @param array $data  Array of fields and values to update
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return array
     */


    public function updateByEmailAndTracker($emailAddress='',$trackerCode='',$data=array()) {


       $emailRecord = $this->findByEmailAndTracker($emailAddress,$trackerCode,-1); 
       if($emailRecord){


           $data['id'] = $emailRecord['BatchemailToSupplier']['id'];

//           if ((isset($data['reply_udate'])) && 
//              ($data['reply_udate']<$emailRecord['BatchemailToSupplier']['reply_udate'])){

                // current response is less than existing response so ignore.
//                return true;
//           }




            if (isset($data['reply_udate'])){

                if ($emailRecord['BatchemailToSupplier']['reply_udate']!==null) {

                    //If emailrecord reply date > current mailbox email reply date then no change.
                    if ($emailRecord['BatchemailToSupplier']['reply_udate'] > $data['reply_udate']) {

                        return true;

                    }


                }


            }

            if(count($data)>1) {
                return (boolean) $this->save($data);

           } else
            {
                return true;
            }

       } else {


            return false;
       }


    }
    
    /**
     * getStatus method
     * returns the database status field if found, otherwise empty string.
     * 
     * 
     * @param string $emailAddress The email address of user for request we're looking for
     * @param string $trackerCode The tracker code used to track the request email
     * @param string $checkStatus The status to check against current database record
     * @param string status from database (empty if not found)
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return array
     */


    public function getStatus($emailAddress='',$trackerCode='') {


       $emailRecord = $this->findByEmailAndTracker($emailAddress,$trackerCode); 

       if($emailRecord){
            return $emailRecord['BatchemailToSupplier']['status'];
       } else {

            return '';
       }


    }

}
