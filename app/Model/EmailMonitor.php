<?php

App::uses('AppModel', 'Model');
App::uses('CakeEmail', 'Network/Email');
App::uses('BatchEmail', 'Model');
App::uses('User', 'Model');
App::uses('BatchemailToSupplier', 'Model');

require_once(APP.DS.'Vendor'.DS.'BounceHandler'.DS.'bounce_driver.class.php');
require_once(APP.'Vendor'.DS.'ImapClient'.DS.'Imap.php');

/**
 * EmailMonitor Model
 *
 * @property 
 * @property 
 */
class EmailMonitor extends AppModel {

    public $useTable = false; // This model does not use a database table
    public $hostname = "connect.mailwall.com:143";
    public $username = "greenst32";
    public $password = "GSsupport2014#!";

    /**
     * Connect to imap and download last 24 hours of emails.
     * search for returned emails and save the intended recipient as a bad email address
     * 
     * @return type
     */
	 
	public $foldersToProcess =array('01Submitted/02PossibleQuery','01Submitted/02PossibleQuery');
	
	public $statusData = array('folderName'=>'','runStartDate'=>null,'runEndDate'=>null,'Results'=>array());
	
	
	private function _getEml($imap,$i)
	{
		
			$header = imap_header($imap, $i);

			if (isset($header->from[0]->personal))
			{
			$personal = $header->from[0]->personal;
			}
			else
			{
			$personal = $header->from[0]->mailbox;
			}

			$eml = "date : $header->Date \n";
			$eml .= "from : $personal
			[0]->host}> \n";
			$eml .= "subject : $header->Subject \n";
			$eml .= "to : $personal
			[0]->host}> \n";

			$eml .= imap_body($imap, $i, FT_PEEK)."\n";

			return $eml;
	}

	public function saveValidatedAttachments($cron=true) {
		
		$imap = new Imap($this->hostname, $this->username, $this->password, '');

		if($cron) 
			{$LINEBREAK="\n";}			
		else
		{$LINEBREAK="<br />";}
			
		// stop on error
		if($imap->isConnected()===false)
			die($imap->getError());

		// get all folders as array of strings
		$imap->selectFolder('01Submitted');
		$folders = $imap->getFolders();
		$imap->setTrackingTokens(array('Vendor Code:','Request #:','Email:','Step:'));
		foreach($folders as $folder) {

			//echo $folder."\n";
			$currFolderArray = explode('/',$folder);
			$currentRootFolder = $currFolderArray[0];
			if (($currentRootFolder ==='02Validation')) {
				//echo "$LINEBREAK Folder: ".$folder."$LINEBREAK";
				//echo "---------------------------------------------------------$LINEBREAK";
				$imap->selectFolder($folder);
				$numMsgs = $imap->countMessages();
				if ($numMsgs) {
				//	$currentStatus = array_pop($currFolderArray);
					$currentStatus = $folder;
					
					if (strpos($currentStatus,'02Validation/10Validated') !==false) {
						echo "$LINEBREAK Here....$folder";
						$emails = $imap->getMessages(false);
						$batchemailtosupplier_model = new BatchemailToSupplier();
						foreach($emails as $email){

						$from = $email['from'];
							if (isset($email['Email:']) && (strlen($email['Email:']))) {
								
								$from  =$email['Email:'];
							}
							
							$tracker_code = (isset($email['Request #:']) ? $email['Request #:'] : '');

							
							if (isset($email['attachments'])) {

								$idx=0;
								foreach ($email['attachments'] as $attachment) {
									
									$filename = str_replace(",","",$attachment['name']);
									if(strpos($filename,'.xls')) {
										//echo "$LINEBREAK $from :: $tracker_code :: $filename";										
										$batchEmailDetails = $batchemailtosupplier_model->findByEmailAndTracker(trim($from),trim($tracker_code));
										if(!$batchEmailDetails['BatchemailToSupplier']['attachment_downloaded']) {
											//echo pr($batchEmailDetails);
											$filename = '[VID_'.str_replace(" / ","_",$batchEmailDetails['User']['vendor_code']).']_[R_'.$tracker_code.']_'.$filename;
											//$filename = TMP.str_replace("/",DS,$folder).DS.str_replace(" ","_",$filename);											
											$filename = APP.'emailer_files'.DS.str_replace("/",DS,$folder).DS.str_replace(" ","_",$filename);
											//echo pr($email);
											echo $LINEBREAK.$filename;
											$attachmentFile = $imap->getAttachment($email['uid'],$idx);
											
						
											//echo "$LINEBREAK $filename";
											//echo "$LINEBREAK".escapeshellarg($filename);
											 $fp = fopen($filename, "w+");
											 fwrite($fp, $attachmentFile['content']);
											 fclose($fp);
											 //file_put_contents(escapeshellarg($filename),$attachmentFile['content']);
											 $data = array('attachment_downloaded' => 1);
											$save=$batchemailtosupplier_model->updateByEmailAndTracker($from, $tracker_code, $data);
										}
									}
									$idx++;

								}
								
							}

							
							
							/*	$from = $email['from'];
								if (isset($email['Email:']) && (strlen($email['Email:']))) {
									
									$from  =$email['Email:'];
								}
								
								$tracker_code = (isset($email['Request #:']) ? $email['Request #:'] : '');

								//echo "$LINEBREAKEmail:$from Request #: $tracker_code";
								//echo pr($email);
								//echo "$LINEBREAK";

								$dbStatus = $batchemailtosupplier_model->getStatus($from,$tracker_code);

								if ($dbStatus===$currentStatus){

									if($cron) {
										echo "$LINEBREAK$currentStatus,$from,$tracker_code,Success,No change in $currentStatus";
									}

								}
								else
								{
									if (($batchemailtosupplier_model->existsEmailAndTracker($from,$tracker_code)) ){

										//email found so update the status of the email (i.e. it's current folder)
										//how do we account for multiple email messages (queries from supplier?)
										//that maybe in different folder (TODO)


										$data = array('status'=>$currentStatus,
													  'reply_udate'=>$email['udate']);

										//only track message id from PreValidation on.
										//if ($currentStatus==='05PreValidation')
											$data['reply_uid']=$email['message_id'];									


										if ($batchemailtosupplier_model->updateByEmailAndTracker($from,$tracker_code,$data)){

											//update successful
											if($cron){
												echo "$LINEBREAK$currentStatus,$from,$tracker_code,Success,Updated to $currentStatus";
												}
											if($currentRootFolder==="04NoLongerValid"){
												
												$result = $batchemailtosupplier_model->setEmailError($from,true);
												if (!result) {
													
													echo "$LINEBREAK$currentStatus,$from,$tracker_code,Warning,Could not be set to Email_Error. Please review.";
													
												}
												
											}
											
										} else
										{

											//update failed
											echo "$LINEBREAK$currentStatus,$from,$tracker_code,Failure,Could not be updated to $currentStatus";
										}
									
									} else
									 {

										echo "$LINEBREAK$currentStatus,$from,$tracker_code,Failure,could not be found. CurrentStatus: $currentStatus";

									 }
								}
						//		print_r($email);					
						 */
						} // end foreach
					}
					//echo "$LINEBREAKFolder:".$currentStatus." Number Messages:".$numMsgs."$LINEBREAK";

				}
			}
		}
	}
	
	public function check_folders($cron=true) {
		
		$imap = new Imap($this->hostname, $this->username, $this->password, '');

		if($cron) 
			{$LINEBREAK="\n";}			
		else
		{$LINEBREAK="<br />";}
			
		// stop on error
		if($imap->isConnected()===false)
			die($imap->getError());

		// get all folders as array of strings
		$imap->selectFolder('01Submitted');
		$folders = $imap->getFolders();
		$imap->setTrackingTokens(array('Vendor Code:','Request #:','Email:','Step:'));
		foreach($folders as $folder) {

			//echo $folder."\n";
			$currFolderArray = explode('/',$folder);
			$currentRootFolder = $currFolderArray[0];
			if (($currentRootFolder === '01Submitted') || ($currentRootFolder ==='02Validation')) {
				//echo "$LINEBREAK Folder: ".$folder."$LINEBREAK";
				//echo "---------------------------------------------------------$LINEBREAK";
				$imap->selectFolder($folder);
				$numMsgs = $imap->countMessages();
				if ($numMsgs) {
				//	$currentStatus = array_pop($currFolderArray);
					$currentStatus = $folder;
				//	if ($currentStatus!=='00UnProcessed') {
					if ($currentStatus!=='01Submitted/00UnProcessed') {
						$emails = $imap->getMessages(false);
						$batchemailtosupplier_model = new BatchemailToSupplier();
						foreach($emails as $email){

								$from = $email['from'];
								if (isset($email['Email:']) && (strlen($email['Email:']))) {
									
									$from  =$email['Email:'];
								}
								
								$tracker_code = (isset($email['Request #:']) ? $email['Request #:'] : '');

								//echo "$LINEBREAKEmail:$from Request #: $tracker_code";
								//echo pr($email);
								//echo "$LINEBREAK";

								$dbStatus = $batchemailtosupplier_model->getStatus($from,$tracker_code);

								if ($dbStatus===$currentStatus){

									if($cron) {
										echo "$LINEBREAK$currentStatus,$from,$tracker_code,Success,No change in $currentStatus";
									}

								}
								else
								{
									if (($batchemailtosupplier_model->existsEmailAndTracker($from,$tracker_code)) ){

										//email found so update the status of the email (i.e. it's current folder)
										//how do we account for multiple email messages (queries from supplier?)
										//that maybe in different folder (TODO)


										$data = array('status'=>$currentStatus,
													  'reply_udate'=>$email['udate']);

										//only track message id from PreValidation on.
										//if ($currentStatus==='05PreValidation')
											$data['reply_uid']=$email['message_id'];									


										if ($batchemailtosupplier_model->updateByEmailAndTracker($from,$tracker_code,$data)){

											//update successful
											if($cron){
												echo "$LINEBREAK$currentStatus,$from,$tracker_code,Success,Updated to $currentStatus";
												}
											if($currentRootFolder==="04NoLongerValid"){
												
												$result = $batchemailtosupplier_model->setEmailError($from,true);
												if (!result) {
													
													echo "$LINEBREAK$currentStatus,$from,$tracker_code,Warning,Could not be set to Email_Error. Please review.";
													
												}
												
											}
											
										} else
										{

											//update failed
											echo "$LINEBREAK$currentStatus,$from,$tracker_code,Failure,Could not be updated to $currentStatus";
										}
									
									} else
									 {

										echo "$LINEBREAK$currentStatus,$from,$tracker_code,Failure,could not be found. CurrentStatus: $currentStatus";

									 }
								}
						//		print_r($email);					
						}
					}
					//echo "$LINEBREAKFolder:".$currentStatus." Number Messages:".$numMsgs."$LINEBREAK";

				}
			}
		}
	}
	
	
	private function checkEmailStatus($reply_uid,$folderName){
		
		
		
	}
	
	

	
	
	
	public function check_bounces(){
	
		$inbox = "{" . $this->hostname . "}INBOX";

		$runData = $this->statusData;
		
		$runData['folderName']="INBOX";
		$runData['runStartDate']= date('m/d/Y h:i:s a', time());
		
        $imap = imap_open($inbox, $this->username, $this->password) or die('cannot connect: ' . imap_last_error());

        /** grab emails for the last 24 hours */
//        $date = date("d M Y", strToTime("-10 day"));
//		$emails = imap_search($imap, "SINCE \"$date\"");

		$MC = imap_check($imap);

		$maxNumMsg = ($MC->Nmsgs>5) ? 5 : $MC->Nmsgs;
		
		
		$emails = imap_fetch_overview($imap, "1:{$maxNumMsg}"); /** first 5 messages */
		$numMsgs = count($emails);

		$runData['Results'][]=array('NumberMessagesFound:'=>$numMsgs);
		
		$batchemailtosupplier_model = new BatchemailToSupplier();
		
		foreach ($emails as $email) {
			
			
			$header = imap_fetchheader($imap, $email->msgno);
			$source = $header ."\n". imap_body($imap, $email->msgno);
			$bh = new Bouncehandler();

			// a perl regular expression to find a web beacon in the email body
			$bh->web_beacon_preg_1 = "/\[Vendor Code: ([0-9a-fA-F| ]{1,40})\]/";  //find a max of 40 characters for the vendor code XXXXXX | YYYYYY etc
			$bh->web_beacon_preg_2 = "/\[Request #: ([0-9a-fA-F]{8})\]/";  //8 characters for request #
			// find a web beacon in an X-header field (in the head section)
			$bh->x_header_search_1 = "X-GSERBETSID";

			$bounceinfo = $bh->get_the_facts($source);

			if ($bh->type === 'bounce') {
				print "\nMessage: ".$email->msgno." = ".$bh->type."\n";
				if (strlen($bh->x_header_beacon_1)>0) {
					$tracker_code = $bh->x_header_beacon_1;
				} else
				{
					$tracker_code = $bh->web_beacon_2;					
				}

				if (($batchemailtosupplier_model->existsEmailAndTracker($bounceinfo[0]['recipient'],$tracker_code))) {
					
					$data = array('status'=>'01Bounce');
					
					if ($batchemailtosupplier_model->updateByEmailAndTracker($bounceinfo[0]['recipient'],$tracker_code,$data)){	
						
						if ($batchemailtosupplier_model->setEmailError($bounceinfo[0]['recipient'],true)) {
							imap_mail_move($imap, $email->uid, "01Submitted/01Bounce", CP_UID);
						}
						
					}
				}	
				print "\nTracker: ".$tracker_code;
				print "\nRecipient: ".$bounceinfo[0]['recipient'];
				print "\n===========================================\n";
				
			}
			 
			
			
/*			print "\nMessage: ".$email->msgno." = ".$bh->type."\n";
			print "\n===========================================\n";
			echo pr($bounceinfo);
			print "\n===========================================\n";
			print "Header Beacon1: ". $bh->x_header_beacon_1."\n";
			print "Web Beacon 1: ". $bh->web_beacon_1."\n";
			print "Web Beacon 2: ". $bh->web_beacon_2."\n";
			print "\n===========================================\n";
*/				
			if ($bh->type == 'fbl') {
				print "ENV FROM  ". @$bh->fbl_hash['Original-mail-from']. "\n";
				print "AGENT     ". @$bh->fbl_hash['User-agent']. "\n";
				print "IP        ". @$bh->fbl_hash['Source-ip']. "\n";
			} 
			
		}	
		imap_expunge($imap);
		imap_errors();
        imap_alerts();

        /* close the connection */
        imap_close($imap);
		$runData['runEndDate']= date('m/d/Y h:i:s a', time());
		print_r($runData);
        return count($numMsgs);
		
	} 
	
	private function _getEmailDetails($mbox,$email) {
		
		echo '\nEmail Details\n';

		$hText = imap_fetchbody($mbox, $uid, '0', FT_UID);
		$headers = imap_rfc822_parse_headers($hText); 

		$header = imap_fetchheader($mbox, $email->uid,FT_UID);
		$fromInfo = $header->from[0];
		$replyInfo = $header->reply_to[0];

		$details = array(
			"fromAddr" => (isset($fromInfo->mailbox) && isset($fromInfo->host)) ? $fromInfo->mailbox . "@" . $fromInfo->host : "",
			"fromName" => (isset($fromInfo->personal)) ? $fromInfo->personal : "",
			"replyAddr" => (isset($replyInfo->mailbox) && isset($replyInfo->host)) ? $replyInfo->mailbox . "@" . $replyInfo->host : "",
			"replyName" => (isset($replyTo->personal)) ? $replyto->personal : "",
			"subject" => (isset($header->subject)) ? $header->subject : "",
			"udate" => (isset($header->udate)) ? $header->udate : "",
			"UID" => imap_uid($imap, $email_number),
			'vendor_code' => trim($this->_get_string_between($details['subject'], "[Vendor Code:", "]")),
			'tracking_code' => trim($this->_get_string_between($details['subject'], "[Request #:", "]")),	
		);

	//	echo pr($details);
		return $details;
	}
	 
    public function check_emails() {
        /** connect to imap */
        $inbox = "{" . $this->hostname . "}INBOX";

        $imap = imap_open($inbox, $this->username, $this->password) or die('cannot connect: ' . imap_last_error());

        /** grab emails for the last 24 hours */
        $date = date("d M Y", strToTime("-10 day"));
        $emails = imap_search($imap, "SINCE \"$date\"");
//        $emails = imap_search($inbox, "ALL");      

        $undelivered = array();
        $has_attachments = array();
        $out_of_office_emails = array();
        $remaining_emails = array();

		
        /* if emails are returned, cycle through each one */
        if ($emails) {
            /** put the newest emails on top */
            rsort($emails);

			$numMsgsProcessed = 0;
            /** for each email */
            foreach ($emails as $email_number) {
				
				if($numMsgsProcessed<=50) {
					$numMsgsProcessed++;
					//echo $numMsgsProcessed.'<br />';
					$header = imap_header($imap, $email_number);
					$fromInfo = $header->from[0];
					$replyInfo = $header->reply_to[0];

					//echo pr($header);
					
					$details = array(
						"fromAddr" => (isset($fromInfo->mailbox) && isset($fromInfo->host)) ? $fromInfo->mailbox . "@" . $fromInfo->host : "",
						"fromName" => (isset($fromInfo->personal)) ? $fromInfo->personal : "",
						"replyAddr" => (isset($replyInfo->mailbox) && isset($replyInfo->host)) ? $replyInfo->mailbox . "@" . $replyInfo->host : "",
						"replyName" => (isset($replyTo->personal)) ? $replyto->personal : "",
						"subject" => (isset($header->subject)) ? $this->decodeMimeString($header->subject) : "",
						"udate" => (isset($header->udate)) ? $header->udate : "",
						"UID" => imap_uid($imap, $email_number)
					);

					/**
					 * if undelivered email - vendor_code and tracking_code cannot be take from subject
					 * else extract vendor_code and tracking_code from subject
					 */
					if (stripos($details['subject'], 'Returned mail: see transcript for details') !== false) {
						array_push($undelivered, $details['UID']);
					} elseif (stripos($details['subject'], 'Undeliverable:') !== false) {
						array_push($undelivered, $details['UID']);
						
					} 
						elseif (stripos($details['subject'], 'Delivery unsuccessful:') !== false) {
						array_push($undelivered, $details['UID']);
						
					} 
				else {
                    /* $vendor_id_start = strpos($details['subject'], '[VID:');
                      $substring = substr($details['subject'], $vendor_id_start + 5);
                      $vendor_id_end = strpos($substring, '[VID:');
                      $details['vendor_code'] = substr($substring, 0, $vendor_id_end);
                      $tracking_code_start = strpos($substring, 'BID:[');
                      $details['tracking_code'] = substr($substring, $tracking_code_start + 5, 32); */

                    $details['vendor_code'] = trim($this->_get_string_between($details['subject'], "[Vendor Code:", "]"));
                    $details['tracking_code'] = trim($this->_get_string_between($details['subject'], "[Request #:", "]"));
					$details['user_email'] = trim($this->_get_string_between($details['subject'], "[Email:", "]"));
					$details['step'] = trim($this->_get_string_between($details['subject'], "[Step:", "]"));
					
					//echo pr($details);
					
					
                    if (stripos($details['subject'], 'Out of Office') !== false) {
                        array_push($out_of_office_emails, $details);
                    } 
					elseif (stripos($details['subject'], 'Auto') !== false) {
						array_push($out_of_office_emails, $details);}
					else {
                        if (isset(imap_fetchstructure($imap, $email_number)->parts)) {
                            $details['parts'] = imap_fetchstructure($imap, $email_number)->parts;
                            if ($this->_has_attachments($details['parts'])) {
                                array_push($has_attachments, $details);
                            } else {
                                array_push($remaining_emails, $details);
                            }
                        }
                    }
                }
				} //if email<25
			}  //foreach
        }

        if (!empty($undelivered)) {
            $this->_proccess_undelivered_emails($imap, $undelivered);
        }

        if (!empty($out_of_office_emails)) {
            $this->_proccess_out_of_office($imap, $out_of_office_emails);
        }

  /*      if (!empty($has_attachments) || !empty($remaining_emails)) {
            $emails = array_merge($has_attachments, $remaining_emails);
            $this->_check_email_addresses($emails);
        }
*/
        if (!empty($has_attachments)) {
            $this->_proccess_emails_with_attachments($imap, $has_attachments);
            $this->_proccess_remaining_emails($imap, $has_attachments);
        }

        if (!empty($remaining_emails)) {
            $this->_proccess_remaining_emails($imap, $remaining_emails);
       }

//		echo pr($has_attachments);
//		echo pr ($remaining_emails);

        imap_errors();
        imap_alerts();

        /* close the connection */
        imap_close($imap);

        return count($emails);
    }

	

//return supported encodings in lowercase.
function mb_list_lowerencodings() { $r=mb_list_encodings();
  for ($n=sizeOf($r); $n--; ) { $r[$n]=strtolower($r[$n]); } return $r;
}

//  Receive a string with a mail header and returns it
// decoded to a specified charset.
// If the charset specified into a piece of text from header
// isn't supported by "mb", the "fallbackCharset" will be
// used to try to decode it.
function decodeMimeString($mimeStr, $inputCharset='utf-8', $targetCharset='utf-8', $fallbackCharset='iso-8859-1') {
  $encodings=$this->mb_list_lowerencodings();
  $inputCharset=strtolower($inputCharset);
  $targetCharset=strtolower($targetCharset);
  $fallbackCharset=strtolower($fallbackCharset);

  $decodedStr='';
  $mimeStrs=imap_mime_header_decode($mimeStr);
  for ($n=sizeOf($mimeStrs), $i=0; $i<$n; $i++) {
    $mimeStr=$mimeStrs[$i];
    $mimeStr->charset=strtolower($mimeStr->charset);
    if (($mimeStr == 'default' && $inputCharset == $targetCharset)
      || $mimeStr->charset == $targetCharset) {
      $decodedStr.=$mimeStr->text;
    } else {
      $decodedStr.=mb_convert_encoding(
        $mimeStr->text, $targetCharset,
        (in_array($mimeStr->charset, $encodings) ?
          $mimeStr->charset : $fallbackCharset)
      );
    }
  } return $decodedStr;
}
	
	
	
	
    /* $fullstring = "this is my [tag]dog[/tag]";
      $parsed = get_string_between($fullstring, "[tag]", "[/tag]");
      echo $parsed; // (result = dog)
     */

    private function _get_string_between($string, $start, $end) {
        $string = " " . $string;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return "";
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    private function _has_attachments($parts) {
        $has_attachments = FALSE;

        foreach ($parts as $key => $part) {
            if (isset($part->parts)) {
                $subparts = $part->parts;

                foreach ($subparts as $subpart) {
                    if ($subpart->ifdparameters === 1) {
                        $has_attachments = TRUE;
                        break;
                    }
                }
            }
        }

        return $has_attachments;
    }

    private function _proccess_emails_with_attachments($imap, $emails) {
        foreach ($emails as $email) {
            if ($this->_update_batch_email_to_supplier_status($email, 'Attachment Recieved')) {
                $email_uid = $email['UID'];
                imap_mail_move($imap, $email_uid, "01Submitted/05PreValidation", CP_UID);
                imap_expunge($imap);
            }
        }

        return;
    }

    private function _proccess_out_of_office($imap, $emails) {
        foreach ($emails as $email) {
            if ($this->_update_batch_email_to_supplier_status($email, 'Out Of Office')) {
                $email_uid = $email['UID'];
                imap_mail_move($imap, $email_uid, "01Submitted/03OutOfOffice", CP_UID);
                imap_expunge($imap);
            }
        }

        return;
    }

    private function _proccess_remaining_emails($imap, $emails) {
        foreach ($emails as $email) {

			$tracking_code = $email['tracking_code'];
			$from = $email['fromAddr'];				
			//override with email passed in subject line (if there)
			if (isset($email['user_email'])){
				if (strlen($email['user_email'])) {
					$from = $email['user_email'];
				}
			}				
		
            if ($this->_update_batch_email_to_supplier_status($email, 'PossibleQuery')) {
                $email_uid = $email['UID'];
                imap_mail_move($imap, $email_uid, "01Submitted/04PossibleQuery", CP_UID);
                imap_expunge($imap);
				echo "\nInbox,$from,$tracking_code,Success,Moved to 01Submitted/04PossibleQuery";
				
            } else {
	
				echo "\nInbox,$from,$tracking_code,Failure,Could not be process in Inbox";
				
			}
		
        }

        return;
    }

    /**
     * 
     * @param type $emails
     * @return type
     * 
     * This will take the 'fromAddr' from recieved emails, find that user and check is they 
     * were on original list of recipients.
     */
    private function _check_email_addresses($emails) {
        $user_model = new User();
        $user_model->recursive = -1;

        foreach ($emails as $email) {
            $username = $email['fromAddr'];

            $user = $user_model->find('first', array(
                'conditions' => array(
                    'username' => $username
                ),
                'fields' => array('id')
            ));

            if (empty($user)) {
                $new_user = array();
                $new_user['username'] = trim($username);
                $new_user['firstname'] = 'New';
                $new_user['lastname'] = 'Email';
                $new_user['group_id'] = 5;
                $new_user['company_id'] = 61; // B&Q
                $new_user['supplier_company'] = 'Unknown';

                $token = $user_model->createTokenParams($new_user);
                $new_user['password'] = substr($token, 1, 40);

                $user_model->create();
                $user_model->save($new_user);

                $user_id = $user_model->id;
            } else {
                $user_id = $user['User']['id'];
            }

            $this->_check_if_user_was_recipient($user_id, $email);
        }

        return;
    }

    private function _check_if_user_was_recipient($user_id, $email) {
//        $batchemail_model = new BatchEmail();
        $batchemailtosupplier_model = new BatchemailToSupplier();

//        $batchemail_model->recursive = -1;
        $batchemailtosupplier_model->recursive = -1;

 /*       $batch_email = $batchemail_model->find('first', array(
            'conditions' => array(
                'tracking_code' => $email['tracking_code']
            ),
            'fields' => array('id')
        ));

        $batch_email_id = $batch_email['BatchEmail']['id'];
*/
        $batchemailtosupplier = $batchemailtosupplier_model->find('first', array(
            'conditions' => array(
                'tracker' => $email['tracking_code'],
                'user_id' => $user_id
            )
        ));

        if (empty($batchemailtosupplier)) {

			//original user not found.  See if tracking_code is attached to another user
			// if so copy this new email to this user id and set the old user tracking_code to null?
			
			$batchemailtosupplier = $batchemailtosupplier_model->find('first', array(
				'conditions' => array(
					'tracker' => $email['tracking_code']
				)
			));
			
			if (!empty($batchemailtosupplier)) {
				
				//$batchemailtosupplier_model->saveField('tracker',$email['tracking_code'])
		
				$data = array(
					'batch_email_id' => $batchemailtosupplier['BatchemailToSupplier']['batch_email_id'],
					'user_id' => $user_id,
					'company_id' => 61,
					'status' => 'New Supplier Added'
				);

				$batchemailtosupplier_model->create();
				$batchemailtosupplier_model->save($data);
				
			}
        }

        return;
    }

    private function _proccess_undelivered_emails($imap, $email_uids) {
       
	
	   $user_model = new User();
        $user_model->recursive = -1;

        foreach ($email_uids as $uid) {
            /* get information specific to this email */
            $email['overview'] = imap_fetch_overview($imap, $uid, FT_UID);
            $email['body'] = imap_body($imap, $uid, FT_UID);
            /**
             * get all failed to deliver messages
             */
			 
			 
			 //'vendor_code' => trim($this->_get_string_between($details['subject'], "[Vendor Code:", "]")),
			//'tracking_code' => trim($this->_get_string_between($details['subject'], "[Request #:", "]")),	
			 
			 
            $tracking_code_start = strpos($email['body'], 'X-GSERBETSID: ') + 14;
            $tracking_code = substr($email['body'], $tracking_code_start, 10);


			
			
            $start = strpos($email['body'], 'Final-Recipient: RFC822;') + 25;
            $end = strpos($email['body'], 'Action: failed');
            $bad_email_address = substr($email['body'], $start, $end - $start);
		
            $returned_email = array(
                'fromAddr' => trim($bad_email_address),
                'tracking_code' => trim($tracking_code),
                'UID' => $uid
            );

            if ($this->_update_batch_email_to_supplier_status($returned_email, 'Bounced')) {
                imap_mail_move($imap, $uid, "01Submitted/01Bounce", CP_UID);
                imap_expunge($imap);

                $user_model->id = $user_model->find('first', array(
                    'conditions' => array(
                        'User.username' => $returned_email['fromAddr']
                    ),
                    'fields' => array('User.id')
                ));

                $user_model->saveField('email_error', 1);
            }
        }

        return;
    }

    private function _update_batch_email_to_supplier_status($email, $status) {
 
		$user_model = new User();
        $batchemailtosupplier_model = new BatchemailToSupplier();

        $user_model->recursive = -1;

        $tracking_code = $email['tracking_code'];
        $email_address = $email['fromAddr'];
		
		
		//override with email passed in subject line (if there)
		if (isset($email['user_email'])){
			if (strlen($email['user_email'])) {
				$email_address = $email['user_email'];
			}
		}

		
		


		
		
		if ($batchemailtosupplier_model->existsEmailAndTracker($email_address,$tracking_code)) {
			
				$data = array(
					'status' => $status,
					'reply_uid' => $email['UID']
				);
			
			 return (boolean) $batchemailtosupplier_model->updateByEmailAndTracker($email_address,$tracking_code,$data);
			
		} else{
			
			if($status !== "Bounced") {

		
				$batchemail_to_supplier = $batchemailtosupplier_model->find('first', array(
				'conditions' => array(
					'BatchemailToSupplier.tracker' => $tracking_code
				),
					'fields' => array('BatchemailToSupplier.id,BatchemailToSupplier.batch_email_id','BatchemailToSupplier.user_id')
				));
				
				//echo pr($batchemail_to_supplier);
				
				if(isset($batchemail_to_supplier['BatchemailToSupplier']['user_id'])) {
					if ($this->createUserFromCopy($batchemail_to_supplier['BatchemailToSupplier']['user_id'],$email_address)) {
						
						echo "\nInbox,$email_address,$tracking_code,Update,New User Created in Inbox";
						
						$user = $user_model->find('first', array(
							'conditions' => array(
								'User.username' => $email_address
							),
							'fields' => array('User.id')
						));

						if (isset($user['User']['id'])) {
							$user_id = $user['User']['id'];										
						
							$data = array(
							'tracker' => $tracking_code,
							'batch_email_id' => $batchemail_to_supplier['BatchemailToSupplier']['batch_email_id'],
							'user_id' => $user_id,
							'status' => 'New Contact',
							'reply_uid' => $email['UID']);

						if ($batchemailtosupplier_model->save($data)) {
							
								$batchemail_to_supplier = $batchemailtosupplier_model->find('first', array(
								'conditions' => array(
									'BatchemailToSupplier.tracker' => $tracking_code,
									'User.id' => $user_id
								),
									'fields' => array('BatchemailToSupplier.id,BatchemailToSupplier.batch_email_id','BatchemailToSupplier.user_id')
								));
											
								$data = array(
								'id' => $batchemail_to_supplier['BatchemailToSupplier']['id'],
								'tracker' => $tracking_code,
								'batch_email_id' => $batchemail_to_supplier['BatchemailToSupplier']['batch_email_id'],
								'user_id' => $user_id,
								'status' => $status,
								'reply_uid' => $email['UID']);

								if ($batchemailtosupplier_model->save($data)) {
									
									echo "\nInbox,$email_address,$tracking_code,Update,New Tracker Record Created in Inbox";
									return true;
									
								}
						
							}
						}
				
					}	
				}
			}
		}		
		return false;
		
    }

	
private function createUserFromCopy($copy_from_user_id,$new_user_email) {
	
			$user_model = new User();
			$user_model->recursive=-1;
			$existing_user = $user_model->find('first', array(
            'conditions' => array(
                'User.id' => $copy_from_user_id
            )
			));
	
			if(isset($existing_user['User']['id'])){
	              $new_user = array();
                $new_user['username'] = trim($new_user_email);
                $new_user['firstname'] = 'New';
                $new_user['lastname'] = 'Email';
                $new_user['group_id'] = $existing_user['User']['group_id'];
                $new_user['company_id'] = $existing_user['User']['company_id'];
                $new_user['supplier_company'] = $existing_user['User']['supplier_company'];
                $new_user['vendor_code'] = $existing_user['User']['vendor_code'];

                $token = $user_model->createTokenParams($new_user);
                $new_user['password'] = substr($token, 1, 40);

                $user_model->create();
                return $user_model->save($new_user);
					
					
					
				}
				
				
			 else
				return false;
	
	
}	

}