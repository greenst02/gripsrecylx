<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
//    public function beforeSave($options = array()) {
//        if (isset($this->data[$this->alias]['password'])) {
//            $passwordHasher = new SimplePasswordHasher();
//            $this->data[$this->alias]['password'] = $passwordHasher->hash(
//                    $this->data[$this->alias]['password']
//            );
//        }
//        return true;
//    }

    /*
     * afterSave function is called after each individual save in a saveAll execution, so you could do
     * See http://stackoverflow.com/questions/9769543/retrieving-inserted-ids-from-saveall-in-cakephp
     */
//    var $inserted_ids = array();
//
//    function afterSave($created) {
//        if ($created) {
//            $this->inserted_ids[] = $this->getInsertID();
//        }
//        return true;
//    }

    /**
     * Transform a set of hasMany multi-select data into a format which can be saved
     * using saveAll in the controller
     * 
     * http://jedistirfry.co.uk/blog/2013-08/how-to-create-hasmanythrough-multi-selects/
     * 
     * @param array $data
     * @param str $fieldToSave
     * @param int $deleteId
     * @return array
     */
//    public function massageHasManyForSaveAll($data, $fieldToSave, $deleteId = null) {
//        foreach ($this->belongsTo as $model => $relationship) {
//            if ($relationship['foreignKey'] != $fieldToSave) {
//                $relatedModel = $model;
//                $relatedModelPrimaryKey = $this->{$model}->primaryKey;
//                $relatedForeignKey = $relationship['foreignKey'];
//            }
//        }
//
//        if ($deleteId !== null) {
//            $this->deleteAll(array(
//                $this->alias . '.' . $relatedForeignKey => $deleteId
//            ));
//        }
//
//        if (is_array($data[$fieldToSave])) {
//            foreach ($data[$fieldToSave] as $packageId) {
//                $return[] = array($fieldToSave => $packageId);
//            }
//
//            return $return;
//        }
//
//        return $data;
//    }

    /**
     * beforeFind method
     * Adds the company_id to the conditions array of all find methods where the model contains a company_id field.
     * This excludes the models that are not associated with a company, i.e. IndustrySector, IndustryType
     * The User, CompanyAudit and AuditQuestionResponse models have also been excluded, these models have company_id applied as needed.
     * 
     * @access public
     * @param array $queryData
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return array $queryData
     */
    public function beforeFind($queryData) {
        $company_id = CakeSession::read('User.company_id');

        if ($this->name === 'User') {
            return $queryData;
        }
        if (!empty($company_id) && ($this->name === 'CompanyAudit' || $this->name === 'AuditQuestionResponse')) {
            return $queryData;
        }
        if (!empty($company_id)) {
            //add company_id to conditions if field exists
            if ($this->hasField('company_id')) {
                $queryData['conditions'][$this->name . '.company_id'] = $company_id;
                return $queryData;
            }
        }
    }

    /**
     * beforeDelete method
     * 
     * Checks if the specified item's company_id matches with the current user's company_id in the active session
     * i.e. isOwner check.
     * 
     * Delete is not yet implemented for Users / Suppliers or Companies
     * More checks will be required if the above is to be implemented
     * 
     * @access public
     * @param object Model
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return boolean
     */
     public function beforeDelete($cascade = true) {
        $company_id = CakeSession::read('User.company_id');
        $group_id = CakeSession::read('User.group_id');
        $this->recursive = -1;


        if($this->name !== 'Permission') {
        
            if (!empty($company_id)) {
                //add company_id to conditions if field exists
                $owner_of_item = $this->find("first", array(
                    "conditions" => array("company_id" => $company_id)
                ));
                if ($owner_of_item) {
                    return true;
                } else {
                    return false;
                }
            }
        
        }
        /*
         * managers and admins can delete users and suppliers
         * 
         * users can delete suppliers
         */
//        if (!empty($company_id) && $this->name === 'User') {
//            if ($group_id < 4) {
//                $is_company_supplier = $this->find('first', array(
//                    'conditions' => array(
//                        'id' => $this->id,
//                        'company_id' => $company_id,
//                        'group_id > ' => 3
//                    )
//                ));
//                
//                if ($is_company_supplier) {
////                    echo 'TRUE';
////                    pr($is_company_supplier);
//                    return true;                    
//                } else {
////                    echo 'FALSE';
//                    return false;
//                }
//            }
//        }
    }

//    public function beforeSave($options = array()) {
//        if (!$this->data[$this->alias] === 'Company' && empty($this->data[$this->alias]['company_id'])) {
//            $this->data[$this->alias]['company_id'] = CakeSession::read('User.company_id');
//            return true;
//        }
//    }
}
