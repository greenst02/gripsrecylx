<?php
App::uses('AppModel', 'Model');
/**
 * BrandActivityType Model
 *
 * @property CompanyBrand $CompanyBrand
 */
class BrandActivityType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CompanyBrand' => array(
			'className' => 'CompanyBrand',
			'foreignKey' => 'brand_activity_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
