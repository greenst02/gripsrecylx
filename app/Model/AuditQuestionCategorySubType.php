<?php
App::uses('AppModel', 'Model');
/**
 * AuditQuestionCategorySubType Model
 *
 * @property AuditQuestionCategoryType $AuditQuestionCategoryType
 * @property AuditQuestionCategory $AuditQuestionCategory
 */
class AuditQuestionCategorySubType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'AuditQuestionCategoryType' => array(
			'className' => 'AuditQuestionCategoryType',
			'foreignKey' => 'audit_question_category_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'AuditQuestionCategory' => array(
			'className' => 'AuditQuestionCategory',
			'foreignKey' => 'audit_question_category_sub_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
