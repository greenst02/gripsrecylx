<?php

App::uses('AppModel', 'Model');

/**
 * AuditQuestion Model
 *
 * @property AuditQuestionCategory $AuditQuestionCategory
 * @property AuditToQuestion $AuditToQuestion
 */
class AuditQuestion extends AppModel {


    public $actsAs = array(
        'JsonColumn' => array(
            'fields' => array('response_options') /** add the fields you wanna encode here **/
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'audit_question_category_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'AuditQuestionCategory' => array(
            'className' => 'AuditQuestionCategory',
            'foreignKey' => 'audit_question_category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AuditQuestionCategoryType' => array(
            'className' => 'AuditQuestionCategoryType',
            'foreignKey' => 'audit_question_category_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'AuditToQuestion' => array(
            'className' => 'AuditToQuestion',
            'foreignKey' => 'audit_question_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

//    public function findIDsByQuestionCategoryType($question_category_type_id) {
//        $fields = array(
//            'AuditQuestion.id as audit_question_id'
//        );
//        $conditions = array(
//            'AuditQuestion.audit_question_category_type_id' => 1
//        );
////        $this->AuditQuestion->recursive = -1;
//
//        $audit_question_ids = $this->find('all', array(
//            'fields' => $fields,
//            'conditions' => $conditions
//                )
//        );
//        
//        return $audit_question_ids;
//    }

    /**
     * getQuestionsForType method
     * returns questions for the audit id provided
     *
     * @param int $audit_question_category_type_id the id of audit to retrieve questions for (default=1).
     * 
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return cakephp array of category
     */
    public function getQuestionsForType($audit_question_category_type_id = 1) {

        $this->recursive = -1;

        /**
         * AuditToQuestion.id as id - This is used to populate responses section on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
         * audit_question_id is used by navigation on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
         */
        $fields = array(
            'AuditQuestion.id as id',
            'AuditQuestion.reference as reference',
            'AuditQuestion.question as question',
            'AuditQuestion.audit_question_category_id as audit_question_category_id',
            'AuditQuestion.response_options as response_options'
        );


        $conditions = array(
            'AuditQuestion.audit_question_category_type_id' => $audit_question_category_type_id
        );

       
        return $this->find('all', array('fields' => $fields, 'conditions' => $conditions));
    }

    /**
     * getSupplierQuestionsForType method
     * returns only supplier (=1) questions for the audit type id provided
     *
     * @param int $audit_question_category_type_id the id of audit to retrieve questions for.
     * 
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return cakephp array of category
     */
    public function getSupplierQuestionsForType($audit_question_category_type_id) {

        $this->recursive = -1;

        /**
         * AuditToQuestion.id as id - This is used to populate responses section on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
         * audit_question_id is used by navigation on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
         */
        $fields = array(
            'AuditQuestion.id as id',
            'AuditQuestion.question as question',
            'AuditQuestion.audit_question_category_id as audit_question_category_id',
        );


        $conditions = array(
            'AuditQuestion.audit_question_category_type_id' => $audit_question_category_type_id,
            'AuditQuestion.supplier' => 1
        );


        return $this->find('all', array('fields' => $fields, 'conditions' => $conditions));
    }

}
