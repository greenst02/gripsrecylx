<?php

App::uses('AppModel', 'Model');

/**
 * BatchEmail Model
 *
 * @property Company $Company
 * @property VendorRequest $VendorRequest
 */
class VendorRequest extends AppModel {


    /**
     * Add Changeable behaviour.
     * All actions on this model will now be recorded to 
     * changes and change_deltas tables.
     * 
     * @var array  
     */
//    public $actsAs = array('ChangeLog.Changeable');

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(    
        'company_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'vendor_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Vendor' => array(
            'className' => 'Vendor',
            'foreignKey' => 'vendor_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
/*    public $hasMany = array(
        'VendorRequest' => array(
            'className' => 'VendorRequest',
            'foreignKey' => 'vendor_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
*/

}
