<?php

App::uses('AppModel', 'Model');

/**
 * ChangeDelta Model
 *
 * @property Change $Change
 */
class ChangeDelta extends AppModel {
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Change' => array(
            'className' => 'Change',
            'foreignKey' => 'change_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
