<?php

App::uses('AppModel', 'Model');

/**
 * CompanyAudit Model
 *
 * @property Company $Company
 * @property AuditQuestionCategoryType $AuditQuestionCategoryType
 * @property AuditEnabledStatus $AuditEnabledStatus
 * @property AuditQuestionResponse $AuditQuestionResponse
 * @property AuditToQuestion $AuditToQuestion
 * @property AuditToSupplier $AuditToSupplier
 */
class CompanyAudit extends AppModel {

    /**
     * Add Changeable behaviour.
     * All actions on this model will now be recorded to 
     * changes and change_deltas tables.
     * 
     * @var array  
     */
    public $actsAs = array('ChangeLog.Changeable');

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'audit_date' => array(
            'date' => array(
                'rule' => array('date'),
                'message' => 'Must be of date format : yyyy-mm-dd',
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'spg_audit_code' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
//            'maxlength' => array(
////                'rule' => array('maxlength'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
        ),
        'company_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
//        'lead_auditor_name' => array(
//            'notempty' => array(
//                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
        'audit_enabled_status_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
//        'summary' => array(
//            'maxlength' => array(
//                'rule' => array('maxlength'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'contact_phone' => array(
//            'notempty' => array(
////                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'contact_email' => array(
//            'notempty' => array(
////                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'lead_auditor_phone' => array(
//            'notempty' => array(
////                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'lead_auditor_email' => array(
//            'notempty' => array(
////                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'review_auditor_name' => array(
//            'notempty' => array(
////                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'review_auditor_phone' => array(
//            'notempty' => array(
////                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
//        'review_auditor_email' => array(
//            'notempty' => array(
////                'rule' => array('notempty'),
//            //'message' => 'Your custom message here',
//            //'allowEmpty' => false,
//            //'required' => false,
//            //'last' => false, // Stop validation after this rule
//            //'on' => 'create', // Limit validation to 'create' or 'update' operations
//            ),
//        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AuditEnabledStatus' => array(
            'className' => 'AuditEnabledStatus',
            'foreignKey' => 'audit_enabled_status_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'AuditToQuestion' => array(
            'className' => 'AuditToQuestion',
            'foreignKey' => 'company_audit_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'AuditQuestionResponse' => array(
            'className' => 'AuditQuestionResponse',
            'foreignKey' => 'company_audit_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'AuditToSupplier' => array(
            'className' => 'AuditToSupplier',
            'foreignKey' => 'company_audit_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CompanyDocument' => array(
            'className' => 'CompanyDocument',
            'foreignKey' => 'audit_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'AuditToDocument' => array(
            'className' => 'AuditToDocument',
            'foreignKey' => 'company_audit_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    
    /**
     * create_audit_to_questions method
     * 
     * Used by CompanyAuditsController::add
     * 
     * Takes audit_id and company_id, creates relationship of audit_to_questions
     * 
     * 1. Get Audit Questions - only id's are needed using AuditToQuestion Model
     * 2. Use the id of the new audit to build an array with the required relationships between the new audit and audit_questions
     * 3. Save the array to audit_to_qustions table using AuditToQuestion Model
     * 
     * @uses AuditQuestion
     * @uses AuditToQuestion
     * 
     * @param int $audit_id The id of new audit
     * @param int $company_id is read from session and appended to above array before new audit is saved
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
      *@edit    2015-02-01<br />
     *          Keith Burton <keith.burton@greenstreets.ie><br />
     *          Added new parameter to allow for different category types. default to 1 for compatability<br/>
     * @return boolean 
     */
    public function create_audit_to_questions($audit_id, $company_id,$audit_question_category_type_id=1) {
        App::import('Model', 'AuditQuestion');
        $AuditQuestionModel = new AuditQuestion();

        App::import('Model', 'AuditToQuestion');
        $AuditToQuestionModel = new AuditToQuestion();

        /*
         * Get Audit Questions - only id's are needed
         */
        $fields = array(
            'AuditQuestion.id as audit_question_id'
        );
        $conditions = array(
            // refactored to use parameter passed in. default is 1
            'AuditQuestion.audit_question_category_type_id' => $audit_question_category_type_id
        );
        $AuditQuestionModel->recursive = -1;

        $audit_question_ids = $AuditQuestionModel->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions
                )
        );

        $audit_question_ids = Hash::extract($audit_question_ids, '{n}.AuditQuestion');

        /*
         * Use the id of the new audit to build an array with the required relationships between 
         * the new audit and audit_questions
         * Save the array to audit_to_qustions table
         */
        $audit_to_questions = array();
        foreach ($audit_question_ids as $audit_question) {
            $audit_question['company_audit_id'] = $audit_id;
            $audit_question['company_id'] = $company_id;
            array_push($audit_to_questions, $audit_question);
        };

        if ($AuditToQuestionModel->saveMany($audit_to_questions)) {
            return TRUE;
        } else {
            return $AuditToQuestionModel->validationErrors;
        }
    }

}
