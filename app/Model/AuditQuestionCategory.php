<?php

App::uses('AppModel', 'Model');

/**
 * AuditQuestionCategory Model
 *
 * @property AuditQuestionCategoryType $AuditQuestionCategoryType
 * @property AuditQuestionCategorySubType $AuditQuestionCategorySubType
 * @property AuditQuestion $AuditQuestion
 */
class AuditQuestionCategory extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'AuditQuestionCategoryType' => array(
            'className' => 'AuditQuestionCategoryType',
            'foreignKey' => 'audit_question_category_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
		//,
      //  'AuditQuestionCategorySubType' => array(
      //      'className' => 'AuditQuestionCategorySubType',
      //      'foreignKey' => 'audit_question_category_sub_type_id',
      //      'conditions' => '',
      //      'fields' => '',
      //      'order' => ''
      //  )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'AuditQuestion' => array(
            'className' => 'AuditQuestion',
            'foreignKey' => 'audit_question_category_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     * getCategoriesForType method
     * returns a categories for type id provided
     *
     * @param int $audit_category_type_id the type of audit category to retrieve.
     * 
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return cakephp array of category
     */

public function getCategoriesForType($audit_question_category_type_id) {

    $this->recursive = -1;
	$fields = array(
            'id',
            'name',
            'sort_order'
        );
    $conditions = array(
				'audit_question_category_type_id'=>$audit_question_category_type_id
    					);

    return $this->find('all',array('fields'=>$fields,'conditions'=>$conditions));


}

    /**
     * getSupplierCategoriesForType method
     * returns only supplier categories (where supplier=1) for type id provided
     *
     * @param int $audit_category_type_id the type of audit category to retrieve.
     * 
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return cakephp array of category
     */

public function getSupplierCategoriesForType($audit_question_category_type_id) {

    $this->recursive = -1;
	
	$fields = array(
            'id',
            'name',
            'sort_order'
        );

    $conditions = array(
    				'audit_question_category_type_id'=>$audit_question_category_type_id,
    				'supplier'=>1
    				);

    return $this->find('all',array('fields'=>$fields,'conditions'=>$conditions));


}


}
