<?php

App::uses('AppModel', 'Model');

/**
 * AuditQuestionCategoryType Model
 *
 * @property AuditQuestionCategory $AuditQuestionCategory
 * @property AuditQuestionCategorySubType $AuditQuestionCategorySubType
 */
class AuditQuestionCategoryType extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'AuditQuestion' => array(
            'className' => 'AuditQuestion',
            'foreignKey' => 'audit_question_category_type_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'AuditQuestionCategory' => array(
            'className' => 'AuditQuestionCategory',
            'foreignKey' => 'audit_question_category_type_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
            //,
            //'AuditQuestionCategorySubType' => array(
            //    'className' => 'AuditQuestionCategorySubType',
            //    'foreignKey' => 'audit_question_category_type_id',
            //    'dependent' => false,
            //    'conditions' => '',
            //    'fields' => '',
            //    'order' => '',
            //    'limit' => '',
            //    'offset' => '',
            //    'exclusive' => '',
            //    'finderQuery' => '',
            //    'counterQuery' => ''
            //)
    );
    //category type can below to one company
    // or if null all companies
    public $belongsTo = array(
        'CustomCompany' => array(
            'className' => 'Company',
            'foreignKey' => false,
            'conditions' => array('CustomCompany.id' => 'AuditQuestionCategoryType.custom_company_id'))
    );

    /**
     * getType method
     * returns a single record based on an id provided
     *
     * 
     * 
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return cakephp array of category types
     */
    public function getType($id) {

        $this->recursive = -1;
        return $this->findById($id);
    }

    /**
     * getAllTypes method
     * returns all category types
     *
     * 
     * 
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return cakephp array of category types
     */
    public function getAllTypes() {

        $this->recursive = -1;
        return $this->find('all');
    }

    /**
     * getTypesForCompany method
     * returns all category types for a particular company id
     * this includes custom (custom_company_id=$company_id) and public (custom_company_id is null)
     * 
     * 
     * @param int $company_id The company for the types we are looking for in addition to public types
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return cakephp array of category types
     */
    public function getTypesForCompany($company_id) {

        //to do include condition for public types (company_id is null)
        $conditions = array('OR' => array(
                'AuditQuestionCategoryType.custom_company_id' => $company_id,
                'AuditQuestionCategoryType.custom_company_id' => 'Is Null'
        ));

        $this->recursive = -1;
        return $this->find('all', array(
                    'conditions' => $conditions
        ));
    }

}
