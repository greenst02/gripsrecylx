<?php

App::uses('AppModel', 'Model');

/**
 * AuditToDocument Model
 *
 * @property CompanyAudit $CompanyAudit
 * @property CompanyDocument $CompanyDocument
 * @property Company $Company
 */
class AuditToDocument extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'audit_to_document';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'company_audit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'company_document_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'company_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'CompanyAudit' => array(
            'className' => 'CompanyAudit',
            'foreignKey' => 'company_audit_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CompanyDocument' => array(
            'className' => 'CompanyDocument',
            'foreignKey' => 'company_document_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
