<?php

App::uses('AppModel', 'Model');

/**
 * AuditToQuestion Model
 *
 * @property CompanyAudit $CompanyAudit
 * @property AuditQuestion $AuditQuestion
 * @property Company $Company
 * @property AuditQuestionResponse $AuditQuestionResponse
 */
class AuditToQuestion extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'audit_to_question';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'company_audit_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'CompanyAudit' => array(
            'className' => 'CompanyAudit',
            'foreignKey' => 'company_audit_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AuditQuestion' => array(
            'className' => 'AuditQuestion',
            'foreignKey' => 'audit_question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Company' => array(
            'className' => 'Company',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'AuditQuestionResponse' => array(
            'className' => 'AuditQuestionResponse',
            'foreignKey' => 'audit_to_question_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /*
     * get_ids_of_audit_to_questions method
     * 
     * This method called by    AuditQuestionResponses->create_blank_responses()
     *                          AuditQuestionResponses->create_copied_responses()
     * 
     * return array of $audit_to_question_ids
     */

    public function get_ids_of_audit_to_questions($audit_id) {
        /*
         * Get the ids of audit_to_questions that we want to create responses for
         */
        $fields = array(
            'AuditToQuestion.id as audit_to_question_id',
            'AuditToQuestion.audit_question_id as audit_question_id'
        );

        $conditions = array(
            'AuditToQuestion.company_audit_id' => $audit_id
        );

        $this->recursive = -1;

        $audit_to_question_ids = $this->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions
                )
        );

        return Hash::extract($audit_to_question_ids, '{n}.AuditToQuestion');
    }

    /**
     * getAuditTosForAudit method
     * returns questions for the audit id provided
     *
     * @param int $audit_id the id of audit to retrieve questions for.
     * 
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return cakephp array of category
     */
    public function getAuditTosForAudit($audit_id) {

        $this->recursive = 0;

        /**
         * AuditToQuestion.id as id - This is used to populate responces section on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
         * audit_question_id is used by navigation on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
         */
        $fields = array(
            'AuditToQuestion.id  as id', // NB: used to navigate through questions.
            'AuditToQuestion.company_audit_id AS company_audit_id',
            'AuditToQuestion.audit_question_id AS audit_question_id',
            'AuditQuestion.reference AS reference',
            'AuditQuestion.question AS question',
            'AuditQuestion.audit_question_category_id AS audit_question_category_id',
            'AuditQuestion.audit_question_category_type_id AS audit_question_category_type_id',
            'AuditQuestion.supplier AS supplier_question',
            'AuditQuestion.response_options as response_options'
        );


        $conditions = array(
            'AuditToQuestion.company_audit_id' => $audit_id
        );

        return $this->find('all', array('fields' => $fields, 'conditions' => $conditions));
    }

    /**
     * getSupplierAuditTosForAudit method
     * returns only supplier (=1) questions for the audit id provided
     *
     * @param int $audit_id the id of audit to retrieve questions for.
     * 
     * @access public
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return cakephp array of category
     */
    public function getSupplierAuditTosForAudit($audit_id) {

        $this->recursive = 0;

        /**
         * AuditToQuestion.id as id - This is used to populate responces section on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
         * audit_question_id is used by navigation on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
         */
        $fields = array(
            'AuditToQuestion.id  as id', // NB: used to navigate through questions.
            'AuditToQuestion.company_audit_id AS company_audit_id',
            'AuditToQuestion.audit_question_id AS audit_question_id',
            'AuditQuestion.question AS question',
            'AuditQuestion.audit_question_category_id AS audit_question_category_id',
            'AuditQuestion.supplier AS supplier_question',
        );


        $conditions = array(
            'AuditToQuestion.company_audit_id' => $audit_id,
            'AuditQuestion.supplier' => 1
        );

        return $this->find('all', array('fields' => $fields, 'conditions' => $conditions));
    }

}
