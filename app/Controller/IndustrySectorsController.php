<?php

App::uses('AppController', 'Controller');

/**
 * IndustrySectors Controller
 *
 * @property IndustrySector $IndustrySector
 */
class IndustrySectorsController extends AppController {

    var $name = 'IndustrySectors';

    /**
     * index method
     * return a list of Industry Sectors
     * - is accessed from the companyDetails form -> the APC Classification Section.
     * 
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON array listing all Industry Sectors
     */
    public function index() {
        $this->IndustrySector->recursive = -1;
        $industry_sectors_list = $this->IndustrySector->find('all');
        $industry_sectors = Hash::extract($industry_sectors_list, '{n}.IndustrySector');
        $this->respondAsJSON(STATUS_CODE_OK, $industry_sectors);
    }
}
