<?php

App::uses('AppController', 'Controller');

/**
 * CompanyContacts Controller
 *
 * @property CompanyContact $CompanyContact
 */
class CompanyContactsController extends AppController {

    var $name = 'CompanyContacts';

    /**
     * index method
     * The company_id is checked in the active session using AppModel::beforeFind()
     * The Contacts owned by that company are returned
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all Company Contacts
     */
    public function index() {
        $fields = array(
            'CompanyContact.id AS id',
            'CompanyContact.title AS title',
            'CompanyContact.first_name AS first_name',
            'CompanyContact.last_name AS last_name',
            'CompanyContact.phone AS phone',
            'CompanyContact.email AS email',
            'CompanyContact.australian_state_id AS australian_state_id',
            'AustralianState.name AS state_name'
        );

        $this->CompanyContact->recursive = 1;

        $company_contacts_result = $this->CompanyContact->find('all', array(
            'fields' => $fields
        ));

        $a1 = Hash::extract($company_contacts_result, '{n}.CompanyContact');
        $a2 = Hash::extract($company_contacts_result, '{n}.AustralianState');

        $company_contacts = Hash::merge($a1, $a2);

        $this->respondAsJSON(STATUS_CODE_OK, $company_contacts);
    }

    /**
     * view method
     * 
     * Finds the contact by id and also checks the session company_id
     * to ensure the contact is only accessible to owners
     * 
     * @param int $id The id of the Company Contact
     * @param int $company_id is added to the find() method in appModel::beforeFind()
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return object json object.
     */
    public function view($id = null) {
        $this->CompanyContact->recursive = -1;
        $company_contact = $this->CompanyContact->findById($id);
        if ($company_contact) {
            $this->respondAsJSON(STATUS_CODE_OK, $company_contact['CompanyContact']);
        } else {
            throw new NotFoundException(__('Contact Not Found'));
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $new_company_contact = $this->request->data;

            $new_company_contact['company_id'] = CakeSession::read('User.company_id');

            $this->CompanyContact->create();
            if ($this->CompanyContact->save($new_company_contact)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'New Contact Added'));
            } else {
                $errors = $this->validationErrorsToString($this->CompanyContact->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

    /**
     * edit method
     * 
     * Finds the contact by id and also checks the session company_id
     * to ensure the contact is only accessible to owners.
     * 
     * After above check is complete, updates existing contact with new data
     * 
     * @param int $id The id of the Company Contact
     * @param int $company_id is added to the find() method in appModel::beforeFind()
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return object json object.
     */
    public function edit($id = null) {
        $this->CompanyContact->recursive = -1;
        $company_contact = $this->CompanyContact->findById($id);
        if ($company_contact) {
            $this->CompanyContact->id = $id;
            if ($this->CompanyContact->save($this->request->data)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Contact Edited'));
            } else {
                $errors = $this->validationErrorsToString($this->CompanyContact->validationErrors);
                throw new BadRequestException(__($errors));
            }
        } else {
            throw new NotFoundException(__('Contact Not Found'));
        }
    }

    /**
     * delete method
     * 
     * AppModel:: beforeDelete() checks if current user's company_id === contact's company_id.
     * i.e. isOwner check
     * 
     * After above check is complete, deletes specified contact
     * 
     * @param int $id The id of the Company Contact
     * 
     * @uses AppModel::beforeDelete() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array array with flash message.
     */
    public function delete($id = null) {
        $this->CompanyContact->id = $id;
        if ($this->CompanyContact->delete($id)) {
            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Company Contact Deleted'));
        } else {
            throw new NotFoundException(__('Contact Not Found'));
        }
    }
}
