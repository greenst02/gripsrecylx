<?php

App::uses('AppController', 'Controller');

/**
 * AuditQuestionCategories Controller
 *
 * Only index() and view() methods are currently used.
 * There is no ability in the app to add new categories or edit existing categories.
 * 
 * 
 * @property AuditQuestionCategory $AuditQuestionCategory
 */
class AuditQuestionCategoriesController extends AppController {

    var $name = 'AuditQuestionCategories';

    /**
     * index method
     * returns a list of Categories for an audit type. default is SPG (audit_category_type_id=1)
     * List returned differs depending on user's group_id.
     * If user is supplier, only applicable categories are returned.
     * 
     * @uses AuditQuestonsCategory::getCategoriesForType
     * @uses AuditQuestonsCategory::getSupplierCategoriesForType
     *
     * @param int $group_id The $group_id is read from the Session.
     * @param int $audit_category_type_id the type of audit category to retrieve.
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @edit    2015-02-01<br />
     *          Keith Burton <keith.burton@greenstreets.ie><br />
     *          Refactored finds into model
     *          and added category_type_id as a parameter to allow for
     *          custom audit types<br/>
     * @return array JSON object array listing all Abaudits associated with a single company
     */
    public function index() {
        $group_id = CakeSession::read('User.group_id');

        $audit_question_category_type_id = $this->request->query('audit_question_category_type_id');
        
        if (!$audit_question_category_type_id){
            $audit_question_category_type_id=1;
        }
    		
/*        $this->AuditQuestionCategory->recursive = -1;

        $fields = array(
            'id',
            'name'
        );

        $base_conditions = array(
            'audit_question_category_type_id' => $audit_question_category_type_id
        );

        $supplier_conditions = array('supplier' => 1);
*/
        /*
         * if logged in as a supplier, add additional restrictions
         */

/*        if ($group_id === '4') {
            $conditions = array_merge($base_conditions, $supplier_conditions);
        } else {
            $conditions = $base_conditions;
        }

        $audit_question_category = $this->AuditQuestionCategory->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions
        ));
*/

        if ($group_id === '5') {
            $audit_question_category=$this->AuditQuestionCategory->getSupplierCategoriesForType($audit_question_category_type_id);
        } else {
            $audit_question_category=$this->AuditQuestionCategory->getCategoriesForType($audit_question_category_type_id);
        }

        $this->respondAsJSON(STATUS_CODE_OK, Hash::extract($audit_question_category, '{n}.AuditQuestionCategory'));
    }

    /**
     * view method
     * 
     * Not Currently Used - 14/12/02
     * Finds the category by id
     * 
     * @param int $id The id of the Category
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array json array.
     */
//    public function view($id = null) {
//        $this->AuditQuestionCategory->recursive = -1;
//
//        if (!$this->AuditQuestionCategory->exists($id)) {
//            throw new NotFoundException(__('Invalid AuditQuestionCategory'));
//        }
//        $category = $this->AuditQuestionCategory->findById($id);
//
//        $this->respondAsJSON(STATUS_CODE_OK, $category['AuditQuestionCategory']);
//    }

    /**
     * add method
     * 
     * Not Currently Used - 14/12/02
     * 
     * @access public
     * @throws  BadRequestException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return int id of new category or list of validation errors
     */
//    public function add() {
//        if ($this->request->is('post') || $this->request->is('put')) {
//            $new_category = $this->request->data;
//            $this->AuditQuestionCategory->create();
//            if ($this->AuditQuestionCategory->save($new_category)) {
//                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'New AuditQuestionCategory Added'));
//            } else {
//                $errors = $this->validationErrorsToString($this->AuditQuestionCategory->validationErrors);
//                throw new BadRequestException(__($errors));
//            }
//        }
//    }

    /**
     * edit method
     * 
     * Not Currently Used - 14/12/02
     * 
     * @param int $id The Category id.
     * @access public
     * @throws  BadRequestException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return string success message or list of validation errors
     */
//    public function edit($id = null) {
//        $this->AuditQuestionCategory->id = $id;
//
//        if (!$this->AuditQuestionCategory->exists($id)) {
//            throw new NotFoundException(__('Invalid Company Site id'));
//        }
//        if ($this->request->is('post') || $this->request->is('put')) {
//            Configure::write('debug', 0);
//            $this->disableCache();
//
//            if ($this->AuditQuestionCategory->save($this->request->data)) {
//
//                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'AuditQuestionCategory Edited'));
//            } else {
//                $errors = $this->validationErrorsToString($this->AuditQuestionCategory->validationErrors);
//                throw new BadRequestException(__($errors));
//            }
//        }
//    }

    /**
     * delete method
     *
     * Not Currently Used - 14/12/02
     * 
     * @param int $id The Category id.
     * @access public
     * @throws  BadRequestException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return string success message of list of validation errors
     */
//    public function delete($id = null) {
//        $this->AuditQuestionCategory->id = $id;
//        if (!$this->AuditQuestionCategory->exists($id)) {
//            throw new NotFoundException(__('Invalid AuditQuestionCategory id'));
//        }
//
//        if ($this->AuditQuestionCategory->delete($id)) {
//            $message = 'AuditQuestionCategory Deleted';
//            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => $message));
//        } else {
//                throw new BadRequestException(__('Error'));
//        }
//    }

    /**
     * save_firebase method
     *
     * Used by a demo, not accessible from normal app.
     * The save_firebase() method was used to enable the ui-tree demo.
     * 
     * @param int $id The Category id.
     * @access public
     * @throws  BadRequestException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     */
//    public function save_firebase() {
//        if ($this->request->is('post')) {
//            $data = $this->request->input('json_decode');
//
//            $this->respondAsJSON(STATUS_CODE_OK, $data);
//        }
//    }
}
