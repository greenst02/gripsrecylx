<?php

App::uses('AppController', 'Controller');

/**
 * AuditToQuestions Controller
 *
 * Only the index() method is currently used.
 * 
 * @property AuditToQuestion $AuditToQuestion
 */
class AuditToQuestionsController extends AppController {

    var $name = 'AuditToQuestions';

    /**
     * index method
     * 
     * Returns a list of questions associated with a specified audit
     * Questions returned differ for supplier (23 questions) and other user types (70 questions)
     * @uses AuditToQueston::getAuditTosForType
     * @uses AuditToQueston::getSupplierAuditTosForType
     *
     * 
     * @param int $audit_id The id of the audit to find associated questions
     * @param int $company_id The id of the user's company read from the session
     * @param int $group_id The id of the user's group read from the session
     *
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @edit    2015-02-01<br />
     *          Keith Burton <keith.burton@greenstreets.ie><br />
     *          Refactored finds into model
     *          and removed <=70 on the find<br/>

     * @return array JSON object array of questions
     */
    public function index($audit_id = null) {
        $group_id = CakeSession::read('User.group_id');

        /*        $base_conditions = array(
          'AuditToQuestion.company_audit_id' => $audit_id,
          'AuditToQuestion.audit_question_id <=' => 70
          );

          $supplier_conditions = array('AuditQuestion.supplier' => 1);
         */
        /**
         * if logged in as a supplier, add additional condidtions to the find action
         */
        /*        if ($group_id === '4') {
          $conditions = array_merge($base_conditions, $supplier_conditions);
          } else {
          $conditions = $base_conditions;
          }
         */
        /**
         * AuditToQuestion.id as id - This is used to populate responces section on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
         * audit_question_id is used by navigation on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
         */
        /*        $fields = array(
          'AuditToQuestion.id  as id', // NB: used to navigate through questions.
          'AuditToQuestion.company_audit_id AS company_audit_id',
          'AuditToQuestion.audit_question_id AS audit_question_id',
          'AuditQuestion.question AS question',
          'AuditQuestion.audit_question_category_id AS audit_question_category_id',
          'AuditQuestion.supplier AS supplier_question',
          );

          $this->AuditToQuestion->recursive = 0;

          $audit_to_questions = $this->AuditToQuestion->find('all', array(
          'fields' => $fields,
          'conditions' => $conditions
          )
          );
         */

        if ($group_id === '5') {
            $audit_to_questions = $this->AuditToQuestion->getSupplierAuditTosForAudit($audit_id);
        } else {
            $audit_to_questions = $this->AuditToQuestion->getAuditTosForAudit($audit_id);
        }

        $a1 = Hash::extract($audit_to_questions, '{n}.AuditToQuestion');
        $a2 = Hash::extract($audit_to_questions, '{n}.AuditQuestion');

        $audit_to_questions = Hash::merge($a1, $a2);

        $this->respondAsJSON(STATUS_CODE_OK, $audit_to_questions);
    }

    /**
     * view method
     * 
     * Not Currently Used - 14/12/02
     * 
     * @throws Exception NotImplementedException
     *
     */
//    public function view($id = null) {
//
//        throw NotImplementedException;
//        if (!$this->AuditToQuestion->exists($id)) {
//            throw new NotFoundException(__('Invalid audit to question'));
//        }
//        $options = array('conditions' => array('AuditToQuestion.' . $this->AuditToQuestion->primaryKey => $id));
//        $this->set('auditToQuestion', $this->AuditToQuestion->find('first', $options));
//    }

    /**
     * add method
     * 
     * Not Currently Used - 14/12/02
     * 
     * @throws Exception NotImplementedException
     */
//    public function add() {
//        throw NotImplementedException;
//        if ($this->request->is('post')) {
//            $this->AuditToQuestion->create();
//            if ($this->AuditToQuestion->save($this->request->data)) {
//                $this->Session->setFlash(__('The audit to question has been saved'));
//                $this->redirect(array('action' => 'index'));
//            } else {
//                $this->Session->setFlash(__('The audit to question could not be saved. Please, try again.'));
//            }
//        }
//        $companyAudits = $this->AuditToQuestion->CompanyAudit->find('list');
//        $auditQuestions = $this->AuditToQuestion->AuditQuestion->find('list');
//        $companies = $this->AuditToQuestion->Company->find('list');
//        $this->set(compact('companyAudits', 'auditQuestions', 'companies'));
//    }

    /**
     * edit method
     * 
     * Not Currently Used - 14/12/02
     * 
     * @throws Exception NotImplementedException
     */
//    public function edit($id = null) {
//
//        throw NotImplementedException;
//        if (!$this->AuditToQuestion->exists($id)) {
//            throw new NotFoundException(__('Invalid audit to question'));
//        }
//        if ($this->request->is('post') || $this->request->is('put')) {
//            if ($this->AuditToQuestion->save($this->request->data)) {
//                $this->Session->setFlash(__('The audit to question has been saved'));
//                $this->redirect(array('action' => 'index'));
//            } else {
//                $this->Session->setFlash(__('The audit to question could not be saved. Please, try again.'));
//            }
//        } else {
//            $options = array('conditions' => array('AuditToQuestion.' . $this->AuditToQuestion->primaryKey => $id));
//            $this->request->data = $this->AuditToQuestion->find('first', $options);
//        }
//        $companyAudits = $this->AuditToQuestion->CompanyAudit->find('list');
//        $auditQuestions = $this->AuditToQuestion->AuditQuestion->find('list');
//        $companies = $this->AuditToQuestion->Company->find('list');
//        $this->set(compact('companyAudits', 'auditQuestions', 'companies'));
//    }

    /**
     * delete method
     * 
     * Not Currently Used - 14/12/02
     * 
     * @throws Exception NotImplementedException
     */
//    public function delete($id = null) {
//        throw NotImplementedException;
//        $this->AuditToQuestion->id = $id;
//        if (!$this->AuditToQuestion->exists()) {
//            throw new NotFoundException(__('Invalid audit to question'));
//        }
//        $this->request->onlyAllow('post', 'delete');
//        if ($this->AuditToQuestion->delete()) {
//            $this->Session->setFlash(__('Audit to question deleted'));
//            $this->redirect(array('action' => 'index'));
//        }
//        $this->Session->setFlash(__('Audit to question was not deleted'));
//        $this->redirect(array('action' => 'index'));
//    }
}
