<?php

App::uses('AppController', 'Controller');

/**
 * IndustryTypes Controller
 *
 * @property IndustryType $IndustryType
 */
class IndustryTypesController extends AppController {

    var $name = 'IndustryTypes';

    /**
     * index method
     * return a list of Industry Types
     * - is accessed from the companyDetails form -> the APC Classification Section.
     * 
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all Industry Types
     */
    public function index() {
        $this->IndustryType->recursive = -1;
        $industry_types_list = $this->IndustryType->find('all');
        $industry_types = Hash::extract($industry_types_list, '{n}.IndustryType');
        $this->respondAsJSON(STATUS_CODE_OK, $industry_types);
    }
}
