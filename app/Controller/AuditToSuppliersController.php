<?php

App::uses('AppController', 'Controller');

/**
 * AuditToSuppliers Controller
 * 
 * functionality related to connecting audits to supplier type users.
 * used by the page: dashboard/companyAudits/audit/274/auditSuppliers/list
 *
 * @property AuditToSupplier $AuditToSupplier
 */
class AuditToSuppliersController extends AppController {

    var $name = 'AuditToSuppliers';

    /**
     * index method
     * 
     * return a list of suppliers linked to a specified audit.
     * Restricted to User group and above
     * 
     * @param int $audit_id The id of the audit
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all suppliers associated with a single audit
     */
    public function index($audit_id = null) {
        $conditions = array(
            'AuditToSupplier.company_audit_id' => $audit_id
        );

        $fields = array(
            'AuditToSupplier.id as id',
            'AuditToSupplier.company_audit_id AS company_audit_id',
            'AuditToSupplier.user_id AS supplier_id',
            'AuditToSupplier.request_sent AS request_sent',
            'User.username AS username',
            'User.firstname AS firstname',
            'User.lastname AS lastname',
            'User.supplier_company AS supplier_company'
        );

        $this->AuditToSupplier->recursive = 0;

        $audit_to_suppliers_list = $this->AuditToSupplier->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions
        ));

        $a1 = Hash::extract($audit_to_suppliers_list, '{n}.AuditToSupplier');
        $a2 = Hash::extract($audit_to_suppliers_list, '{n}.User');

        $audit_to_suppliers = Hash::merge($a1, $a2);

        $this->respondAsJSON(STATUS_CODE_OK, $audit_to_suppliers);
    }

    /**
     * index method
     * 
     * Returns a list of company suppliers that are not associated with a specified audit.
     * 
     * @param int $company_id The company_id is read from the Session
     * @param int $audit_id The id of the audit
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all company suppliers that are not associated with a specified audit.
     */
    public function index_not_associated($audit_id = null) {
        $company_id = CakeSession::read('User.company_id');

        /**
         * First get id's of associated suppliers (user_id')
         */
        $this->AuditToSupplier->recursive = -1;
        $associated_suppliers = $this->AuditToSupplier->find('all', array(
            'conditions' => array(
                'company_audit_id' => $audit_id
            ),
            'fields' => array(
                'AuditToSupplier.user_id AS user_id'
            )
        ));

        $associated_suppliers = Hash::extract($associated_suppliers, '{n}.AuditToSupplier.user_id');

        $this->loadModel('User');
        $this->User->recursive = 0;

        $conditions = array(
            'User.company_id' => $company_id,
            'User.group_id' => 5,
            'NOT' => array(
                'User.id' => $associated_suppliers
            )
        );

        $fields = array(
            'User.id AS supplier_id',
            'User.username AS username',
            'User.firstname AS firstname',
            'User.lastname AS lastname',
            'User.supplier_company AS supplier_company'
        );

        $this->AuditToSupplier->recursive = 0;

        $audit_to_suppliers_list = $this->User->find('all', array(
            'conditions' => $conditions,
            'fields' => $fields,
        ));

        $a1 = Hash::extract($audit_to_suppliers_list, '{n}.AuditToSupplier');
        $a2 = Hash::extract($audit_to_suppliers_list, '{n}.User');

        $audit_to_suppliers = Hash::merge($a1, $a2);

        $this->respondAsJSON(STATUS_CODE_OK, $audit_to_suppliers);
    }

    /**
     * add method
     * 
     * Associates a specified supplier type user with a specified Audit
     * 
     * @param int $audit_id The id the audit
     * @param int $supplier_id The user_id of the supplier
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return string success message - STATUS_CODE_OK
     */
    public function add() {
        $company_id = CakeSession::read('User.company_id');

        if ($this->request->is('post') || $this->request->is('put')) {
            $data = $this->request->input('json_decode');
            $audit_id = $data->audit_id;
            $supplier_id = $data->supplier_id;

            /**
             * First check if the current user owns the Audit
             */
            $this->loadModel('CompanyAudit');
            $conditions = array(
                'CompanyAudit.id' => $audit_id,
                'CompanyAudit.company_id' => $company_id
            );

            $this->CompanyAudit->recursive = -1;
            $is_owner = $this->CompanyAudit->find('first', array(
                'conditions' => $conditions
            ));

            if ($is_owner) {
                $new_audit_to_supplier = array(
                    'company_id' => $company_id,
                    'company_audit_id' => $audit_id,
                    'user_id' => $supplier_id,
                );

                $this->AuditToSupplier->create();
                if ($this->AuditToSupplier->save($new_audit_to_supplier)) {                   
                    $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Supplier added to audit'));
                } else {
                    $errors = $this->validationErrorsToString($this->AuditToSupplier->validationErrors);
                    throw new BadRequestException(__($errors));
                }
            } else {
                throw new NotImplementedException(__('TO Do: not authorised!!'));
            }
        }
    }

    /**
     * delete method
     * 
     * deletes the association between an Audit and a Supplier
     * 
     * 1. check user group -> must not be supplier (ACL will enforce this rule)
     * 2. check that the current logged in user owns the Audit
     * 3. check that supplier has NOT already been sent a request (supplier.request_sent)
     * 4. delete the association
     * 
     * @param int $company_id The company_id is read from the Session
     * @param int $audit_id The company_id is read from the Session
     * @param int $supplier_id The id of the audit
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return string Success Message, Failure Message, Not Authorised Message
     */
    public function delete() {
        $company_id = CakeSession::read('User.company_id');

        if ($this->request->is('post') || $this->request->is('put')) {
            $data = $this->request->input('json_decode');
            $audit_id = $data->audit_id;
            $supplier_id = $data->supplier_id;

            /*
             * First check if the current user owns the Audit
             */
            $this->loadModel('CompanyAudit');
            $conditions[] = array();
            $conditions = array(
                'CompanyAudit.id' => $audit_id,
                'CompanyAudit.company_id' => $company_id
            );

            $is_owner = $this->CompanyAudit->find('first', array(
                'conditions' => $conditions
            ));

            if ($is_owner) {
                $delete_audit_to_supplier = array(
                    'company_audit_id' => $audit_id,
                    'user_id' => $supplier_id,
                );

                if ($this->AuditToSupplier->deleteAll($delete_audit_to_supplier)) {
                    $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Supplier removed from audit'));
                } else {
                    $errors = $this->validationErrorsToString($this->AuditToSupplier->validationErrors);
                    throw new BadRequestException(__($errors));
                }
            } else {
                throw new BadRequestException(__('Not authorised'));
            }
        }
    }

    /**
     * send_request_to_supplier method
     * 
     * 1. retrieve data from client side
     * 2. create blank responses for the supplier to use
     * 3. when the blank responses have been saved the request can be sent to the supplier.
     *  - AuditToSuppliersController::_prepare_to_send_to_supplier
     * 4. change the request_sent field in Audit_to_supplier to true / 1
     * 5. Send Success or Failure message to client
     * 
     * @uses AuditQuestionResponse::create_blank_responses
     * @uses AuditToSuppliersController::_prepare_to_send_to_supplier
     * 
     * @param int $company_id The company_id is read from the Session
     * @param int $audit_id The company_id is read from the Session
     * @param int $supplier_id The id of the audit
     * @param date $respond_by respond_by date
     * @param string $message the message that will form the body of the email to the supplier
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return Success Message, Failure Message
     */
    public function send_request_to_supplier() {
        $company_id = CakeSession::read('User.company_id');

        /**
         * retrieve data from client side
         */
        $data = $this->request->input('json_decode');
        $audit_id = $data->audit_id;
        $supplier_id = $data->supplier_id;
        $respond_by = $data->respond_by;
        $message = $data->supplier_request_message;
        $subject = $data->supplier_request_subject;

        /**
         * create blank responses for the supplier to use
         */
        $this->loadModel('AuditQuestionResponse');
        if ($this->AuditQuestionResponse->create_blank_responses($audit_id, $company_id, $supplier_id)) {
            /**
             * when the blank responses have been saved the request can be sent to the supplier.
             * uses a method from another controller to send the email to the supplier
             * UsersController::send_link_to_supplier
             */
            $this->loadModel('User');
            $this->User->recursive = -1;

            $supplier_details = $this->User->find('first', array(
                'conditions' => array('id' => $supplier_id),
                'fields' => array('User.id', 'User.username', 'User.company_id')
            ));

            if (!$this->_prepare_to_send_to_supplier($supplier_details, $message, $subject)) {
                throw new BadRequestException(__('Your request was not sent'));
            } else {

                /**
                 * change the request_sent field in Audit_to_supplier to true / 1
                 */
                $this->AuditToSupplier->recursive = -1;

                $fields = array(
                    'AuditToSupplier.request_sent' => 1,
                    'AuditToSupplier.respond_by' => "\"" . $respond_by . "\""
                );

                $conditions = array(
                    'AuditToSupplier.user_id' => $supplier_id,
                    'AuditToSupplier.company_audit_id' => $audit_id,
                    'AuditToSupplier.company_id' => $company_id
                );

                if ($this->AuditToSupplier->updateAll($fields, $conditions)) {
                    $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Request has been sent'));
                } else {
                    $errors = $this->validationErrorsToString($this->AuditToSupplier->validationErrors);
                    throw new BadRequestException(__($errors));
                }
            }
        }
    }

    /**
     * _prepare_to_send_to_supplier method
     * 
     * prepares data before it can be passed to User::sendEmail
     * 
     * @uses User::createTokenParams
     * @uses User::sendEmail
     * 
     * @param array $supplier_details
     * @param string $message the message that will form the body of the email to the supplier
     * @access private
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return boolean
     */
    private function _prepare_to_send_to_supplier($supplier_details, $message, $subject) {
        $this->loadModel('User');

        $company = $this->User->Company->findById($supplier_details['User']['company_id']);
        $company_name = $company['Company']['trading_name'];

        $view_variables['key'] = $this->User->createTokenParams($supplier_details);
        $view_variables['url'] = Router::url('/', true) . 'app/webroot/bin/#/updateSupplierPassword/' . $view_variables['key'];

        $view_variables['message'] = $message;

        $supplier_details['User']['tokenhash'] = $view_variables['key'];

        $this->User->id = $supplier_details['User']['id'];
        if ($this->User->saveField('tokenhash', $supplier_details['User']['tokenhash'])) {
            $this->User->sendEmail('supplierreq', 'spg@greenstreets.ie', $supplier_details['User']['username'], $subject, $view_variables, $company_name);
            return true;
        } else {
            return false;
        }
    }
}
