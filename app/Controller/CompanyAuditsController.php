<?php

App::uses('AppController', 'Controller');

/**
 * CompanyAudits Controller
 * 
 * Main controller for listing, uploading, viewing and deleting Company Audits
 *
 * CompanyAudit is not included AppModel::beforeFind() so the 
 * company_id must be read from the session for use with find() methods.
 *
 * @property CompanyAudit $CompanyAudit
 */
class CompanyAuditsController extends AppController {

    public function category_types_index() {

        $company_id = CakeSession::read('User.company_id');
        $group_id = CakeSession::read('User.group_id');

        /*
         * If user is admin or manager then they can select audits from different companies to copy.
         * if normal user, then they can only copy audits from their own company
         */

        $this->loadModel('AuditQuestionCategoryType');

        if ($group_id < 3) {
            if (isset($this->request->data['company_id'])) {
                $question_categories = $this->AuditQuestionCategoryType->getAllTypes();
            } else {
                $question_categories = $this->AuditQuestionCategoryType->getTypesForCompany($company_id);
            }
        }
        $question_categories = $this->AuditQuestionCategoryType->getAllTypes();

        $this->respondAsJSON(STATUS_CODE_OK, Hash::extract($question_categories, '{n}.AuditQuestionCategoryType'));
    }


    private function TestCurl($powerBI = null){

                    /* Get oauth2 token using a POST request */

                    


                    $curlPostToken = curl_init();

                    curl_setopt_array($curlPostToken, array(

                    CURLOPT_URL => "https://login.windows.net/common/oauth2/token",

                    CURLOPT_RETURNTRANSFER => true,

                    CURLOPT_ENCODING => "",

                    CURLOPT_MAXREDIRS => 10,

                    CURLOPT_TIMEOUT => 30,

                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,

                    CURLOPT_CUSTOMREQUEST => "POST",

                    CURLOPT_POSTFIELDS => array(

                    'grant_type' => 'password',

                    'scope' => 'openid',

                    'resource' => 'https://analysis.windows.net/powerbi/api',

                    'client_id' => '5bf748d7-4ce2-476c-a21e-d4b741a7f86a', // Registered App ApplicationID

                    'username' => 'powerbi.dataflow@GreenstreetsAdmin.onmicrosoft.com', // for example john.doe@yourdomain.com

                    'password' => 'b5Yh{F\6DM{HXfe' // Azure password for above user

                    )

                    ));

                    $tokenResponse = curl_exec($curlPostToken);


                    $tokenError = curl_error($curlPostToken);

                    curl_close($curlPostToken);


                    // decode result, and store the access_token in $embeddedToken variable:

                    $tokenResult = json_decode($tokenResponse, true);

                    $token = $tokenResult["access_token"];

                    $embeddedToken = "Bearer "  . ' ' .  $token;


                    /*      Use the token to get an embedded URL using a GET request */

                    $curlGetUrl = curl_init();

                    
                    //CURLOPT_URL => "https://api.powerbi.com/v1.0/myorg/groups/69827214-c23c-40c9-9744-71dd50b79938/reports/",


                    $powerBI_group = $powerBI['powerbi']['group_id'];
                    $powerBI_report = $powerBI['powerbi']['report_id'];

//                    $powerBI_group= '10ce2816-7fcf-4ac6-bba9-88d3b427035e';
//                    if($powerBI_report == null) {
//                        $powerBI_report ='14d8553c-9710-4c0f-b4ce-46bab340411d';
//                    }
 

                    curl_setopt_array($curlGetUrl, array(

                        
                        CURLOPT_URL => "https://api.powerbi.com/v1.0/myorg/groups/$powerBI_group/reports/",

                    
                    CURLOPT_RETURNTRANSFER => true,

                    CURLOPT_ENCODING => "",

                    CURLOPT_MAXREDIRS => 10,

                    CURLOPT_TIMEOUT => 30,

                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,

                    CURLOPT_CUSTOMREQUEST => "GET",

                    CURLOPT_HTTPHEADER => array(

                    "Authorization: $embeddedToken",

                    "Cache-Control: no-cache",

                    ),

                    ));

                    $embedResponse = curl_exec($curlGetUrl);

                    $embedError = curl_error($curlGetUrl);

                    curl_close($curlGetUrl);

                    $embedUrl = null;

                    if ($embedError) {

                        echo "cURL Error #:" . $embedError;
                        
                        } else {
                        
                            $embedResponse = json_decode($embedResponse, true);
                        
                     //   echo pr($embedResponse);
                     //   die;

                     // to do loop through the response to find matcing report id
                     // $embedResponse['value'][i]['id'] = $powerBI_report

 

                        foreach($embedResponse['value'] as $embedItem){

                            //echo pr($embedResponse['value']);

                            //echo pr($embedItem['id']);
                            $repID = $embedItem['id'];
                            
                            if($powerBI_report === $repID) {
                                $embedUrl = $embedItem['embedUrl'];

//                                echo pr('found....');
                            }
                            

  //                          die;

                            
//
//                            if(strpos($powerBI_report, $embedItem['id']) !== false){
//                            }
                        }

                        
                        }

               //  echo pr(array('token'=> $token, 'embedUrl' =>$embedUrl, 'powerBIReportID' =>$powerBI_report ));

                 return array('token'=> $token, 'embedUrl' =>$embedUrl, 'powerBIReportID' =>$powerBI_report );

    }




    /**
     * index method
     * return a list of Audits associated with a company.
     * 
     * @param int $company_id The company_id is read from the Session.
     * @param int $group_id The group_id is read from the Session.
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all Audits associated with a single company
     */
    public function index() {
        $company_id = CakeSession::read('User.company_id');
        $group_id = CakeSession::read('User.group_id');

        $cat_id=-1;

        if (isset($this->request->query['category_type_id'])) {

            $cat_id = $this->request->query['category_type_id'];

        }




        /*
         * If user is admin or manager then they can select audits from different companies to copy.
         * if normal user, then they can only copy audits from their own company
         */
        if ($group_id < 3) {
            if (isset($this->request->data['company_id'])) {
                $company_id = $this->request->data['company_id'];
            } else {
                $company_id = CakeSession::read('User.company_id');
            }
        } else {
            $company_id = CakeSession::read('User.company_id');
        }


        if (isset($this->request->query['category_type_id'])) {

            $cat_id = $this->request->query['category_type_id'];

            //temp to test
            if ($cat_id>=900) {    
                
                $whereClause = "company_id=$company_id and audit_question_category_type_id>=$cat_id;";
            } else {
                $whereClause = "company_id=$company_id and audit_question_category_type_id=$cat_id;";

            }

        }
        else {

            $whereClause = "company_id=$company_id;";

        }


        if ($cat_id === 999) {

            $strLenght = "1000";

        }else
        {
            $strLenght= "100";
        }

        $this->CompanyAudit->recursive = 1;
        $company_audits_list = $this->CompanyAudit->query(
                "SELECT 
                        company_audits.id as id, 
                        company_audits.audit_date as audit_date, 
                        company_audits.spg_audit_code as spg_audit_code, 
                        company_audits.lead_auditor_name as lead_auditor_name, 
                        company_audits.company_id as company_id, 
                        company_audits.audit_enabled_status_id as audit_enabled_status_id,
                        LEFT(company_audits.audit_description,1000) as shortened_description,
                        audit_enabled_statuses.status as audit_enabled_status,
						company_audits.audit_question_category_type_id as audit_question_category_type_id
                    FROM 
                        company_audits 
                        
                    LEFT JOIN 
                        audit_enabled_statuses 
                    ON 
                        audit_enabled_statuses.id=company_audits.audit_enabled_status_id 
                    WHERE 
                        $whereClause;"
        );

        $a1 = Hash::extract($company_audits_list, '{n}.0');
        $a2 = Hash::extract($company_audits_list, '{n}.company_audits');
        $a3 = Hash::extract($company_audits_list, '{n}.audit_enabled_statuses');
        $company_audits = Hash::merge($a1, $a2, $a3);


        if ($cat_id == 999) {
        
            //echo pr($whereClause);

            //echo pr($company_audits);
            //echo pr($company_audits[0]['shortened_description']);
            //echo pr(json_decode($company_audits[0]['shortened_description'],true));
           
            //die;

            $powerbiArray = json_decode($company_audits[0]['shortened_description'],true);

            $this->respondAsJSON(STATUS_CODE_OK, $this->TestCurl($powerbiArray));
 

            }

        else {

            $this->respondAsJSON(STATUS_CODE_OK, $company_audits);


        }

 




    }

    public function index_reduced() {
        $company_id = CakeSession::read('User.company_id');

        $this->CompanyAudit->recursive = 1;
        $company_audits_list = $this->CompanyAudit->query(
                "SELECT 
                    company_audits.id AS company_audit_id, 
                    company_audits.spg_audit_code AS spg_audit_code
                    FROM 
                        company_audits
                    WHERE 
                        company_id = $company_id;"
        );

        $company_audits = Hash::extract($company_audits_list, '{n}.company_audits');
        $this->respondAsJSON(STATUS_CODE_OK, $company_audits);
    }

    /**
     * index_of_all_audits method
     * return a list of All Audits on the system
     * 
     * @param int $company_id The company_id is read from the Session.
     * @param int $group_id The group_id is read from the Session.
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all Audits associated with a single company
     */
    public function index_of_all_audits() {
        $company_id = CakeSession::read('User.company_id');
        $group_id = CakeSession::read('User.group_id');

        if ($group_id < 3) {
            if (isset($this->request->data['company_id'])) {
                $company_id = $this->request->data['company_id'];
            } else {
                $company_id = CakeSession::read('User.company_id');
            }
        } else {
            $company_id = CakeSession::read('User.company_id');
        }

        $this->CompanyAudit->recursive = 1;

        $fields = "
            company_audits.id as id, 
                company_audits.audit_date as audit_date, 
                company_audits.spg_audit_code as spg_audit_code, 
                company_audits.company_id as company_id, 
                company_audits.audit_enabled_status_id as audit_enabled_status_id,
                LEFT(company_audits.audit_description,100) as shortened_description,
                audit_enabled_statuses.status as audit_enabled_status,
                companies.trading_name as company_name
                ";

        $sql_query = " 
            FROM 
                company_audits              
            LEFT JOIN 
                audit_enabled_statuses 
            ON 
                audit_enabled_statuses.id = company_audits.audit_enabled_status_id
            LEFT JOIN
                companies
            ON
                companies.id = company_audits.company_id
            ";

        if (!empty($this->request->query['filter'])) {
            $filter = '%' . $this->request->query['filter'] . '%';
            $conditions = "   company_audits.audit_date LIKE '$filter'
                    OR
                        company_audits.spg_audit_code LIKE '$filter'
                    OR
                        audit_enabled_statuses.status LIKE '$filter'
                    OR
                        companies.trading_name LIKE '$filter'
                    ";

            $count = $this->CompanyAudit->query("SELECT COUNT(*) " . $sql_query . ' WHERE ' . $conditions);

            $sql_query = $fields . ' ' . $sql_query . ' WHERE ' . $conditions;
        } else {
            $count = $this->CompanyAudit->query("SELECT COUNT(*) " . $sql_query);
            $sql_query = $fields . ' ' . $sql_query;
        }

        $count = $count[0][0]['COUNT(*)'];

        if (($this->request->query['sorting']) && (strlen($this->request->query['sorting']) > 2)) {
            $sorting = preg_replace(array("/{/", "/}/", '/"/'), "", $this->request->query['sorting']);
            $sorting = preg_replace("/:/", " ", $sorting);

            $sql_query = $sql_query . ' ORDER BY ' . $sorting;
        } else {
            $sql_query = $sql_query . ' ORDER BY company_name ASC';
        }

        if (!empty($this->request->query['limit'])) {
            $sql_query = $sql_query . ' LIMIT ' . ($this->request->query['page'] * $this->request->query['limit']) . ', ' . $this->request->query['limit'];
        }

        $company_audits_list = $this->CompanyAudit->query("SELECT " . $sql_query);

        $a1 = Hash::extract($company_audits_list, '{n}.0');
        $a2 = Hash::extract($company_audits_list, '{n}.company_audits');
        $a3 = Hash::extract($company_audits_list, '{n}.audit_enabled_statuses');
        $a4 = Hash::extract($company_audits_list, '{n}.companies');
        $company_audits = Hash::merge($a1, $a2, $a3, $a4);

        $this->respondAsJSON(STATUS_CODE_OK, $company_audits, $count);
    }

    /**
     * view method
     * 
     * return a JSON object of the specified CompanyAudit
     * Uses CompanyAuditsController::_find_audit to check if the current User's company_id and Audit's company_id match.
     * i.e. isOwner
     * 
     * if user is not admin or manager, checks audit_enabled_status 
     * and throws UnauthorizedException if audit is disabled
     * 
     * @uses CompanyAuditsController::_find_audit
     * @uses CompanyAuditsController::_check_audit_status
     * 
     * @param int $company_id The company_id The id that is sent from client side is always 0. This is overwritten by the company_id from the session
     * @param int $group_id The group_id is read is from the session.
     * 
     * @throws  NotFoundException
     * @throws  UnauthorizedException
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array of all companies
     */
    public function view($id = null) {
        $group_id = CakeSession::read('User.group_id');

        $company_audit = $this->_find_audit($id);
        if ($company_audit) {
            // if user is not an admin, check if the audit has been disabled by the admin.
            if ($group_id > 2) {
                $status_id = $this->_check_audit_status($id);
                switch ($status_id) {
                    case 1:
                        throw new UnauthorizedException(__('Audit disabled by Manager'));
                };
            }
            $this->respondAsJSON(STATUS_CODE_OK, $company_audit['CompanyAudit']);
        } else {
            throw new NotFoundException(__('Audit Not Found'));
        }
    }

    /**
     * add method
     * 
     * 1. retrieve data from client side
     * 2. creates new audit
     * 3. creates audit_to_questions association using CompanyAudit::create_audit_to_questions
     * 4. creates create_blank_responses using AuditQuestionResponse::create_blank_responses
     * 
     * @uses CompanyAudit::create_audit_to_questions
     * @uses AuditQuestionResponse::create_blank_responses
     * 
     * @param array $new_audit array of spg_audit_code, audit_date, audit_description
     * @param int $company_id is read from session and appended to above array before new audit is saved
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @edit    2015-02-01<br />
     *          Keith Burton <keith.burton@greenstreets.ie><br />
     *          Added new parameter to allow for different category types. default to 1 for compatability<br/>
     * @return int $id The id of new audit.
     */
    public function add() {
        $company_id = CakeSession::read('User.company_id');

        /*
         * Get submitted Audit Details for the new Audit
         */
        if ($this->request->is('post') || $this->request->is('put')) {
            $new_audit_data = $this->request->data;
        }

        //if category type is not set, default to 1.  
        if (!isset($new_audit_data['audit_question_category_type_id'])) {

            $new_audit_data['audit_question_category_type_id'] = 1;
        }


        $audit_question_category_type_id = $new_audit_data['audit_question_category_type_id'];

        $new_audit = array(
            'CompanyAudit' => array(
                'company_id' => $company_id,
                'spg_audit_code' => $new_audit_data['spg_audit_code'],
                'audit_date' => $new_audit_data['audit_date'],
                'audit_description' => $new_audit_data['audit_description'],
                'audit_question_category_type_id' => $new_audit_data['audit_question_category_type_id'])
        );


        //add in aspect_id if it is set.
        if (isset($new_audit_data['aspect_id'])) {

            $new_audit['CompanyAudit']['aspect_id'] = $new_audit_data['aspect_id'];
        }



        /*
         * Save new audit
         */
        $this->CompanyAudit->save($new_audit);
        $company_audit_id = $this->CompanyAudit->id;

        $create_audit_to_questions = $this->CompanyAudit->create_audit_to_questions(
                $company_audit_id, $company_id, $audit_question_category_type_id);

        $this->loadModel('AuditQuestionResponse');
        $create_blank_responses = $this->AuditQuestionResponse->create_blank_responses($company_audit_id, $company_id, NULL);

        if ($create_audit_to_questions && $create_blank_responses) {
            $this->respondAsJSON(STATUS_CODE_OK, array('id' => $company_audit_id, 'flash' => 'New audit created.'));
        } else {
            throw new BadRequestException(__('Error, please try again'));
        }
    }

    /**
     * copy_by_manager method
     * 
     * Could be refactored - some repeated code with add() and copy() methods
     * 
     * 1. retrieve data from client side including the ID of the Audit to be copied
     * 2. creates new audit
     * 3. creates audit_to_questions association using CompanyAudit::create_audit_to_questions
     * 4. creates create_copied_responses using AuditQuestionResponse::create_copied_responses
     * 
     * @uses CompanyAuditsController::_copy_audit_fields
     * @uses CompanyAudit::create_audit_to_questions
     * @uses AuditQuestionResponse::create_copied_responses
     * 
     * @param array $new_audit array of copy_from_audit_id, spg_audit_code, audit_date, audit_description
     * @param int $company_id is read from session and appended to above array before new audit is saved
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return int The id of the newly created audit
     */
    public function copy_by_manager() {
        $company_id = CakeSession::read('User.company_id');

        if ($this->request->is('post') || $this->request->is('put')) {
            // if an standard audit template has been selected
            if (isset($this->request->data['copy_from_audit_id'])) {
                $copy_from_audit_id = $this->request->data['copy_from_audit_id'];
            } else {
                $copy_from_audit_id = null;
            }

            if (isset($this->request->data['copy_from_company_id'])) {
                $copy_from_company_id = $this->request->data['copy_from_company_id'];
            } else {
                $copy_from_company_id = $company_id;
            }

            $source_audit = $this->CompanyAudit->findById($copy_from_audit_id);

            /*
             * When 'Copy Existing' is selected from the create audit form
             * 3 fields will be sent from that form (Reference Code, Audit Date, Audit Description).
             * All other fields will be copied from the selected audit.
             */
            if (isset($this->request->data['spg_audit_code'])) {
                $spg_audit_code = $this->request->data['spg_audit_code'];
                $audit_date = $this->request->data['audit_date'];
                $audit_description = $this->request->data['audit_description'];
            } else {
                $spg_audit_code = 'Copy ' . $source_audit['CompanyAudit']['spg_audit_code'];
                $audit_date = date("Y-m-d");
                $audit_description = $source_audit['CompanyAudit']['audit_description'];
            }

            $company_audit_id = $this->_copy_audit_fields($company_id, $spg_audit_code, $audit_date, $audit_description, $source_audit);

            $create_audit_to_questions = $this->CompanyAudit->create_audit_to_questions($company_audit_id, $company_id);

            if ($create_audit_to_questions) {
                $this->loadModel('AuditQuestionResponse');

                // if no template has been chosen, create blank responses.
                if (is_null($copy_from_audit_id)) {
                    // create a blank response for the company for each question
                    $this->AuditQuestionResponse->create_blank_responses($company_audit_id, $company_id, NULL);
                } else {
                    $audit_template_id = $copy_from_audit_id;
                    $this->AuditQuestionResponse->create_copied_responses($audit_template_id, $company_audit_id, $company_id, $copy_from_company_id);
                }

                $this->respondAsJSON(STATUS_CODE_OK, array('id' => $company_audit_id, 'flash' => 'Copy audit created.'));
            } else {
                $m1 = $this->CompanyAudit->validationErrors;
                $m2 = $this->AuditToQuestion->validationErrors;

                $errors = $this->validationErrorsToString(Hash::merge($m1, $m2));
                throw new BadRequestException(__($errors));
            }
        }  //end check for post/put     
    }

    /**
     * copy method
     * 
     * Could be refactored - some repeated code with add() and copy_by_manager() methods
     * 
     * Create a copy of an audit.
     * User can copy a pre-existing audit from their own company
     * 
     * 1. retrieve data from client side including the ID of the Audit to be copied
     * 2. creates new audit
     * 3. creates audit_to_questions association using CompanyAudit::create_audit_to_questions
     * 4. creates create_copied_responses using AuditQuestionResponse::create_copied_responses
     * 
     * @uses CompanyAuditsController::_copy_audit_fields
     * @uses CompanyAudit::create_audit_to_questions
     * @uses AuditQuestionResponse::create_copied_responses
     *  
     * @param array $new_audit array of copy_from_audit_id, spg_audit_code, audit_date, audit_description
     * @param int $company_id is read from session and appended to above array before new audit is saved
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return int The id of the newly created audit
     */
    public function copy() {
        $company_id = CakeSession::read('User.company_id');
        $group_id = CakeSession::read('User.group_id');

        if ($this->request->is('post') || $this->request->is('put')) {
            // if an standard audit template has been selected
            if (isset($this->request->data['copy_from_audit_id'])) {
                $copy_from_audit_id = $this->request->data['copy_from_audit_id'];
            }

            if (isset($this->request->data['audit_question_category_type_id'])) {
                $audit_question_category_type_id = $this->request->data['audit_question_category_type_id'];
            }

            //} else {
            //	
            //		$copy_from_audit_id = $this->request->query('copy_from_audit_id');
            //
		//}
            $source_audit = $this->CompanyAudit->findById($copy_from_audit_id);

            // if user is not an admin, check if the audit if the audit has been disabled by the manager.
            if ($group_id > 2) {
                $status_id = $this->_check_audit_status($copy_from_audit_id);

                switch ($status_id) {
                    case 1:
                        throw new BadRequestException(__('Audit disabled by Manager'));
//                    case 2:
//                        $message = 'Company disabled';
//                        break;
//                    default:
//                        break;
                };
            }

            /*
             * When 'Copy Existing' is selected from the create audit form
             * 3 fields will be sent from that form (Reference Code, Audit Date, Audit Description).
             * All other fields will be copied from the selected audit.
             */
            if (isset($this->request->data['spg_audit_code'])) {
                $spg_audit_code = $this->request->data['spg_audit_code'];
                $audit_date = $this->request->data['audit_date'];
                $audit_description = $this->request->data['audit_description'];
            } else {
                $spg_audit_code = 'Copy ' . $source_audit['CompanyAudit']['spg_audit_code'];
                $audit_date = date("Y-m-d");
                $audit_description = $source_audit['CompanyAudit']['audit_description'];
            }



            $company_audit_id = $this->_copy_audit_fields($company_id, $spg_audit_code, $audit_date, $audit_description, $source_audit);

            $create_audit_to_questions = $this->CompanyAudit->create_audit_to_questions($company_audit_id, $company_id, $source_audit['CompanyAudit']['audit_question_category_type_id']);

            if ($create_audit_to_questions) {

                $this->loadModel('AuditQuestionResponse');

                // if no template has been chosen, create blank responses.
                if (is_null($copy_from_audit_id)) {
                    // create a blank response for the company for each question
                    $this->AuditQuestionResponse->create_blank_responses($company_audit_id, $company_id, NULL);
                } else {
//                    $audit_template_id = $copy_from_audit_id;
                    $this->AuditQuestionResponse->create_copied_responses($copy_from_audit_id, $company_audit_id, $company_id, $company_id);
                }

                $this->respondAsJSON(STATUS_CODE_OK, array('id' => $company_audit_id, 'flash' => 'Copy audit created.'));
            } else {
                $m1 = $this->CompanyAudit->validationErrors;
                $m2 = $this->AuditToQuestion->validationErrors;

                $errors = $this->validationErrorsToString(Hash::merge($m1, $m2));
                throw new BadRequestException(__($errors));
            }
        }  //end check for post/put     
    }

    /**
     * _copy_audit_fields method
     * 
     * copies some field values from the source_audit
     *  
     * @param int $company_id the id of the current company
     * @param string $spg_audit_code 
     * @param string $audit_date 
     * @param string $audit_description
     * @param array $source_audit
     * 
     * @access private
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return int The id of the newly created audit
     */
    private function _copy_audit_fields($company_id, $spg_audit_code, $audit_date, $audit_description, $source_audit) {
        $new_audit = array(
            'CompanyAudit' => array(
                'company_id' => $company_id,
                'spg_audit_code' => $spg_audit_code,
                'audit_date' => $audit_date,
                'audit_description' => $audit_description,
                'summary' => $source_audit['CompanyAudit']['summary'],
                'contact_name' => $source_audit['CompanyAudit']['contact_name'],
                'contact_details' => $source_audit['CompanyAudit']['contact_details'],
                'contact_email' => $source_audit['CompanyAudit']['contact_email'],
                'contact_phone' => $source_audit['CompanyAudit']['contact_phone'],
                'lead_auditor_name' => $source_audit['CompanyAudit']['lead_auditor_name'],
                'lead_auditor_email' => $source_audit['CompanyAudit']['lead_auditor_email'],
                'lead_auditor_phone' => $source_audit['CompanyAudit']['lead_auditor_phone'],
                'review_auditor_name' => $source_audit['CompanyAudit']['review_auditor_name'],
                'review_auditor_email' => $source_audit['CompanyAudit']['review_auditor_email'],
                'review_auditor_phone' => $source_audit['CompanyAudit']['review_auditor_phone'],
                'packaging_description' => $source_audit['CompanyAudit']['packaging_description'],
                'audit_question_category_type_id' => $source_audit['CompanyAudit']['audit_question_category_type_id']
            )
        );

        /*
         * Save new audit
         */
        $this->CompanyAudit->create();
        if ($this->CompanyAudit->save($new_audit)) {
            return $this->CompanyAudit->id;
        } else {
            throw new BadRequestException(__('Error, please try again'));
        }
    }

    /**
     * edit method
     * 
     * Update a specified CompanyAudit.
     * Ownership and enabled status must be checked first.
     * 
     * @uses CompanyAuditsController::_find_audit to check if the current User's company_id and Audit's company_id match.
     * @uses CompanyAuditsController::_check_audit_status to check if the audit has been disabled by a manager / admin
     * 
     * @param int $id The audit_id that is sent from client side.
     * @param int $company_id The company_id is read from the session
     * @param int $group_id The group_id is read is from the session.
     * @throws NotFoundException if not owner
     * @throws UnauthorizedException id normal user and audit has been dsabled by manager / admin
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array Array with success message
     */
    public function edit($id = null) {
        $group_id = CakeSession::read('User.group_id');

        $company_audit = $this->_find_audit($id);
        if ($company_audit) { // if user if not an admin, check if the audit if the audit has been disabled by the admin.
            if ($group_id > 2) {
                $status_id = $this->_check_audit_status($id);
                switch ($status_id) {
                    case 1:
                        throw new UnauthorizedException(__('Audit disabled by Manager'));
                };
            }

            if ($this->CompanyAudit->save($this->request->data)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Audit Edited'));
            } else {
                $errors = $this->validationErrorsToString($this->CompanyAudit->validationErrors);
                throw new BadRequestException(__($errors));
            }
        } else {
            throw new NotFoundException(__('Audit Not Found'));
        }
    }

    /**
      /**
     * delete method
     * 
     * ACL - Currently only available to admins and managers - 14/12/03
     * 
     * After above check is complete, deletes specified audit.
     * Normal cascading deletion of associated items wouldn't work with documents so 
     * CompanyDocument::delete_audit_documents() is used.
     * 
     * @param int $id The audit_id sent from client side.
     * 
     * @uses CompanyAuditsController::_find_audit to check if the current User's company_id and Audit's company_id match.
     * @uses CompanyDocument::delete_audit_documents() method deletes all documents associated with audit.
     * 
     * @throws NotFoundException if not owner
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array Array with success message
     */
    public function delete($id = null) {
        $company_audit = $this->_find_audit($id);
        if ($company_audit) {
            if ($this->CompanyAudit->delete($id)) {
                $this->loadModel('CompanyDocument');
                $this->CompanyDocument->delete_audit_documents($id);
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Audit deleted'));
            } else {
                throw new BadRequestException(__('Error, please try again'));
            }
        } else {
            throw new NotFoundException(__('Audit Not Found'));
        }

        /*
         * below would only be required if normal user is allowed to delete an audit
         */
//        if($group_id === '3') {
//            //if current user's company_id and audit's company_id match
//            $isOwner = $this->_find_audit($id);
//            if ($isOwner) {
//
//                /*
//                 *  if user, 
//                 *  check if the audit has been disabled by the manager.
//                 *  other statuses could be added to switch i.e. disabled by manager etc.
//                 */
//                $status_id = $this->_check_audit_status($id);
//                switch ($status_id) {
//                    case 1:
//                throw new BadRequestException(__('Audit disabled by Manager'));
//                    default:
//                        break;
//                };
//
//                if ($this->CompanyAudit->delete($id)) {
//                    $this->loadModel('CompanyDocument');
//                    $this->CompanyDocument->delete_audit_documents($id);
//                    $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Audit deleted'));
//                } else {
//                    $this->respondAsJSON(STATUS_CODE_NOT_FOUND, array('flash' => 'Not Found'));
//                }
//            }
//        }
    }

    /*
     * ************************************************************************
     */

//this function removes any unwanted html so that text can be embedded in docx template.
//certain html codes are converted to wordml
    private function strip_word_html($text, $allowed_tags = '<b><i><sup><sub><em><strong><u><br>') {
        mb_regex_encoding('UTF-8');
//replace MS special characters first

        $search = array('/&lsquo;/u', '/&quot;/u', '/&rsquo;/u', '/&ldquo;/u', '/&rdquo;/u', '/&mdash;/u', '/&nbsp;/u');
        $replace = array('\'', '\'', '\'', '"', '"', '-', ' ');
        $text = preg_replace($search, $replace, $text);
//make sure _all_ html entities are converted to the plain ascii equivalents - it appears
//in some MS headers, some html entities are encoded and some aren't
        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
//try to strip out any C style comments first, since these, embedded in html comments, seem to
//prevent strip_tags from removing html comments (MS Word introduced combination)
        if (mb_stripos($text, '/*') !== FALSE) {
            $text = mb_eregi_replace('#/\*.*?\*/#s', '', $text, 'm');
        }
//introduce a space into any arithmetic expressions that could be caught by strip_tags so that they won't be
//'<1' becomes '< 1'(note: somewhat application specific)
        $text = preg_replace(array('/<([0-9]+)/'), array('< $1'), $text);
        $text = strip_tags($text, $allowed_tags);

//strip out style and id tags
        $text = preg_replace('/style="(.+?)"/i', "", $text);
        $text = preg_replace('/id="(.+?)"/i', "", $text);
        $text = preg_replace('/_moz_editor_bogus_node="(.+?)"/i', "", $text);
        $text = preg_replace('/class="(.+?)"/i', "", $text);

//eliminate extraneous whitespace from start and end of line, or anywhere there are two or more spaces, convert it to one
        $text = preg_replace(array('/^\s\s+/', '/\s\s+$/', '/\s\s+/u'), array('', '', ' '), $text);
//strip out inline css and simplify style tags
        $search = array('#<(strong|b)[^>]*>(.*?)</(strong|b)>#isu', '#<(em|i)[^>]*>(.*?)</(em|i)>#isu', '#<u[^>]*>(.*?)</u>#isu');
//        $replace = array('<b>$2</b>', '<i>$2</i>', '<u>$1</u>');
        $replace = array('<w:rPr><w:b/></w:rPr><w:t>$2</w:t>', '<w:rPr><w:i/></w:rPr><w:t>$2</w:t>', '<u>$1</u>');

        $text = preg_replace($search, $replace, $text);
//on some of the ?newer MS Word exports, where you get conditionals of the form 'if gte mso 9', etc., it appears
//that whatever is in one of the html comments prevents strip_tags from eradicating the html comment that contains
//some MS Style Definitions - this last bit gets rid of any leftover comments */
        $num_matches = preg_match_all("/\<!--/u", $text, $matches);
        if ($num_matches) {
            $text = preg_replace('/\<!--(.)*--\>/isu', '', $text);
        }

//kb added this bit until I figure out how to do in one
        $text = str_replace(array('<br />', '<br>', '<br/>', '<BR>'), "<w:p></w:p>", $text);
        $text = str_replace(array("&amp;amp;", "&amp;"), "&", $text);
        $text = str_replace("&quot;", "'", $text);
        $text = str_replace("&nbsp;", " ", $text);
        $text = str_replace(array("<b>,<b >", "<b  >"), "", $text);
        $text = str_replace("</b>", "", $text);

        return $text;
    }

    private function _docxgen($fileName, $fileExt, $fileDir, $auditQuestionCompanyResponses, $opportunities_only = false, $word_template = "audittemplate.docx") {
        $audit = $auditQuestionCompanyResponses[0];
        $docxQuestions = array();
        //$i=0;
        //create array of data f rom $auditQuestionCompanyResponses to pass to docxgen
        //the strip_word_html converts any embedded html to wordml or removes it.
        if ($opportunities_only) {
            foreach ($auditQuestionCompanyResponses as $auditQuestionCompanyResponse) {
                $docxQuestions[] = array("#reference#" => $auditQuestionCompanyResponse['reference'],
                    "#question#" => $auditQuestionCompanyResponse['question'],
                    "#opportunity_note#" => $this->strip_word_html($auditQuestionCompanyResponse['opportunity_note']),
                    "#supplier_firstname#" => $this->strip_word_html($auditQuestionCompanyResponse['supplier_firstname']),
                    "#supplier_lastname#" => $this->strip_word_html($auditQuestionCompanyResponse['supplier_lastname']),
                    "#supplier_company#" => $this->strip_word_html($auditQuestionCompanyResponse['supplier_company']),
                );
            }

            $word_template = 'opportunitiesonlytemplate.docx';
        } else {
            foreach ($auditQuestionCompanyResponses as $auditQuestionCompanyResponse) {
                $docxQuestions[] = array("#reference#" => $auditQuestionCompanyResponse['reference'],
                    "#question#" => $auditQuestionCompanyResponse['question'],
                    "#consumer_note#" => $this->strip_word_html($auditQuestionCompanyResponse['consumer_note']),
                    "#secondary_note#" => $this->strip_word_html($auditQuestionCompanyResponse['secondary_note']),
                    "#tertiary_note#" => $this->strip_word_html($auditQuestionCompanyResponse['tertiary_note']),
                    "#opportunity_note#" => $this->strip_word_html($auditQuestionCompanyResponse['opportunity_note']),
                    "#audit_trail#" => $this->strip_word_html($auditQuestionCompanyResponse['audit_trail'])
                );
            }
        }

//        echo pr($audit['audit_question_category_type_id']);
        if($audit['audit_question_category_type_id']==3) {
//            echo 'here';
            $word_template = 'csrtemplate.docx';
        }

//        echo pr($audit);
//        echo $word_template;
//        die;

        App::import('Vendor', 'docxgen/docxgen');

//set path to template for output

        $phpdocx = new phpdocx(APP . $fileDir . "templates" . DS . $word_template);

        $phpdocx->assignToHeader("#CompanyNameHeader#", $audit['trading_name']); // basic field mapping to header
        $phpdocx->assign("#CompanyName#", $audit['trading_name']); // basic field mapping
        $phpdocx->assign("#AuditDate#", $audit['audit_date']);
        $phpdocx->assign("#ReviewCode#", $audit['spg_audit_code']);
//$phpdocx->assign("#AuditType#","");  // not currently set in template
        $phpdocx->assign("#ContactName#", $audit['contact_name']);
//        $phpdocx->assign("#ContactDetails#", $audit['contact_details']);
        $phpdocx->assign("#ContactPhone#", $audit['contact_phone']);
        $phpdocx->assign("#ContactEmail#", $audit['contact_email']);
        $phpdocx->assign("#LeadAuditor#", $audit['lead_auditor_name']);
        $phpdocx->assign("#ReviewAuditor#", $audit['review_auditor_name']);
        $phpdocx->assign("#Description#", $audit['audit_description']);
//$phpdocx->assign("#Photo#","");  //not currently set in template
        $phpdocx->assign("#Existing#", ($audit['isnew_packaging'] == 0 ? 'Existing' : 'New'));
        $phpdocx->assign("#SummaryNotes#", $this->strip_word_html($audit['summary']));

//pass array of responses to docxgen to create in document

        $phpdocx->assignBlock("questions", $docxQuestions);

//save the created file      
        $phpdocx->save(APP . $fileDir . 'temp_downloads' . DS . $fileName . $fileExt);

        return true;
    }

    /**
     * viewDocx method
     * allows user to download a copy of company_responses in word format
     * Returns a file.
     * ACL: Restricted to companyEmployee and above
     * 
     * if companyEmployee then checks if audit has been disabled.
     * 
     * @param int $audit_id The audit_id.
     * @param int $group_id The permissions for the user.
     * 
     * @uses CompanyAuditsController::__auditResponsesForDocument
     * @uses CompanyAuditsController::__createDocXFile
     * 
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return file An ms word document
     */
    public function viewDocx($audit_id = null) {
        // if user if not an manager, check if the audit if the audit has been disabled by the manager.
        $group_id = CakeSession::read('User.group_id');
        if ($group_id > 2) {
            $status_id = $this->_check_audit_status($audit_id);
            switch ($status_id) {
                case 1:
                    throw new BadRequestException(__('Audit disabled by Manager'));
//                    case 2:
//                        $message = 'Company disabled';
//                        break;
//                    default:
//                        break;
            };
        }

        $auditQuestionCompanyResponses = $this->__auditResponsesForDocument($audit_id, null);

        $this->__createDocXFile($auditQuestionCompanyResponses);
    }

    /**
     * viewSupplierDocx method
     * 
     * allows a suipplier to download a copy of their responses in word format
     * Returns a file.
     * ACL: Restricted to suppliers
     * 
     * @param int $audit_id The audit_id.
     * @param int $company_supplier_id The user id of a supplier.
     * 
     * @uses CompanyAuditsController::__auditResponsesForDocument
     * @uses CompanyAuditsController::__createDocXFile
     * 
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return file An ms word document
     */
    public function viewSupplierDocx($audit_id = null) {
        $company_supplier_id = CakeSession::read('User.id');

        if ($audit_id === null) {
            throw new BadRequestException(__('Invalid audit.'));
        }

        $auditQuestionSupplierResponses = $this->__auditResponsesForDocument($audit_id, $company_supplier_id);
        $this->__createDocXFile($auditQuestionSupplierResponses);
    }

    public function viewOpportunitiesDocx($audit_id = null) {

        if ($audit_id === null) {
            throw new BadRequestException(__('Invalid audit.'));
        }

        $auditOpportunitiesResponses = $this->__auditOpportunitiesResponsesForDocument($audit_id, null, true);

        $this->__createDocXFile($auditOpportunitiesResponses, true);
    }

    private function __createDocXFile($outputArray, $oppotunities_only = false) {
        $fileName = String::uuid(); //set a unique id for the file as it's only temp download
        $fileExt = '.docx'; // file extenstion

        $fileDir = 'docxgen_files' . DS; // directory where file will be written to
        //call to docxgen vendor file to create the document
//                    _docxgen($fileName, $fileExt, $fileDir, $auditQuestionCompanyResponses, $word_template = "audittemplate.docx", $opportunities_only = false

        if ($this->_docxgen($fileName, $fileExt, $fileDir, $outputArray, $oppotunities_only)) {
            //use sendfiles
            //Set the response type
            $this->response->type(array('docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'));
            $this->response->type('docx');

            //this set the file path and forces download:
            $this->response->file($fileDir . 'temp_downloads' . DS . $fileName . $fileExt, array('download' => true, 'name' => $outputArray[0]['spg_audit_code'] . $fileExt));

            //Using $this->autoRender instead of return $this->response
            $this->autoRender = false;
        }
    }

    private function __auditOpportunitiesResponsesForDocument($audit_id = null, $company_supplier_id = null, $opportunities_only = false) {
        $fields = array(
            'AuditQuestionResponse.id as id',
            'AuditQuestionResponse.audit_question_id as audit_question_id',
            'AuditQuestionResponse.company_id as company_id',
            'AuditQuestionResponse.audit_to_question_id as audit_question_id',
            'AuditQuestionResponse.created as created',
            'AuditQuestionResponse.modified as modified',
            'AuditQuestionResponse.company_audit_id as company_audit_id',
            'AuditQuestionResponse.company_supplier_id as company_supplier_id',
            'User.firstname as supplier_firstname',
            'User.lastname as supplier_lastname',
            'User.username as supplier_username',
            'User.supplier_company as supplier_company',
            'AuditQuestion.question as question',
            'AuditQuestion.reference as reference',
            'Company.trading_name as trading_name',
            'CompanyAudit.audit_date as audit_date',
            'CompanyAudit.spg_audit_code as spg_audit_code',
            'CompanyAudit.contact_name as contact_name',
            'CompanyAudit.contact_details as contact_details',
            'CompanyAudit.contact_email as contact_email',
            'CompanyAudit.contact_phone as contact_phone',
            'CompanyAudit.lead_auditor_name as lead_auditor_name',
            'CompanyAudit.review_auditor_name as review_auditor_name',
            'CompanyAudit.audit_description as audit_description',
            'CompanyAudit.isnew_packaging as isnew_packaging',
            'CompanyAudit.summary as summary'
        );

        $base_conditions = array(
            'AuditQuestionResponse.company_audit_id' => $audit_id,
//            'AuditQuestionResponse.company_supplier_id' => $company_supplier_id
        );

        $supplier_conditions = array(
            'AuditQuestion.supplier' => 1
        );

        if ($company_supplier_id === null) {
            $conditions = $base_conditions;
        } else {
            $conditions = array_merge($base_conditions, $supplier_conditions);
        }

        if ($opportunities_only) {
            $conditions = array_merge($conditions, array(
                'NOT' => array(
                    'AuditQuestionResponse.opportunity_note' => '',
                )
            ));

            array_push($fields, 'AuditQuestionResponse.opportunity_note as opportunity_note');
        } else {
            array_push($fields, 'AuditQuestionResponse.consumer_note as consumer_note', 'AuditQuestionResponse.secondary_note as secondary_note', 'AuditQuestionResponse.tertiary_note as tertiary_note', 'AuditQuestionResponse.opportunity_note as opportunity_note', 'AuditQuestionResponse.audit_trail as audit_trail');
        }

        $auditResponses = $this->CompanyAudit->AuditQuestionResponse->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
        ));

        $a1 = Hash::extract($auditResponses, '{n}.AuditQuestionResponse');
        $a2 = Hash::extract($auditResponses, '{n}.AuditQuestion');
        $a3 = Hash::extract($auditResponses, '{n}.Company');
        $a4 = Hash::extract($auditResponses, '{n}.CompanyAudit');
        $a5 = Hash::extract($auditResponses, '{n}.User');

        $auditResponses = Hash::merge($a1, $a2, $a3, $a4, $a5);

        return $auditResponses;
    }

    /**
     * __auditResponsesForDocument method
     * 
     * Retrieves the questions and responses etc. - All fields required to generate the document.
     * Fields differ depending on the it's a normal user or supplier 
     * i.e. 70 questions returned for normal user and less for a supplier
     * 
     * @param int $audit_id The audit_id.
     * @param int $company_supplier_id The user id of a supplier..
     * 
     * @access private
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @return array An array of the required fields
     */
    private function __auditResponsesForDocument($audit_id = null, $company_supplier_id = null, $opportunities_only = false) {
        $fields = array(
            'AuditQuestionResponse.id as id',
            'AuditQuestionResponse.audit_question_id as audit_question_id',
            'AuditQuestionResponse.company_id as company_id',
            'AuditQuestionResponse.audit_to_question_id as audit_question_id',
            'AuditQuestionResponse.created as created',
            'AuditQuestionResponse.modified as modified',
            'AuditQuestionResponse.company_audit_id as company_audit_id',
            'AuditQuestionResponse.company_supplier_id as company_supplier_id',
            'AuditQuestion.question as question',
            'AuditQuestion.reference as reference',
            'Company.trading_name as trading_name',
            'CompanyAudit.audit_date as audit_date',
            'CompanyAudit.spg_audit_code as spg_audit_code',
            'CompanyAudit.contact_name as contact_name',
            'CompanyAudit.contact_details as contact_details',
            'CompanyAudit.contact_email as contact_email',
            'CompanyAudit.contact_phone as contact_phone',
            'CompanyAudit.lead_auditor_name as lead_auditor_name',
            'CompanyAudit.review_auditor_name as review_auditor_name',
            'CompanyAudit.audit_description as audit_description',
            'CompanyAudit.isnew_packaging as isnew_packaging',
            'CompanyAudit.summary as summary',
            'CompanyAudit.audit_question_category_type_id as audit_question_category_type_id'
        );

        $base_conditions = array(
            'AuditQuestionResponse.company_audit_id' => $audit_id,
            'AuditQuestionResponse.company_supplier_id' => $company_supplier_id
        );

        $supplier_conditions = array(
            'AuditQuestion.supplier' => 1
        );

        if ($company_supplier_id === null) {
            $conditions = $base_conditions;
        } else {
            $conditions = array_merge($base_conditions, $supplier_conditions);
        }

        if ($opportunities_only) {
            array_push($fields, 'AuditQuestionResponse.opportunity_note as opportunity_note');
        } else {
            array_push($fields, 'AuditQuestionResponse.consumer_note as consumer_note', 'AuditQuestionResponse.secondary_note as secondary_note', 'AuditQuestionResponse.tertiary_note as tertiary_note', 'AuditQuestionResponse.opportunity_note as opportunity_note', 'AuditQuestionResponse.audit_trail as audit_trail');
        }

        $auditResponses = $this->CompanyAudit->AuditQuestionResponse->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
        ));

        $a1 = Hash::extract($auditResponses, '{n}.AuditQuestionResponse');
        $a2 = Hash::extract($auditResponses, '{n}.AuditQuestion');
        $a3 = Hash::extract($auditResponses, '{n}.Company');
        $a4 = Hash::extract($auditResponses, '{n}.CompanyAudit');

        $auditResponses = Hash::merge($a1, $a2, $a3, $a4);

        return $auditResponses;
    }

    /*
     * method change_company_audit_enabled_status
     * 
     * Restricted to Admins and Managers.
     * At present there are only 2 statuses; enabled and admin only, more could be added later.
     * This method allows admin users to enable or lock an audit for normal users (admin only).
     * The audit_enabled_status can then be checked when a normal user attempts to access an audit.
     * 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

    public function change_company_audit_enabled_status() {
        if ($this->request->is('post')) {
            $data = $this->request->input('json_decode');
            $this->CompanyAudit->id = $data->company_audit_id;
            if (!$this->CompanyAudit->exists($data->company_audit_id)) {
                throw new NotFoundException(__('Invalid Audit'));
            }

            if ($this->CompanyAudit->save($this->request->data)) {
                switch ($data->audit_enabled_status_id) {
                    case 1:
                        $message = 'Audit editable only by admins.<br/>And locked for normal users';
                        break;
                    case 5:
                        $message = 'Audit enabled';
                        break;
                    default:
                        break;
                }

                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => $message));
            } else {
                $errors = $this->validationErrorsToString($this->CompanyAudit->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

    /**
     * _check_audit_status method
     * 
     * Retrieves the audit_enabled_status_id for an audit
     * 
     * @param int $audit_id the id of the audit
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return int $status_id the enabled status of the audit
     */
    private function _check_audit_status($audit_id) {
        $this->CompanyAudit->recursive = -1;
        $company_audit = $this->CompanyAudit->findByid($audit_id);
        $status_id = $company_audit['CompanyAudit']['audit_enabled_status_id'];
        return $status_id;
    }

    /**
     * _find_audit method
     * 
     * Used to find the specified audit, checks if audit's company_id is === to this user's company_id
     * i.e. isOwner
     * 
     * returns company_audit array or false if not found
     * 
     * @param int $audit_id the id of the audit
     * @param int $user_company_id The User's company_id is read from the session
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array $company_audit The Company Audit data as an array  or false if not found
     */
    private function _find_audit($audit_id) {
        $user_company_id = CakeSession::read('User.company_id');
        $this->CompanyAudit->recursive = -1;
        $company_audit = $this->CompanyAudit->find('first', array(
            'conditions' => array(
                'id' => $audit_id,
                'company_id' => $user_company_id
            )
        ));

        if ($company_audit) {
            return $company_audit;
        } else {
            return false;
        }
    }

}
