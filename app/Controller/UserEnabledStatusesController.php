<?php

App::uses('AppController', 'Controller');

/**
 * UserEnabledStatuses Controller
 *
 * @property UserEnabledStatus $UserEnabledStatus
 */
class UserEnabledStatusesController extends AppController {

    var $name = 'UserEnabledStatuses';

    /**
     * index method
     * return a list of user enabled statuses 
     * - used only by admins
     * - is accessed from the user edit form at dashboard/admin/users/id
     * 
     * At this time there are only 2 statuses; enabled and disabled.
     * More could be added later if required i.e. manager access only, supplier access only etc.
     *
     * @param string $group_id The group_id is read from the Session.
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON array listing all user enabled statuses 
     */
    public function index() {
        $group_id = CakeSession::read('User.group_id');

        // if user is admin, get the list of statuses
        if (($group_id === '1') || ($group_id === '2') || ($group_id === '3')) {
            $this->UserEnabledStatus->recursive = -1;
            $user_enabled_statuses_list = $this->UserEnabledStatus->find('all');
            $user_enabled_statuses = Hash::extract($user_enabled_statuses_list, '{n}.UserEnabledStatus');
            $this->respondAsJSON(STATUS_CODE_OK, $user_enabled_statuses);
        } else {
            // just return null if the user is Not an admin or manager
            $this->respondAsJSON(STATUS_CODE_OK, NULL);
        }
    }
}
