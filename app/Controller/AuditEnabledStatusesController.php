<?php

App::uses('AppController', 'Controller');

/**
 * AuditEnabledStatuses Controller
 *
 * @property AuditEnabledStatus $AuditEnabledStatus
 */
class AuditEnabledStatusesController extends AppController {

    var $name = 'AuditEnabledStatuses';

    /**
     * index method
     * return a list of audit enable statuses - used only by admins and is accessed from the companyAuditsList and companyAuditDetails form.
     * At this time there are only 2 statuses; enabled and admin_only.
     * More could be added later if required i.e. manager access only, supplier access only etc.
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all audit_enabled_statuses
     */
    public function index() {
        $this->AuditEnabledStatus->recursive = -1;
        $audit_enabled_statuses_list = $this->AuditEnabledStatus->find('all');
        $audit_enabled_statuses = Hash::extract($audit_enabled_statuses_list, '{n}.AuditEnabledStatus');
        $this->respondAsJSON(STATUS_CODE_OK, $audit_enabled_statuses);
    }
}
