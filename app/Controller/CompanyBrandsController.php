<?php

App::uses('AppController', 'Controller');

/**
 * CompanyBrands Controller
 *
 * @property CompanyBrand $CompanyBrand
 */
class CompanyBrandsController extends AppController {

    var $name = 'CompanyBrands';

    /**
     * index method
     * The company_id is checked in the active session using AppModel::beforeFind()
     * The Brands owned by that company are returned
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all Company Brands
     */
    public function index() {
        $fields = array(
            'CompanyBrand.id AS id',
            'CompanyBrand.name AS name',
            'CompanyBrand.description AS description',
            'CompanyBrand.created AS created',
            'CompanyBrand.modified AS modified',
            'PackagingSource.name AS packaging_source_name',
            'BrandActivityType.name AS brand_activity_type_name'
        );

        $this->CompanyBrand->recursive = 0;

        $company_brands_result = $this->CompanyBrand->find('all', array(
            'fields' => $fields
        ));

        $a1 = Hash::extract($company_brands_result, '{n}.CompanyBrand');
        $a2 = Hash::extract($company_brands_result, '{n}.BrandActivityType');
        $a3 = Hash::extract($company_brands_result, '{n}.PackagingSource');

        $company_brands = Hash::merge($a1, $a2, $a3);

        $this->respondAsJSON(STATUS_CODE_OK, $company_brands);
    }

    /**
     * view method
     * 
     * Finds the brand by id and also checks the session company_id
     * to ensure the brand is only accessible to owners
     *  
     * @param int $id The id of the Company Brand
     * @param int $company_id is added to the find() method in appModel::beforeFind()
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return object json object.
     */
    public function view($id = null) {
        $this->CompanyBrand->recursive = -1;
        $company_brand = $this->CompanyBrand->findById($id);
        if ($company_brand) {
            $this->respondAsJSON(STATUS_CODE_OK, $company_brand['CompanyBrand']);
        } else {
            throw new NotFoundException(__('Brand Not Found'));
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $company_id = CakeSession::read('User.company_id');

        if ($this->request->is('post') || $this->request->is('put')) {
            $new_brand = $this->request->data;
            $new_brand['company_id'] = $company_id;

            $this->CompanyBrand->create();
            if ($this->CompanyBrand->save($new_brand)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'New Company Brand Added'));
            } else {
                $errors = $this->validationErrorsToString($this->CompanyBrand->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

    /**
     * edit method
     * 
     * Finds the brand by id and also checks the session company_id
     * to ensure the brand is only accessible to owners.
     * 
     * After above check is complete, updates existing brand with new data
     * 
     * @param int $id The id of the Company Brand
     * @param int $company_id is added to the find() method in AppModel::beforeFind()
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array array with flash message.
     */
    public function edit($id = null) {
        $this->CompanyBrand->recursive = -1;
        $company_brand = $this->CompanyBrand->findById($id);

        if ($company_brand) {
            $this->CompanyBrand->id = $id;
            if ($this->CompanyBrand->save($this->request->data)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Brand Edited'));
            } else {
                $errors = $this->validationErrorsToString($this->CompanyBrand->validationErrors);
                throw new BadRequestException(__($errors));
            }
        } else {
            throw new NotFoundException(__('Brand Not Found'));
        }
    }

    /**
     * delete method
     * 
     * AppModel:: beforeDelete() checks if current user's company_id === brand's company_id.
     * i.e. isOwner check
     * 
     * After above check is complete, deletes specified brand
     * 
     * @param int $id The id of the Company Brand
     * 
     * @uses AppModel::beforeDelete() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array array with flash message.
     */
    public function delete($id = null) {
        $this->CompanyBrand->id = $id;
        if ($this->CompanyBrand->delete($id)) {
            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Company Brand Deleted'));
        } else {
            throw new NotFoundException(__('Brand Not Found'));
        }
    }

}
