<?php

App::uses('AppController', 'Controller');

/**
 * Groups Controller
 *
 * @property Group $Group
 */
class GroupsController extends AppController {
    
    var $name = 'Groups';

    /**
     * index method
     * return a list of ACL Groups 
     * - Accessible by admins and manager only
     * - is accessed from 'dashboard/admin/users/id'.
     * 
     * Admins will see all 4 groups.
     * Managers will see 2 groups; user and supplier
     * 
     * At this time there are 4 ACL Groups; Administrators, Managers, Users and Suppliers
     * More could be added later if required.
     *
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all ACL Groups
     */
    public function index() {
        $group_id = CakeSession::read('User.group_id');

        $this->Group->recursive = -1;

        $fields = array(
            'Group.id as id',
            'Group.name as name'
        );

        if ($group_id === '1') {
            $groups_list = $this->Group->find('all', array(
                'fields' => $fields
            ));
        } elseif ($group_id === '2') {
            $conditions = array('id >' => 2);
            $groups_list = $this->Group->find('all', array(
                'fields' => $fields,
                'conditions' => $conditions
            ));
        } elseif ($group_id === '3') {
            $conditions = array('id >' => 3);
            $groups_list = $this->Group->find('all', array(
                'fields' => $fields,
                'conditions' => $conditions
            ));
        }

        $groups = Hash::extract($groups_list, '{n}.Group');
        $this->respondAsJSON(STATUS_CODE_OK, $groups);
    }

    /**
     * view method
     * 
     * Not Currently Used - 14/12/02
     * Finds the group by id
     * 
     * @param int $id The id of the Group
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array json array.
     */
//    public function view($id = null) {
//        if (!$this->Group->exists($id)) {
//            throw new NotFoundException(__('Invalid group'));
//        }
//        $options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
//        $this->set('group', $this->Group->find('first', $options));
//    }

    /**
     * add method
     * 
     * Not Currently Used - 14/12/03
     * Finds the category by id
     * 
     * @access public
     * @throws  BadRequestException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return int id of new group
     */
//    public function add() {
//        if ($this->request->is('post')) {
//            $this->Group->create();
//            if ($this->Group->save($this->request->data)) {
//                $this->flash(__('Group saved.'), array('action' => 'index'));
//            } else {
//                
//            }
//        }
//    }

    /**
     * edit method
     * 
     * Not Currently Used - 14/12/03
     * Finds the group by id
     * 
     * @access public
     * @throws  BadRequestException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return string success message or list of validation errors
     */
//    public function edit($id = null) {
//        if (!$this->Group->exists($id)) {
//            throw new NotFoundException(__('Invalid group'));
//        }
//        if ($this->request->is('post') || $this->request->is('put')) {
//            if ($this->Group->save($this->request->data)) {
//                $this->flash(__('The group has been saved.'), array('action' => 'index'));
//            } else {
//                
//            }
//        } else {
//            $options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
//            $this->request->data = $this->Group->find('first', $options);
//        }
//    }

    /**
     * delete method
     * 
     * Not Currently Used - 14/12/03
     * Finds the group by id
     * 
     * @access public
     * @throws NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return string success message or list of validation errors
     */
//    public function delete($id = null) {
//        $this->Group->id = $id;
//        if (!$this->Group->exists()) {
//            throw new NotFoundException(__('Invalid group'));
//        }
//        $this->request->onlyAllow('post', 'delete');
//        if ($this->Group->delete()) {
//            $this->flash(__('Group deleted'), array('action' => 'index'));
//        }
//        $this->flash(__('Group was not deleted'), array('action' => 'index'));
//        $this->redirect(array('action' => 'index'));
//    }
}
