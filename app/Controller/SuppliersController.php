<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property RequestHandlerComponent $RequestHandler
 */
class SuppliersController extends AppController {

    var $name = 'Suppliers';
    
    private $_count = 0;

    public function beforeFilter() {
        parent::beforeFilter();
        $this->loadModel('User');
    }

    /**
     * index method
     * Restricted to admin and manager
     * 
     * @return array of json objects
     */
    public function index() {
        $this->User->recursive = 0;

        $fields = array(
            'User.id AS id',
            'User.username AS username',
            'User.firstname AS firstname',
            'User.lastname AS lastname',
            'User.last_login AS last_login',
            'User.supplier_company AS suppliers_company',
            'Group.id AS group_id',
            'Group.name AS group_name',
            'Company.id AS company_id',
            'Company.trading_name AS company_name'
        );

        /*
         *  only show suppliers in this list.
         */
        $conditions = array(
            'group_id' => 5
        );

        /**
         * The setting required to filter, sort and limit the data returned to the client side.
         */
        $order = array();   // sort order
        $limit = null;      // items per page, no default
        $offset = 0;        // calculated by $limit * the page number
        $filter = '';       // find items based on the string entered in the search box

        if (!empty($this->request->query['filter'])) {
            $filter = '%' . $this->request->query['filter'] . '%';
            $conditions = array(
                "OR" => array(
                    'username LIKE ' => $filter,
                    'firstname LIKE ' => $filter,
                    'lastname LIKE ' => $filter,
                    'last_login LIKE ' => $filter,
                    'User.supplier_company LIKE ' => $filter,
                    'Company.trading_name LIKE ' => $filter
                )
            );
        }

        if (!empty($this->request->query['sorting'])) {
            $sorting = preg_replace(array("/{/", "/}/", '/"/'), "", $this->request->query['sorting']);
            $sorting = preg_replace("/:/", " ", $sorting);
            array_push($order, $sorting);
        }

        if (!empty($this->request->query['limit'])) {
            $limit = $this->request->query['limit'];
        }

        if (!empty($this->request->query['page']) && ($this->request->query['page'] > 0)) {
            $offset = $limit * $this->request->query['page'];
        }

        $users_result = $this->User->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $limit,
            'offset' => $offset
        ));

        $this->_set_count($conditions);

        $a1 = Hash::extract($users_result, '{n}.User');
        $a2 = Hash::extract($users_result, '{n}.Company');
        $a3 = Hash::extract($users_result, '{n}.Group');

        $users = Hash::merge($a1, $a2, $a3);

        $this->respondAsJSON(STATUS_CODE_OK, $users, $this->_count);
    }

    private function _set_count($conditions = NULL) {
        $count = $this->User->find('count', array(
            'conditions' => $conditions
        ));
        $this->_count = (int) $count;
    }

    /**
     * view_supplier_details method
     *
     * @throws NotFoundException
     * @return json object
     */
    public function view_supplier_details() {
        $supplier_id = CakeSession::read('User.id');

        if (!$this->User->exists($supplier_id)) {
            throw new NotFoundException(__('Invalid user'));
        } else {
            $this->User->recursive = 1;

            $user_result = $this->User->findById($supplier_id);
            $user_result = $user_result['User'];

            $user = array(
                'id' => $user_result['id'],
                'username' => $user_result['username'],
                'firstname' => $user_result['firstname'],
                'lastname' => $user_result['lastname'],
                'company_id' => $user_result['company_id'],
                'group_id' => $user_result['group_id']
            );

            $this->respondAsJSON(STATUS_CODE_OK, $user);
        }
    }

    /**
     * method   audit_requests 
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function audit_requests() {
        $user_id = CakeSession::read('User.id');

        $this->loadModel('AuditToSupplier');

        $conditions = array(
            'AuditToSupplier.user_id' => $user_id,
            'AuditToSupplier.request_sent' => true
        );

        $fields = array(
            'AuditToSupplier.id as id',
            'AuditToSupplier.company_audit_id AS company_audit_id',
            'AuditToSupplier.user_id AS supplier_id',
            'AuditToSupplier.request_sent AS request_sent',
            'AuditToSupplier.respond_by AS respond_by',
            'CompanyAudit.spg_audit_code AS spg_audit_code',
            'CompanyAudit.audit_description AS description',
            'Company.trading_name AS trading_name'
        );

        $this->AuditToSupplier->recursive = 0;

        $audit_requests_list = $this->AuditToSupplier->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions
        ));

        $a1 = Hash::extract($audit_requests_list, '{n}.AuditToSupplier');
        $a2 = Hash::extract($audit_requests_list, '{n}.CompanyAudit');
        $a3 = Hash::extract($audit_requests_list, '{n}.Company');

        $audit_requests = Hash::merge($a1, $a2, $a3);
        $this->respondAsJSON(STATUS_CODE_OK, $audit_requests);
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit_supplier_details() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $data = $this->request->input('json_decode');
            $id = $data->id;
            $username = $data->firstname;
            $password = $data->lastname;
        }

        $this->loadModel('User');
        $this->User->id = $id;

        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid Supplier id'));
        }

        if ($this->User->save($this->request->data)) {
            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Supplier Edited'));
        } else {
            $errors = $this->validationErrorsToString($this->User->validationErrors);
            throw new BadRequestException(__($errors));
        }
    }

}
