<?php

App::uses('AppController', 'Controller');

/**
 * AuditTemplates Controller
 *
 * @property AuditTemplate $AuditTemplate
 */
class AuditTemplatesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('RequestHandler', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->disableCache();
        if (isset($this->request->data['company_id'])) {
            $company_id = $this->request->data['company_id'];
        } else {
            $company_id = CakeSession::read('User.company_id');
        }

        if (CakeSession::read('User.company_id')) {
            $conditions = array();
            $conditions[] = array('company_id' => 1); // the template company

            $this->loadModel('CompanyAudit');
            $this->CompanyAudit->recursive = 1;

            $company_audits_list = $this->CompanyAudit->query(
                    "SELECT 
                        company_audits.id as id, 
                        company_audits.audit_date as audit_date, 
                        company_audits.spg_audit_code as spg_audit_code, 
                        company_audits.lead_auditor_name as lead_auditor_name, 
                        company_audits.company_id as company_id, 
                        company_audits.audit_enabled_status_id as audit_enabled_status_id,
                        LEFT(company_audits.audit_description,100) as shortened_description,
                        audit_enabled_statuses.status as audit_enabled_status 
                    FROM 
                        company_audits 
                        
                    LEFT JOIN 
                        audit_enabled_statuses 
                    ON 
                        audit_enabled_statuses.id=company_audits.audit_enabled_status_id 
                        
                    WHERE 
                        company_id = $company_id;"
            );

            $a1 = Hash::extract($company_audits_list, '{n}.0');
            $a2 = Hash::extract($company_audits_list, '{n}.company_audits');
            $a3 = Hash::extract($company_audits_list, '{n}.audit_enabled_statuses');
            $company_audits = Hash::merge($a1, $a2, $a3);

            $this->respondAsJSON(STATUS_CODE_OK, $company_audits);
        } else {
            $this->respondAsJSON(STATUS_CODE_UNAUTHORIZED, array('flash' => 'You are not logged in'));
        }
    }

    /*
     * new_audit_build method
     * 
     * 
     */

//    public function new_audit_template() {
//        if ($this->request->is('post')) {
//            $data = $this->request->data;
//            $categories = $data['categories'];
//
//            $this->loadModel('Abcategory');
//            $this->loadModel('Abquestion');
//            $this->loadModel('AbcategoriesAbquestion');
//            $this->loadModel('AuditToAbcategory');
//
//            $audit_to_abcategory = array();
//            $abcategory_to_abquestion = array();
//
//            /*
//             * 
//             */
//            foreach ($categories as $category) {
//                $category_id = $this->_category_exists($category);
//
//                if ($category_id === -1) {
//                    $this->Abcategory->create();
//                    if ($this->Abcategory->save($category)) {
//                        $category_id = $this->Abcategory->id;
//                    }
//                }
//
//                $audit_to_cat_association = array(
//                    'company_audit_id' => $data['audit_id'],
//                    'abcategory_id' => $category_id,
//                    'category_sort_order' => $category['sortOrder']
//                );
//
//                if ($this->_audit_to_category_exists($audit_to_cat_association) === -1) {
//                    array_push($audit_to_abcategory, $audit_to_cat_association);
//                }
//
//                foreach ($category['ab_questions'] as $question) {
//                    $question_id = $this->_question_exists($question);
//
//                    if ($question_id === -1) {
//                        $this->Abquestion->create();
//                        if ($this->Abquestion->save($question)) {
//                            $question_id = $this->Abquestion->id;
//                        }
//                    }
//
//                    $c_to_q_association = array(
//                        'abcategory_id' => $category_id,
//                        'abquestion_id' => $question_id,
//                        'question_sort_order' => $question['sortOrder']
//                    );
//
//                    if ($this->_category_to_question_exists($c_to_q_association) === -1) {
//                        array_push($abcategory_to_abquestion, $c_to_q_association);
//                    }
//                }
//            }
//
//            if (!empty($audit_to_abcategory)) {
//                if ($this->AuditToAbcategory->saveAll($audit_to_abcategory)) {
//                    $response_message['audit_to_category'] = $audit_to_abcategory;
//                } else {
//                    $response_message = array('flash' => 'Nothing to save');
//                }
//            }
//
//            if (!empty($abcategory_to_abquestion)) {
//                if ($this->AbcategoriesAbquestion->saveAll($abcategory_to_abquestion)) {
//                    $response_message['category_to_question'] = $abcategory_to_abquestion;
//                }
//            }
//
//            $this->respondAsJSON(STATUS_CODE_OK, $response_message);
//        }
//    }

    /**
     * @name _audit_to_category_exists
     * @author Patrick Ward
     * @access private
     * @param array $association
     * @return int
     * 
     * if the association between audit and category already exists then the id of the existing record is return.
     * else -1 is returned
     */
//    private function _audit_to_category_exists($association) {
//        $this->loadModel('AuditToAbcategory');
//        $this->AuditToAbcategory->recursive = -1;
//
//        $association_exists = $this->AuditToAbcategory->find('first', array(
//            'conditions' => array(
//                'AuditToAbcategory.company_audit_id' => $association['company_audit_id'],
//                'AuditToAbcategory.abcategory_id' => $association['abcategory_id']
//            ),
//        ));
//
//        if (!empty($association_exists)) {
//            return $association_exists['AuditToAbcategory']['id'];
//        } else {
//            return -1;
//        }
//    }

    /**
     * @name _category_exists
     * @author Patrick Ward
     * @access private
     * @param array $category
     * @return int
     * 
     * if a category with the same name already exists then the id of the existing record is return.
     * else -1 is returned
     */
//    private function _category_exists($category) {
//        $this->loadModel('Abcategory');
//        $this->Abcategory->recursive = -1;
//
//        $category_exists = $this->Abcategory->find('first', array(
//            'conditions' => array(
//                'Abcategory.name' => $category['name']
//            ),
//        ));
//
//        if (!empty($category_exists)) {
//            if ($category_exists['Abcategory']['name'] === $category['name']) {
//                return $category_exists['Abcategory']['id'];
//            } else {
//                return -1;
//            }
//        } else {
//            return -1;
//        }
//    }

    /**
     * @name _question_exists
     * @author Patrick Ward
     * @access private
     * @param array $question
     * @return int
     * 
     * if a question with the same text already exists then the id of the existing record is return.
     * else -1 is returned
     */
//    private function _question_exists($question) {
//        $this->loadModel('Abquestion');
//        $this->Abquestion->recursive = -1;
//
//        $question_exists = $this->Abquestion->find('first', array(
//            'conditions' => array(
//                'Abquestion.text' => $question['text']
//            ),
//        ));
//
//        if (!empty($question_exists)) {
//            if ($question_exists['Abquestion']['text'] === $question['text']) {
//                return $question_exists['Abquestion']['id'];
//            } else {
//                return -1;
//            }
//        } else {
//            return -1;
//        }
//    }

    /**
     * @name _question_exists
     * @author Patrick Ward
     * @access private
     * @param array $association
     * @return int
     * 
     * if the association between category and question already exists then the id of the existing record is return.
     * else -1 is returned
     */
//    private function _category_to_question_exists($association) {
//        $this->loadModel('AbcategoriesAbquestion');
//        $this->AbcategoriesAbquestion->recursive = -1;
//
//        $association_exists = $this->AbcategoriesAbquestion->find('first', array(
//            'conditions' => array(
//                'AbcategoriesAbquestion.abcategory_id' => $association['abcategory_id'],
//                'AbcategoriesAbquestion.abquestion_id' => $association['abquestion_id']
//            ),
//        ));
//
//        if (!empty($association_exists)) {
//            return $association_exists['AbcategoriesAbquestion']['id'];
//        } else {
//            return -1;
//        }
//    }

}
