<?php

App::uses('AppController', 'Controller');

/**
 * CompanyDocumentTypes Controller
 *
 * @property CompanyDocumentType $CompanyDocumentType
 */
class CompanyDocumentTypesController extends AppController {

    var $name = 'CompanyDocumentTypes';

    /**
     * index method
     * return a list of document types
     * 
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON array listing all Document Types
     */
    public function index() {
        $this->CompanyDocumentType->recursive = -1;

        $fields = array(
            'CompanyDocumentType.id AS id',
            'CompanyDocumentType.name AS name',
        );

        $company_document_types_list = $this->CompanyDocumentType->find('all', array(
            'fields' => $fields
        ));

        $company_document_types = Hash::extract($company_document_types_list, '{n}.CompanyDocumentType');

        $this->respondAsJSON(STATUS_CODE_OK, $company_document_types);
    }
}
