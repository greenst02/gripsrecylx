<?php

App::uses('AppController', 'Controller');

/**
 * AuditToDocuments Controller
 * 
 * Main controller for handling the association between CompanyAudits and CompanyDocuments
 * The CakePHP relationships in the models alone were not sufficient.
 * 
 * 
 * @property AuditToDocument $AuditToDocument
 */
class AuditToDocumentsController extends AppController {

    var $name = 'AuditToDocuments';
 
    /**
     * index method
     * 
     * Return a list of AuditToDocuments associated with a company.
     * On the client side, this method is called from 
     * dashboard.documents module 
     * -> DocumentToAuditsModalCtrl
     * This method is used to populate the list of audits shown in the DocumentToAuditsModal
     * 
     * @uses CompanyDocument::get_documents 
     * 
     * @param int $document_id The id of the document to check associations against.
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array of all company_audits with a true or false indicating if document is associated with each audit.
     */
    public function index() {

        $this->loadModel('CompanyAudit');
        $company_id = CakeSession::read('User.company_id');

        if ($this->request->is('post')) {
            $document_id = $this->request->data['document_id'];
        }

        $query_result = $this->CompanyAudit->query(
                "SELECT
                    company_audits.id AS company_audit_id, 
                    company_audits.spg_audit_code AS spg_audit_code,
                    (
                        SELECT 
                            audit_to_document.id from audit_to_document
                        WHERE 
                            audit_to_document.company_audit_id=company_audits.id and audit_to_document.company_document_id=$document_id) AS audit_to_doc_id, $document_id AS company_document_id
                        FROM
                            company_audits
                        WHERE
                            company_audits.company_id = $company_id
                "
        );

        $a1 = Hash::extract($query_result, '{n}.company_audits');
        $a2 = Hash::extract($query_result, '{n}.0');

        $audits_to_docs = Hash::merge($a1, $a2);

        $audits_to_docs_update = array();
        foreach ($audits_to_docs as $audit_to_doc) {
            if ($audit_to_doc['audit_to_doc_id']) {
                $audit_to_doc['checked'] = 1;
                array_push($audits_to_docs_update, $audit_to_doc);
            } else {
                $audit_to_doc['checked'] = 0;
                array_push($audits_to_docs_update, $audit_to_doc);
            }
        }

        $this->respondAsJSON(STATUS_CODE_OK, $audits_to_docs_update);
    }
    
    /**
     * modify_document_to_audits method
     * 
     * On the client side, this method is called from 
     * dashboard.documents module 
     * -> DocumentToAuditsModalCtrl
     * This method is called when the user clicks "Attach To Audit".
     * It updates calls other methods to check AuditToDocuments relationship exists and 
     * create or delete the AuditToDocument relationships depending on submitted data.
     * 
     * @uses AuditToDocument::_audit_to_document_exists 
     * @uses AuditToDocument::_save_many 
     * @uses AuditToDocument::_delete_many 
     * 
     * @param int $document_id The id of the document to check associations against.
     * @param array $audits An array with the audit_id as the array key and a value of true or false
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array array with flash message stating 'Associated documents updated'
     */
    public function modify_document_to_audits() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $document_to_audits = $this->request->data;

            $document_id = $document_to_audits['company_document_id'];
            $audits = $document_to_audits['doc_to_audits'];

            $create_many = array();
            $delete_many = array();

            $company_id = CakeSession::read('User.company_id');

            foreach ($audits as $audit_id => $val) {
                $exists = $this->_audit_to_document_exists($document_id, $audit_id);
                if ($exists) {
                    if (!$val) {
                        array_push($delete_many, $exists);
                    }
                } else {
                    if ($val) {
                        $audit_to_document = array(
                            'company_id' => $company_id,
                            'company_audit_id' => $audit_id,
                            'company_document_id' => $document_id
                        );
                        array_push($create_many, $audit_to_document);
                    }
                }
            }

            if (!$create_many && !$delete_many) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'No changes detected'));
            } else {
                if ($create_many) {
                    $this->_save_many($create_many);
                }
                if ($delete_many) {
                    $this->_delete_many($delete_many);
                }
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Associated documents updated'));
            }
        }
    }
    
    /**
     * _audit_to_document_exists method
     * 
     * called by AuditToDocument::modify_document_to_audits.
     * Checks if a relationship exists and retruns true or false
     * 
     * @see AuditToDocument::modify_document_to_audits
     * 
     * @param int $document_id The id of the document to check associations against.
     * @param int $audit_id The id of the audit to check associations against.
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return boolean
     */
    private function _audit_to_document_exists($document_id, $audit_id) {
        $this->AuditToDocument->recursive = -1;
        $conditions = array(
            'company_audit_id' => $audit_id,
            'company_document_id' => $document_id
        );

        $result = $this->AuditToDocument->find('first', array(
            'conditions' => $conditions,
            'fields' => 'id'
        ));

        if ($result) {
            $id = $result['AuditToDocument']['id'];
        }

        return ($result) ? $id : false;
    }
    
    /**
     * _save_many method
     * 
     * called by AuditToDocument::modify_document_to_audits.
     * creates relationships in AuditToDocument
     * 
     * @see AuditToDocument::modify_document_to_audits
     * @uses AuditToDocument::saveMany
     * 
     * @param array $create_many an array containing document_id(s), audit(s) and the company_id
     * @access private
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     */
    private function _save_many($create_many) {
        return $this->AuditToDocument->saveMany($create_many);
    }
    
    /**
     * _delete_many method
     * 
     * called by AuditToDocument::modify_document_to_audits.
     * deletes relationships in AuditToDocument
     * 
     * @see AuditToDocument::modify_document_to_audits
     * @uses AuditToDocument::delete
     * 
     * @param array $delete_many an array containing document_id(s), audit(s) and the company_id
     * @access private
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     */
    private function _delete_many($delete_many) {
        foreach ($delete_many as $id) {
            $this->AuditToDocument->delete($id);
        }
    }
    
    /**
     * remove_doc_from_single_audit method
     * 
     * On the client side, this method is called from 
     * dashboard.companyAudits.audit.auditDocuments module 
     * -> AuditDocumentsListCtrl
     * 
     * This method is called when the user clicks delete from the auditCompanyDocumentsList.
     * A confirmation box is displayed.
     * On confirmation, this method will remove the AuditToDocument relationship for the selected document.
     * 
     * @uses AuditToDocument::delete
     * 
     * @param array An array containing the document_id and the audit_id
     * @param int $company_id The company_id is taken from the session.
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array array with flash message stating 'Document removed from audit'
     */
    public function remove_doc_from_single_audit() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $document_to_audits = $this->request->data;

            $document_id = $document_to_audits['company_document_id'];
            $audit_id = $document_to_audits['audit_id'];
            $company_id = CakeSession::read('User.company_id');

            $this->AuditToDocument->recursive = -1;
            $audit_to_documnent = $this->AuditToDocument->find('first', array(
                'conditions' => array(
                    'AuditToDocument.company_document_id' => $document_id,
                    'AuditToDocument.company_audit_id' => $audit_id,
                    'AuditToDocument.company_id' => $company_id
                ),
                'fields' => array('AuditToDocument.id')
            ));
            
            if ($this->AuditToDocument->delete($audit_to_documnent['AuditToDocument']['id'])) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Document removed from audit.'));
            };
        }
    }
}
