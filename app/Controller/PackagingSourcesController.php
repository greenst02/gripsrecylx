<?php

App::uses('AppController', 'Controller');

/**
 * PackagingSources Controller
 *
 * @property PackagingSource $PackagingSource
 */
class PackagingSourcesController extends AppController {

    var $name = 'PackagingSources';

    /**
     * index method
     * return a list of Packaging Sources
     * - is accessed from the companyBrand Edit form.
     * 
     * @param string $company_id The company_id is read from the Session.
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON array listing all Packaging Sources
     */
    public function index() {
        $this->PackagingSource->recursive = -1;
        $packaging_sources_list = $this->PackagingSource->find('all');
        $packaging_sources = Hash::extract($packaging_sources_list, '{n}.PackagingSource');
        $this->respondAsJSON(STATUS_CODE_OK, $packaging_sources);
    }
}
