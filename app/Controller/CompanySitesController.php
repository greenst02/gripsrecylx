<?php

App::uses('AppController', 'Controller');

/**
 * CompanySites Controller
 *
 * @property CompanySite $CompanySite
 */
class CompanySitesController extends AppController {

    var $name = 'CompanySites';

    /**
     * index method
     * The company_id is checked in the active session using AppModel::beforeFind()
     * The Sites owned by that company are returned
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all Company Sites
     */
    public function index() {
        $this->CompanySite->recursive = 0;

        $fields = array(
            'CompanySite.id as id',
            'CompanySite.name as name',
            'CompanySite.address1 as address1',
            'CompanySite.address2 as address2',
            'CompanySite.address3 as address3',
            'CompanySite.address4 as address4',
            'CompanySite.address5 as address5',
            'CompanySite.postcode as postcode',
            'AustralianState.name as state_name'
        );

        $company_sites_list = $this->CompanySite->find('all', array(
            'fields' => $fields
        ));

        $a1 = Hash::extract($company_sites_list, '{n}.CompanySite');
        $a2 = Hash::extract($company_sites_list, '{n}.AustralianState');

        $company_sites = Hash::merge($a1, $a2);

        $this->respondAsJSON(STATUS_CODE_OK, $company_sites);
    }

    /**
     * view method
     * 
     * Finds the site by id and also checks the session company_id
     * to ensure the site is only accessible to owners
     *  
     * @param int $id The id of the Company Site
     * @param int $company_id is added to the find() method in AppModel::beforeFind()
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return object json object.
     */
    public function view($id = null) {
        $this->CompanySite->recursive = -1;
        $company_site = $this->CompanySite->findById($id);
        if ($company_site) {
            $this->respondAsJSON(STATUS_CODE_OK, $company_site['CompanySite']);
        } else {
            throw new NotFoundException(__('Site Not Found'));
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post') || $this->request->is('put')) {

            $new_site = $this->request->data;
            $new_site['company_id'] = CakeSession::read('User.company_id');

            $this->CompanySite->create();
            if ($this->CompanySite->save($new_site)) {

                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'New Company Site Added'));
            } else {
                $errors = $this->validationErrorsToString($this->CompanySite->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

    /**
     * edit method
     * 
     * Finds the site by id and also checks the session company_id
     * to ensure the site is only accessible to owners.
     * 
     * After above check is complete, updates existing site with new data
     * 
     * @param int $id The id of the Company Site
     * @param int $company_id is added to the find() method in AppModel::beforeFind()
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array array with flash message.
     */
    public function edit($id = null) {
        $this->CompanySite->recursive = -1;
        $company_site = $this->CompanySite->findById($id);

        if ($company_site) {
            $this->CompanySite->id = $id;
            if ($this->CompanySite->save($this->request->data)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Site Edited'));
            } else {
                $errors = $this->validationErrorsToString($this->CompanySite->validationErrors);
                throw new BadRequestException(__($errors));
            }
        } else {
            throw new NotFoundException(__('Site Not Found'));
        }
    }

    /**
     * delete method
     * 
     * AppModel:: beforeDelete() checks if current user's company_id === site's company_id.
     * i.e. isOwner check
     * 
     * After above check is complete, deletes specified site
     * 
     * @param int $id The id of the Company Site
     * 
     * @uses AppModel::beforeDelete() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array array with flash message.
     */
    public function delete($id = null) {
        $this->CompanySite->id = $id;
        if ($this->CompanySite->delete($id)) {
            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Company Site Deleted'));
        } else {
            throw new NotFoundException(__('Site Not Found'));
        }
    }

}
