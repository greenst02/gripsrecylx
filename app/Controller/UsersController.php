<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
//        $user = $this->Auth->isAuthorized($this->Session->read('User'));
        $this->Auth->allow('login', 'logout', 'forgot_password', 'reset_password', 'change_password');
//        $this->Auth->autoRedirct = false;
    }

    private $_count = 0;

    public function login() {

        /*
         * User credentials submitted as json object.
         * Credentials need to be converted before they can be checked
         * Auth component doesn't work as normal when login details are sent as json object
         */

//        cors($this->request, 'http://localhost:4200'); 
        $data = $this->request->input('json_decode');
        $username = $data->username;
        $password = $data->password;

        $hash = $this->Auth->password($password);

        $this->User->recursive = 0;
        $check = $this->User->find('first', array(
            'conditions' => array(
                'username' => $username,
                'password' => $hash
            ),
            'fields' => array(
                'User.username',
                'User.password',
                'User.group_id',
                'User.company_id',
                'User.user_enabled_status_id',
                'Company.company_enabled_status_id'
            )
        ));

        if ($check) {
//            if ($check['User']['id'] === $this->Session->read('User.id')) {
//
//                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'You are already logged in.'));
//            } 
            if (!($check['User']['group_id'] === '1') && $check['Company']['company_enabled_status_id'] === '2') {

                /*
                 *  if user is not admin
                 *  check if the company is disabled
                 */
                $message = 'The company you are attempting to access is currently disabled on our system.';
                $this->respondAsJSON(STATUS_CODE_BAD_REQUEST, array('disabledFlashMessage' => $message));
            } elseif (!($check['User']['group_id'] === '1') && !($check['User']['user_enabled_status_id'] === '1')) {
                /*
                 *  if user is not admin
                 *  check if the user is disabled
                 */
                $message = 'Your user account is currently disabled on our system.';
                $this->respondAsJSON(STATUS_CODE_BAD_REQUEST, array('disabledFlashMessage' => $message));
            } else {
                $this->Session->write(
                        'User', array(
                    'id' => $check['User']['id'],
                    'username' => $check['User']['username'],
                    'group_id' => $check['User']['group_id'],
                    'company_id' => $check['User']['company_id']
                        )
                );

                /*
                 * $role is for client side ACL routing set up.
                 * 
                 * The group_id held in the db is not the same as client side 'bitMask' accessLevels.
                 * It might be good to bring these into line - especially if more accessLevels are required at a later date.
                 * 
                 * anonymous = 'bitMask' => 1, 'title' => anonymous ---- No group id in db
                 * user = 'bitMask' => 2, 'title' => user           ---- Group id in db is 3
                 * admin = 'bitMask' => 4, 'title' => admin         ---- Group id in db is 1
                 * supplier = 'bitMask' => 8, 'title' => supplier   ---- Group id in db is 4
                 * 
                 */
                $role = array();
                switch ($check['User']['group_id']) {
                    case $check['User']['group_id'] = 1;
                        $role = array('bitMask' => 1, 'title' => 'admin');
                        break;
                    case $check['User']['group_id'] = 2;
                        $role = array('bitMask' => 2, 'title' => 'manager');
                        break;
                    case $check['User']['group_id'] = 3;
                        $role = array('bitMask' => 4, 'title' => 'company_manager');
                        break;
                    case $check['User']['group_id'] = 4;
                        $role = array('bitMask' => 8, 'title' => 'company_employee');
                        break;
                    case $check['User']['group_id'] = 5;
                        $role = array('bitMask' => 16, 'title' => 'supplier');
                        break;
                }

                $this->User->id = $this->Auth->user('id'); // target correct record
                $this->User->saveField('last_login', date(DATE_ATOM)); // save login time

                $userData = array('username' => $check['User']['username'],
                    'role' => $role);

                $this->respondAsJSON(STATUS_CODE_OK, $userData);
            }
        } else {
            /*
             *  required a different status code from the usual 401 to work with the 
             *  login dialog that pops up when the session expires.
             */
            $this->response->type('json');
            $this->response->statusCode(400);
            $this->response->body(json_encode(
                            array('flash' => 'Invalid Username or Password.')));
            $this->response->send();
            $this->_stop();
        }
    }

    public function logout() {

        if ($this->request->is('post')) {

            $this->Auth->logout();
            $this->Session->destroy();

            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'You are logged out.'));
        }
    }

    /**
     * index method
     * Restricted to admin and manager
     * 
     * @return array of json objects
     */
    public function index() {
        $group_id = CakeSession::read('User.group_id');
        $this->User->recursive = 0;

        $fields = array(
            'User.id AS id',
            'User.username AS username',
            'User.firstname AS firstname',
            'User.lastname AS lastname',
            'User.last_login AS last_login',
//                'User.user_enabled_status_id AS user_enabled_status_id',
            'Group.id AS group_id',
            'Group.name AS group_name',
            'Company.id AS company_id',
            'Company.trading_name AS company_name'
        );

        /*
         *  do not show suppliers in this list.
         */
        $conditions = array(
            'group_id !=' => 5
        );

        // if manager, do not show admins or managers.
        if ($group_id > 1) {
            array_push($conditions, array('group_id >' => 2));
        }

        // if company_manager, only show users from within company
        if ($group_id === '3') {
            $company_id = CakeSession::read('User.company_id');
            array_push($conditions, array('company_id' => $company_id));
        }

        /**
         * The setting required to filter, sort and limit the data returned to the client side.
         */
        $order = array();   // sort order
        $limit = null;        // items per page, default 25
        $offset = 0;        // calculated by $limit * the page number
        $filter = '';       // find items based on the string entered in the search box

        if (!empty($this->request->query['filter'])) {
            $filter = '%' . $this->request->query['filter'] . '%';
            $conditions = array(
                "OR" => array(
                    'username LIKE ' => $filter,
                    'firstname LIKE ' => $filter,
                    'lastname LIKE ' => $filter,
                    'last_login LIKE ' => $filter,
                    'Group.name LIKE ' => $filter,
                    'Company.trading_name LIKE ' => $filter
                )
            );
        }

        if (!empty($this->request->query['sorting'])) {
            $sorting = preg_replace(array("/{/", "/}/", '/"/'), "", $this->request->query['sorting']);
            $sorting = preg_replace("/:/", " ", $sorting);
            array_push($order, $sorting);
        }

        if (!empty($this->request->query['limit'])) {
            $limit = $this->request->query['limit'];
        }

        if (!empty($this->request->query['page']) && ($this->request->query['page'] > 0)) {
            $offset = $limit * $this->request->query['page'];
        }

        $users_result = $this->User->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $limit,
            'offset' => $offset
        ));

        $this->_set_count($conditions);

        $a1 = Hash::extract($users_result, '{n}.User');
        $a2 = Hash::extract($users_result, '{n}.Group');
        $a3 = Hash::extract($users_result, '{n}.Company');

        $users = Hash::merge($a1, $a2, $a3);

        $this->respondAsJSON(STATUS_CODE_OK, $users, $this->_count);
    }

    private function _set_count($conditions = NULL) {
        $count = $this->User->find('count', array(
            'conditions' => $conditions
        ));
        $this->_count = (int) $count;
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $group_id = CakeSession::read('User.group_id');
        $company_id = CakeSession::read('User.company_id');

        $this->User->recursive = -1;

        $fields = array(
            'User.id AS id',
            'User.username AS username',
            'User.firstname AS firstname',
            'User.lastname AS lastname',
            'User.user_enabled_status_id AS user_enabled_status_id',
            'User.group_id AS group_id',
            'User.company_id AS company_id'
        );

        /*
         *  do not show suppliers in this list.
         */
        $conditions = array(
            'id' => $id
        );

        // if manager, do not allow access to admins or managers.
        if ($group_id > 1) {
            array_push($conditions, array('group_id >' => 2));
        }

        // if company_manger, only allow acces to users of same company.
        if ($group_id === '3') {
            array_push($conditions, array('company_id' => $company_id));
        }

        $user_result = $this->User->find('first', array(
            'fields' => $fields,
            'conditions' => $conditions
        ));

        $user_result = $user_result['User'];

        $this->respondAsJSON(STATUS_CODE_OK, $user_result);
    }

    /**
     * Add a user of any type to the system.
     * Sends an email to the new user with a link to the change password page
     * 
     * This method is restricted to Admin Users by Acl
     *
     * @return void
     */

    /**
     * add method
     * 
     * 1. retrieve data from client side
     * 2. creates user
     * 3. uses User::sendEmail to send an email to the new user
     * 
     * @uses User::sendEmail
     * 
     * @param array $new_user array of username, firstname, lastname, company and group
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return Success Message, Failure Message
     */
    public function add() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $new_user = $this->request->data['new_user'];
            $email_subject = $this->request->data['new_user_email_subject'];

            //set flag to send email to new user
            $send_email = (isset($this->request->data['new_user_send_email'])) ? true : false;

            // if company_manager the company_id will not be supplied from client side.
            $group_id = CakeSession::read('User.group_id');
            if ($group_id === '3') {
                $company_id = CakeSession::read('User.company_id');
                $new_user['company_id'] = $company_id;
            }

            $view_variables['message'] = $this->request->data['new_user_email_body'];
            $view_variables['key'] = $this->User->createTokenParams($new_user);
            $view_variables['url'] = Router::url('/', true) . 'app/webroot/bin/#/updatePassword/' . $view_variables['key'];
            $new_user['tokenhash'] = $view_variables['key'];
            //set a random password for security reasons.  User will reset on setup.
            $new_user['password'] = substr($view_variables['key'], 1, 40);

            $this->User->create();
            if ($this->User->save($new_user)) {
                //blind copy keith when a new user setup.

                if($send_email){
                    $this->User->sendEmail('adduser', 'spg@greenstreets.ie', $new_user['username'], $email_subject, $view_variables, NULL, array('keith.burton@greenstreets.ie'));
                    $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'An email has been sent to ' . $new_user['username']));
                } else {

                    $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'New user saved.'));

                }


            } else {
                $errors = $this->validationErrorsToString($this->User->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->User->id = $id;

        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user id'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            Configure::write('debug', 0);
            $this->disableCache();

            if ($this->User->save($this->request->data)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'User Saved.'));
            } else {
                $errors = $this->validationErrorsToString($this->User->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {

        throw new NotImplementedException(__('TO DO: User -> View'));

//        $this->User->id = $id;
//
//        if (!$this->User->exists($id)) {
//            throw new NotFoundException(__('Invalid user id'));
//        }
//        if ($this->request->is('delete')) {
//            Configure::write('debug', 0);
//            $this->disableCache();
//
//            if ($this->User->delete($id)) {
//                $message = 'Deleted';
//            } else {
//                $message = 'Error';
//            }
//            $this->set(array(
//                'message' => $message,
//                '_serialize' => array('message')
//            ));
//        }
    }

    /*
     * Used when a user has forgotten their password.
     * Also used to send a reset link to Suppliers when they have been requested to respond to an Audit
     * 
     * forgot_password method
     *
     * @throws NotFoundException
     * @param string $username
     * @return json object
     */

    function forgot_password() {
        if ($this->request->is('post')) {
            if (!empty($this->params->params['supplier_details'])) {
                /*
                 * $this->params->params used when this method is called from AuditQuestionResponsesController->create_blank_responses()
                 */
                $username = $this->params->params['supplier_details']['username'];
            } else {
                /*
                 * User credentials submitted as json object from the forgot password form, in this case only a username is submitted.
                 */
                $data = $this->request->input('json_decode');
                $username = $data->username;
            }

            if ($username) {
                $user_result = $this->User->find('first', array('conditions' => array('User.username' => $username)));

                if ($user_result) {
                    $view_variables['key'] = $this->User->createTokenParams($user_result);
                    $view_variables['message'] = '';
                    $view_variables['url'] = Router::url('/', true) . 'app/webroot/bin/#/updatePassword/' . $view_variables['key'];

                    $user_result['User']['tokenhash'] = $view_variables['key'];
                    $this->User->id = $user_result['User']['id'];
                    if ($this->User->saveField('tokenhash', $user_result['User']['tokenhash'])) {
                        $this->User->sendEmail('resetpw', 'spg@greenstreets.ie', $user_result['User']['username'], 'Reset Your spg.greenstreets.ie Password', $view_variables);

                        if (!empty($this->params->params['supplier_details'])) {
                            // This prevents this method from sending json response when it is called from AuditQuestionResponsesController->create_blank_responses()
                            echo 'ok';
                        } else {
                            /*
                             * usual response when a form is used to input data
                             */
                            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Check your email to reset your password'));
                        }
                    } else {
                        throw new BadRequestException(__('Error generating Reset Link'));
                    }
                } else {
                    throw new BadRequestException(__('The email provided is not registered with this system.'));
                }
            } else {
                throw new BadRequestException(__('Please provide your email address that you used to register with us'));
            }
        }
    }

    /*
     * Used when a user has clicked on the link sent by forgot_password method.
     * Also be used to get new users to reset their passwords
     *
     * @param string $token
     * @return json object
     */

    function reset_password($token = null) {
        $data = $this->request->input('json_decode');
        $password = $data->password;
        $password_confirm = $data->password_confirm;

        $token = $this->request->query['token'];

        if ($password === $password_confirm) {
            $password = $this->Auth->password($password);

            $this->User->recursive = -1;

            $u = $this->User->find('first', array(
                'fields' => array('id', 'tokenhash'),
                'conditions' => array('tokenhash' => $token)
                    )
            );

            if (!empty($u)) {
                $this->User->id = $u['User']['id'];
                $this->User->set(array(
                    'tokenhash' => null,
                    'password' => $password,
                ));

                if ($this->User->save()) {
                    $message = array('flash' => 'Your password has been updated.');
                    $this->respondAsJSON(STATUS_CODE_OK, $message);
                } else {
                    $errors = $this->validationErrorsToString($this->User->validationErrors);
                    throw new BadRequestException(__($errors));
                }
            } else {
                throw new BadRequestException(__('Your token has expired'));
            }
        } else {
            throw new BadRequestException(__('Password and confirm password do not match'));
        }
    }

    public function change_password() {
        //check if form data submitted
        if (!empty($this->request->data)) {
            $password = $this->Auth->password($this->request->data['User']['pwd']);
            $fields = array(
                'User.password' => "'" . $password . "'",
                'User.tokenhash' => null
            );

            $conditions = array(
                'User.id' => $this->request->data['User']['id'],
                'User.tokenhash' => $this->request->data['User']['tokenhash'],
            );

            if ($this->User->updateAll($fields, $conditions)) {
                $this->Session->setFlash(__('Password Updated', true));
                $this->redirect(Router::url('/', true) . 'app/webroot/dist/gripsrecylx/');
            } else {
                $this->Session->setFlash(__('Your password could not be updated. Please, try again.', true));
            }
        }
    }

    /*
     * method change_user_enabled_status
     * 
     * Restricted to Admin.
     * At present there are only 2 statuses; enabled and disabled, more could be added later.
     * This method allows admin users to disable / enable a user.
     * The enabled_status can then be checked when a normal user is logging in.
     * 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

    public function change_user_enabled_status() {
        if ($this->request->is('post')) {
            $data = $this->request->input('json_decode');

            $this->User->id = $data->user_id;

            if (!$this->User->exists($data->user_id)) {
                throw new NotFoundException(__('Invalid User'));
            }

            if ($this->User->save($this->request->data)) {
                switch ($data->user_enabled_status_id) {
                    case 1:
                        $message = 'User enabled';
                        break;
                    case 2:
                        $message = 'User disabled';
                        break;
                    default:
                        break;
                }

                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => $message));
            } else {
                $errors = $this->validationErrorsToString($this->User->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

}
