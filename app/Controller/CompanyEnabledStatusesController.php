<?php

App::uses('AppController', 'Controller');

/**
 * CompanyEnabledStatuses Controller
 *
 * @property CompanyEnabledStatus $CompanyEnabledStatus
 */
class CompanyEnabledStatusesController extends AppController {

    var $name = 'CompanyEnabledStatuses';

    /**
     * index method
     * return a list of company enabled statuses 
     * - used only by admins
     * - is accessed from the companyDetails form and from the companies list
     * 
     * At this time there are only 2 statuses; enabled and disabled.
     * More could be added later if required i.e. manager access only, supplier access only etc.
     *
     * @param string $company_id The company_id is read from the Session.
     * @param string $group_id The group_id is read from the Session.
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON array listing all company enabled statuses 
     */
    public function index() {
        $group_id = CakeSession::read('User.group_id');

        // user logged in
        // if user is admin, get the list of statuses
        if (($group_id === '1') || ($group_id === '2')) {
            $this->CompanyEnabledStatus->recursive = -1;
            $company_enabled_statuses_list = $this->CompanyEnabledStatus->find('all');
            $company_enabled_statuses = Hash::extract($company_enabled_statuses_list, '{n}.CompanyEnabledStatus');
            $this->respondAsJSON(STATUS_CODE_OK, $company_enabled_statuses);
        } else {
            // just return null if the user is Not an admin
            $this->respondAsJSON(STATUS_CODE_OK, NULL);
        }
    }

}
