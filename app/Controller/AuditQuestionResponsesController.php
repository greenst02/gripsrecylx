<?php

App::uses('AppController', 'Controller');

/**
 * AuditQuestionResponses Controller
 * 
 * @property AuditQuestionResponse $AuditQuestionResponse
 */
class AuditQuestionResponsesController extends AppController {

    var $name = 'AuditQuestionResponses';

    /**
     * all_supplier_responses_to_audit_questions method
     * 
     * all_supplier_responses_to_audit_questions - all responses by all suppliers to a all questions
     * Responses can be filtered by response type and audit_to_question id
     * Called by dashboard/companyAudits/audit/id/auditQuestions/id
     * An additional query string paramater added to filter responses by responseType i.e. primary, secondary etc.
     * Default (if no responseType is set) is to return all responseTypes
     * 
     * Returns null if the current user is a supplier.
     * 
     * @param int $audit_id The id of the Audit 
     * @param int $company_id The current user's company_id read from the session
     * @param int $group_id The current user's group_id read from the session
     * @param int $responseType The type of response to return representated as an int (primary = 1, secondary = 2 etc.)
     * @param int $auditToQuestionID The id of the audit_to_question, filter can be applied using the AuditAnalysis tab
     * 
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @access public
     * @throws  STATUS_CODE_UNAUTHORIZED
     * @version 0.0.1
     * @return array Array of json objects
     */
    public function all_responses_to_audit_questions($audit_id = null) {
        $responseType = $this->request->query('responseType');
        $auditToQuestionID = $this->request->query('auditToQuestionID');

        $supplierOnly = $this->request->query('supplierOnly');
        $responseType = $this->request->query('responseType');
        $audit_to_question_id = $this->request->query('audit_to_question_id');

        $company_id = CakeSession::read('User.company_id');
        $group_id = CakeSession::read('User.group_id');

        if ($group_id === '1' || ($group_id === '2') || ($group_id === '3') || ($group_id === '4')) {

            /**
             * need to explicitly check for null as using switch would give 0
             * if null return all response fields otherwise return based on responseType
             */
         //     if ($responseType === null) {
         //          $responses_result = $this->AuditQuestionResponse->getAllSupplierResponses($audit_id, $auditToQuestionID);
         //     } else {
         //          $responses_result = $this->AuditQuestionResponse->getSupplierResponsesByType($audit_id, $auditToQuestionID, $responseType);
         //     }
             /* default responseType to 0(primary)
             */
            /**
             * TEMPORY TO ENABLE OPPORTUNITES TAB.
             * The $responseType filter is not being passed in from the opportunities tab and instead is set here
             */
        //    if ($responseType === null) {
       //         $responseType = 3;
      //      }

  /*          echo $company_id.'<br/>';
            echo $audit_id.'<br/>';
            echo $audit_to_question_id.'<br/>';
            echo $responseType.'<br/>';
            echo $supplierOnly.'<br/>';
*/
            $responses_result = $this->AuditQuestionResponse->getResponsesByType($company_id, $audit_id, $audit_to_question_id, $responseType, $supplierOnly);
/*            echo pr($responses_result);
            die;

  /*          echo pr($responses_result);
            die;*/
            /**
             * Flatten the objects so that the data is better formated for use on JS side.
             * i.e. 
             * before ->
             * AuditQuestionResponse: {
             *      response_id: "14288",
             *      response_created: "2013-10-04 17:30:36",
             *      response_supplier_id: "39"
             * },
             * User: {
             *      response_supplier_firstname: "Jeffs",
             *      response_supplier_lastname: "MacIntyre"
             * }
             * 
             * after ->
             * response_id: "14288",
             * response_created: "2013-10-04 17:30:36",
             * response_supplier_id: "39",
             * response_supplier_firstname: "Jeffs",
             * response_supplier_lastname: "MacIntyre" 
             */
            $a1 = Hash::extract($responses_result, '{n}.AuditQuestionResponse');
            $a2 = Hash::extract($responses_result, '{n}.AuditQuestion.AuditQuestionCategory');
            $a3 = Hash::extract($responses_result, '{n}.AuditQuestion');
            $a4 = Hash::extract($responses_result, '{n}.User');

            $responses = Hash::merge($a1, $a2, $a3, $a4);

            $cleaned_responses = array();
            foreach ($responses as $response) {
                $response = $this->AuditQuestionResponse->htmlDecodeResponse($response);
                $response['category_name'] = $response['name'];
                $response['response_opportunity_note'] = trim(strip_tags($response['response_opportunity_note']));
                unset($response['AuditQuestionCategory'], $response['AuditToQuestion'], $response['name'], $response['id'], $response['sort_order'], $response['created'], $response['modified'], $response['audit_question_category_type_id'], $response['audit_question_category_sub_type_id'], $response['open'], $response['supplier']);
                array_push($cleaned_responses, $response);
            }

            $this->respondAsJSON(STATUS_CODE_OK, $cleaned_responses);
        } else {
            /**
             *  if the current user is a supplier, I don't want send back the json data of responses.
             *  But I don't want to send an error either because it will break the client side.
             */
            $this->respondAsJSON(STATUS_CODE_OK, NULL);
        }
    }

    /**
     * all_company_responses_to_audit_questions method
     * 
     * all_company_responses_to_audit_questions - all responses by the company to all audit questions
     * 
     * Called by dashboard/companyAudits/audit/id/auditQuestions
     * Called by dashboard/companyAudits/audit/id/auditOverview
     * 
     * Returns null if the current user is a supplier, this is so that suppliers can use the same js and html pages without the client side breaking.
     * 
     * @param int $audit_id The id of the Audit 
     * @param int $company_id The current user's company_id read from the session
     * @param int $group_id The current user's group_id read from the session
     * 
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @access public
     * @version 0.0.1
     * @return array Array of json objects
     */
    public function all_company_responses_to_audit_questions($audit_id = null) {

        $company_id = CakeSession::read('User.company_id');
        $group_id = CakeSession::read('User.group_id');

        if ($group_id === '1' || ($group_id === '2') || ($group_id === '3') || ($group_id === '4')) {
            $company_responses = $this->AuditQuestionResponse->getAllCompanyResponses($company_id, $audit_id);
            $this->respondAsJSON(STATUS_CODE_OK, $company_responses);
        } else {
            /*
             *  if the current user is a supplier, I don't want send back the json data of responses.
             *  But I don't want to send an error either because it will break the client side.
             */
            $this->respondAsJSON(STATUS_CODE_OK, NULL);
        }
    }

    /**
     * list_of_questions_that_have_been_responded_to method
     * 
     * audit questions section filter on the audit analysis page.
     * Will work off of audit_to_question_id so this should continue to work 
     * even if custom questions are added later.
     * 
     * 1. get id's of all audit_to_question ids where some response not is not empty.
     * 2. use that array to retrieve the question details.
     * 3. remove duplicate questions if there are any
     * 4. send list of unique question to the client for use in question dropdown filter on the audit analysis page
     * 5. the question selection filter can then been set up as a filter in the same way as response selection    
     * 
     * @param int $audit_id The id of the Audit 
     * @param int $company_id The current user's company_id read from the session
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @access public
     * @version 0.0.1
     * @return array Array of json objects
     */
    public function list_of_questions_that_have_been_responded_to($audit_id = null) {
        $company_id = CakeSession::read('User.company_id');


        
        //refactored to work with model method.

        $questions_with_responses = $this->AuditQuestionResponse->findQuestionsWithResponses($company_id,$audit_id);

        $a1 = Hash::extract($questions_with_responses, '{n}.AuditQuestion');
        $a2 = Hash::extract($questions_with_responses, '{n}.AuditToQuestion');
        $questions_with_responses = Hash::merge($a1, $a2);

        $this->respondAsJSON(STATUS_CODE_OK, $questions_with_responses);
    }

    /**
     * response_to_single_question_by_company method
     * 
     * used by the company, this method populates the question_response form if the company has 
     * already entered a response.
     * 
     * Returns null if the current user is a supplier, this is so that suppliers can use the same js and html pages without the client side breaking.
     * 
     * @param int $audit_to_question_id The audit_to_question_id to find a response for 
     * @param int $company_id The current user's company_id read from the session
     * @param int $group_id The current user's group_id read from the session
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @access public
     * @version 0.0.1
     * @return json object
     */
    public function response_to_single_question_by_company($audit_to_question_id = NULL) {

        $this->disableCache();
        $company_id = CakeSession::read('User.company_id');
        $group_id = CakeSession::read('User.group_id');

        // this data is used by admin, manager or normal user


        if ($group_id === '1' || ($group_id === '2') || ($group_id === '3') || ($group_id === '4')) {

            $company_response = $this->AuditQuestionResponse->findResponseToQuestion($audit_to_question_id,
                                                                                    $company_id,
                                                                                    null);

		//	echo pr($company_response);

            if (isset($company_response['AuditQuestion']['audit_question_category_type_id'])) {

                $company_response['AuditQuestionResponse']['audit_question_category_type_id']=$company_response['AuditQuestion']['audit_question_category_type_id'];
            }
            


            if (isset($company_response['ActionPlan'])) {

                $company_response['AuditQuestionResponse']['action_plan_id']=$company_response['ActionPlan']['action_plan_id'];
            }
            



			if($company_response)
				$company_response = $this->AuditQuestionResponse->htmlDecodeResponse($company_response['AuditQuestionResponse']);

           // echo pr($company_response);
           // die;

            $this->respondAsJSON(STATUS_CODE_OK, $company_response);
        } else {
            // if supplier just return null
            $this->respondAsJSON(STATUS_CODE_OK, NULL);
        }
    }

    /**
     * response_to_single_question_by_supplier method
     * 
     * used by the supplier, this method populates the question_response form if the supplier has 
     * already entered a response.
     * 
     * ACL - may need to restrict to supplier. 
     *  A normal user can view a response from the array produced by all_responses_to_audit_questions
     *
     * @param int $audit_to_question_id The audit_to_question_id to find a response for 
     * @param int $company_id The current user's company_id read from the session
     * @param int $group_id The current user's group_id read from the session
     * @param int $supplier_id The current supplier's user_id
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @access public
     * @version 0.0.1
     * @return json object
     */
    public function response_to_single_question_by_supplier($audit_to_question_id = NULL) {
        $this->disableCache();

        $supplier_id = CakeSession::read('User.id');
        $company_id = CakeSession::read('User.company_id');
        $group_id = CakeSession::read('User.group_id');

        /*
         *  this data is used by supplier only
         *  Returns null if user is not supplier
         */
        if ($group_id === "5") {


            $supplier_response = $this->AuditQuestionResponse->findResponseToQuestion($audit_to_question_id,
                                                                                    $company_id,
                                                                                    $supplier_id);


            $supplier_response = $this->AuditQuestionResponse->htmlDecodeResponse($supplier_response['AuditQuestionResponse']);




            $this->respondAsJSON(STATUS_CODE_OK, $supplier_response);
        } else {
            $this->respondAsJSON(STATUS_CODE_OK, null);
        }
    }

    /**
     * update_response method
     * 
     * used by the supplier and normal user, this method will take data posted from the supplier_response_form and company_response_form
     * 
     * @param int $audit_to_question_id The audit_to_question_id to find a response for 
     * @param int $company_id The current user's company_id read from the session
     * @param int $group_id The current user's group_id read from the session
     * @param int $supplier_id The current supplier's user_id
     * 
     * @uses __check_if_response_modified
     * 
     * @throws exception NotImplementedException
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @access public
     * @version 0.0.1
     * @return json object
     */
    public function update_response() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $data = $this->request->input('json_decode');

            $this->AuditQuestionResponse->recursive = -1;

            $this->AuditQuestionResponse->id = (int) $data->response_id;

            /**
             * This would only work once I had surrounded inputs with ""
             */
            $fields = array(
                'consumer_note' => htmlentities(Purifier::clean($data->response_primary_note, 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8"),
                'secondary_note' => htmlentities(Purifier::clean($data->response_secondary_note, 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8"),
                'tertiary_note' => htmlentities(Purifier::clean($data->response_tertiary_note, 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8"),
                'opportunity_note' => htmlentities(Purifier::clean($data->response_opportunity_note, 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8"),
                'audit_trail' => htmlentities(Purifier::clean($data->response_audit_trail, 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8"),
                'arac_emissions' => $data->response_arac_emissions,
                'arac_water' => $data->response_arac_water,
                'arac_waste' => $data->response_arac_waste,
                'arac_land' => $data->response_arac_land,
                'arac_resources' => $data->response_arac_resources,
                'arac_legislation' => $data->response_arac_legislation,
                'arac_other' => htmlentities(Purifier::clean($data->response_arac_other, 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8"),
                'arac_potential' => htmlentities(Purifier::clean($data->response_arac_potential, 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8"),
                'arac_legislation' => htmlentities(Purifier::clean($data->response_arac_legislation, 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8"),
                'arac_stakeholders' => htmlentities(Purifier::clean($data->response_arac_stakeholders, 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8"),
                'aris_quantifiable' => $data->response_aris_quantifiable,
                'aris_committment' => $data->response_aris_committment,
                'aris_previous' => $data->response_aris_previous,
                'aris_feasible' => $data->response_aris_feasible,
                'aris_potential' => $data->response_aris_potential,
                'aris_significant' => $data->response_aris_significant,
                'aris_rationale' => htmlentities(Purifier::clean($data->response_aris_rationale, 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8")


            );

            //add in the response_options if they are set and push to fields array.
            if(isset($data->response_options)){
                $fields['response_options']=$data->response_options;
            }

            if(isset($data->action_plan_id)){
                $fields['action_plan_id']=$data->action_plan_id;
            }

            $modified_response_fields = $this->__check_if_response_modified($data);
            $fields = Hash::merge($fields, $modified_response_fields);

            $update_successful = $this->AuditQuestionResponse->save($fields);

            if ($update_successful) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Response Update Successful'));
            } else {
                $errors = $this->validationErrorsToString($this->AuditQuestionResponse->validationErrors);
                throw new BadRequestException(__($errors));
            }
        } else {
            throw new NotImplementedException(__('TO DO: No Post data'));
        }
    }

    /**
     * lock_single_response_type method
     * 
     * Not Currently Used - 14/12/02
     * This method will lock a single respnse type to a single question.
     * 
     * @param int response_id The id of the response to be updated
     * @param string response_type The response type to be updated i.e. primary, secondary etc.
     * 
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @access public
     * @version 0.0.1
     * @return sting success or error message
     */
//    public function lock_single_response_type() {
//        if ($this->request->is('post') || $this->request->is('put')) {
//            $data = $this->request->input('json_decode');
//            $this->AuditQuestionResponse->id = (int) $data->response_id;
//            if ($this->AuditQuestionResponse->saveField($data->response_type, 2, $validate = true)) {
//                $this->respondAsJSON(STATUS_CODE_OK, $data);
//            } else {
//                throw new BadRequestException(__('Error'));
//            }
//        }
//    }

    /**
     * unlock_single_response_type method
     * 
     * Not Currently Used - 14/12/02
     * used by admins and normal users, this method will unlock a single respnse type to a single question.
     * 
     * ACL - restrict to admin and user.
     *
     * @param int response_id The id of the response to be updated
     * @param string response_type The response type to be updated i.e. primary, secondary etc.
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @access public
     * @version 0.0.1
     * @return sting success or error message
     */
//    public function unlock_single_response_type() {
//        if ($this->request->is('post') || $this->request->is('put')) {
//            $data = $this->request->input('json_decode');
//            $this->AuditQuestionResponse->id = (int) $data->response_id;
//            if ($this->AuditQuestionResponse->saveField($data->response_type, 1, $validate = true)) {
//                $this->respondAsJSON(STATUS_CODE_OK, $data);
//            } else {
//                throw new BadRequestException(__('Error'));
//            }
//        }
//    }

    /**
     * __check_if_response_modified method
     * 
     * this method checks each of the response types; primary, secondary etc.
     * if any of them have been changed then the corresponding field will be added to array
     * and the update_response method will use the returned field to update the database.
     * 
     * usedby update_response method
     * 
     * @uses AuditQuestionResponse->htmlDecodeResponse
     * 
     * @param array $data takes input data and compares it to existing.
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @access private
     * @version 0.0.1
     * @return json An json array of objects
     */
    private function __check_if_response_modified($data) {
        $conditions = array(
            'AuditQuestionResponse.id' => (int) $data->response_id
        );

        $fields = array(
            'AuditQuestionResponse.id AS response_id',
            'AuditQuestionResponse.consumer_note as response_primary_note',
            'AuditQuestionResponse.secondary_note as response_secondary_note',
            'AuditQuestionResponse.tertiary_note as response_tertiary_note',
            'AuditQuestionResponse.opportunity_note as response_opportunity_note',
            'AuditQuestionResponse.audit_trail as response_audit_trail'
        );

        $this->AuditQuestionResponse->recursive = -1;
        $existing_response = $this->AuditQuestionResponse->find('first', array(
            'fields' => $fields,
            'conditions' => $conditions
                )
        );

        $existing_response = $this->AuditQuestionResponse->htmlDecodeResponse($existing_response['AuditQuestionResponse']);

        $group_id = CakeSession::read('User.group_id');

        $modified_response = array();

        /**
         * check if empty
         * 1 = true.... yes, it empty
         * 0 = false... it is not empty
         * 
         */

        //refactored
        $modified_response = $this->__addEmptyFlag($modified_response,$data->response_primary_note,'consumer_note_empty');
        $modified_response = $this->__addEmptyFlag($modified_response,$data->response_secondary_note,'secondary_note_empty');
        $modified_response = $this->__addEmptyFlag($modified_response,$data->response_tertiary_note,'tertiary_note_empty');
        $modified_response = $this->__addEmptyFlag($modified_response,$data->response_opportunity_note,'opportunity_note_empty');
        $modified_response = $this->__addEmptyFlag($modified_response,$data->response_audit_trail,'audit_trail_empty');


        if (isset($data->response_status)) {
            /**
             * update response status for all response types
             */
            $modified_response = Hash::merge($modified_response, array(
                        'consumer_note_modified' => $data->response_status,
                        'secondary_note_modified' => $data->response_status,
                        'tertiary_note_modified' => $data->response_status,
                        'opportunity_note_modified' => $data->response_status,
                        'audit_trail_modified' => $data->response_status
            ));
        }

        $modified_response = Hash::flatten($modified_response);



        return $modified_response;
    }


    /**
     * __addEmptyFlag method
     * 
     * this method checks each of the response. if empty it sets the appropriate field empty flag to true 
     * 
     * usedby __check_if_response_modified method
     * 
     * @uses AuditQuestionResponse->htmlDecodeResponse
     * 
     * @param array $flagFieldsArray array of flags.
     * @param string $responseToCheck text of response to check
     * @param string $flagFieldName name of flag field to add to $flagFieldsArray
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @access private
     * @version 0.0.1
     * @return an array of fields
     */

    private function __addEmptyFlag ($flagFieldsArray, $responseToCheck = '', $flagFieldName = '') {

        if (strlen(strip_tags($responseToCheck)) === 0) {
            return Hash::insert($flagFieldsArray, $flagFieldName, 1);
        } else {
            return Hash::insert($flagFieldsArray, $flagFieldName, 0);
        }

    }



    /**
     * __htmlDecodeResponse method
     * 
     * ??? Description ???
     * 
     * @uses __htmlDecoder
     * 
     * @param array $data Takes input data and compares it to existing.
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @access private
     * @version 0.0.1
     * @return array The response as an array
     */
//    private function __htmlDecodeResponse($responseData) {
//
//        if (isset($responseData['response_note'])) {
//            $responseData['response_note'] = $this->AuditQuestionResponse->htmlDecoder($responseData['response_note']);
//        }
//
//        $responseData['response_primary_note'] = $this->AuditQuestionResponse->htmlDecoder($responseData['response_primary_note']);
//        $responseData['response_secondary_note'] = $this->AuditQuestionResponse->htmlDecoder($responseData['response_secondary_note']);
//        $responseData['response_tertiary_note'] = $this->AuditQuestionResponse->htmlDecoder($responseData['response_tertiary_note']);
//        $responseData['response_opportunity_note'] = $this->AuditQuestionResponse->htmlDecoder($responseData['response_opportunity_note']);
//        $responseData['response_audit_trail'] = $this->AuditQuestionResponse->htmlDecoder($responseData['response_audit_trail']);
//
//        return $responseData;
//    }

    /**
     * __htmlDecoder method
     * 
     * ??? Description ???
     * 
     * @param string $html_string to clean
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @access private
     * @version 0.0.1
     * @return string
     */
//    private function __htmlDecoder($html_string) {
//        //temporary fix to clean out any dodgy formatting
//        $cleanString = str_replace(array('&Acirc;&nbsp;', '&Atilde;?', '&Acirc;&cent;', '&Atilde;&cent;'), ' ', $html_string);
//
//        return html_entity_decode($cleanString, ENT_QUOTES, "UTF-8");
//    }

    /**
     * __repair_responses method
     * 
     * used to run the html cleaner on audit responses.
     * 
     * ACL - restrict to admins.
     * 
     * @uses AuditQuestionResponse->htmlDecodeResponse
     * 
     * @param array $conditions 
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @access private
     * @version 0.0.1
     */
    private function __repair_responses($conditions) {
        $fields = array(
            'AuditQuestionResponse.id AS response_id',
            'AuditQuestionResponse.created AS response_created',
            'AuditQuestionResponse.modified as response_modified',
            'AuditQuestionResponse.consumer_note as response_primary_note',
            'AuditQuestionResponse.secondary_note as response_secondary_note',
            'AuditQuestionResponse.tertiary_note as response_tertiary_note',
            'AuditQuestionResponse.opportunity_note as response_opportunity_note',
            'AuditQuestionResponse.audit_trail as response_audit_trail'
        );

        $this->AuditQuestionResponse->recursive = -1;

        $company_responses = $this->AuditQuestionResponse->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions
                )
        );

        foreach ($company_responses as $company_response) {
            $company_response = $this->AuditQuestionResponse->htmlDecodeResponse($company_response['AuditQuestionResponse']);
            $company_data = array(
                'AuditQuestionResponse.consumer_note' => "\"" . htmlentities(Purifier::clean($company_response['response_primary_note'], 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8") . "\"",
                'AuditQuestionResponse.secondary_note' => "\"" . htmlentities(Purifier::clean($company_response['response_secondary_note'], 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8") . "\"",
                'AuditQuestionResponse.tertiary_note' => "\"" . htmlentities(Purifier::clean($company_response['response_tertiary_note'], 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8") . "\"",
                'AuditQuestionResponse.opportunity_note' => "\"" . htmlentities(Purifier::clean($company_response['response_opportunity_note'], 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8") . "\"",
                'AuditQuestionResponse.audit_trail' => "\"" . htmlentities(Purifier::clean($company_response['response_audit_trail'], 'SPGAllowedHTML'), ENT_QUOTES, "UTF-8") . "\"",
            );

            $this->AuditQuestionResponse->updateAll($company_data, array('AuditQuestionResponse.id' => (int) $company_response['response_id']));
        }
    }

    /**
     * repair_audit_responses_by_html method
     * 
     * ??? Description ?????
     * 
     * ACL - restrict to admins and managers
     * 
     * @param string $html_string 
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @access public
     * @version 0.0.1
     * @return string Success or Error message
     */
    public function repair_audit_responses_by_html($html_string) {
        $this->disableCache();

        // this data is used by admin or manager
        $conditions = array();
        $conditions['or'][] = array('AuditQuestionResponse.consumer_note LIKE' => "%$html_string%");
        $conditions['or'][] = array('AuditQuestionResponse.secondary_note LIKE' => "%$html_string%");
        $conditions['or'][] = array('AuditQuestionResponse.tertiary_note LIKE' => "%$html_string%");
        $conditions['or'][] = array('AuditQuestionResponse.opportunity_note LIKE' => "%$html_string%");
        $conditions['or'][] = array('AuditQuestionResponse.audit_trail LIKE' => "%$html_string%");
        $this->__repair_responses($conditions);
        $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Repair Successful'));
    }

    /**
     * repair_audit_responses method
     * 
     * ??? Description ?????
     * 
     * ACL - restrict to admins.
     * 
     * @uses __repair_responses
     * 
     * @param int $audit_id The Id of the audit to repair 
     * @author Keith Burton <keith.burton@greenstreets.ie>
     * @access public
     * @version 0.0.1
     * @return string Success or Error message
     */
    public function repair_audit_responses($audit_id = NULL) {
        $this->disableCache();
        $group_id = CakeSession::read('User.group_id');

        // this data is used by admin, manager or normal user
        if ($group_id === '1' || ($group_id === '2')) {
            $conditions = array(
                'AuditQuestionResponse.company_audit_id' => $audit_id
            );

            $this->__repair_responses($conditions);
            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Repair Successful'));
        } else {
            $this->respondAsJSON(STATUS_CODE_OK, NULL);
        }
    }

}
