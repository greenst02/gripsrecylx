<?php

//App::uses('AppController', 'Controller');

/**
 * AuditQuestionCategoryTypes Controller
 * 
 * Not Currently Used - 14/12/02
 *
 * @property AuditQuestionCategory $AuditQuestionCategory
 */

//class AuditQuestionCategoryTypesController extends AppController {

//    var $name = 'AuditQuestionCategoryTypes';
//
//    public function index() {
////        Configure::write('debug', 0);
//        $this->disableCache();
//
//        $this->AuditQuestionCategoryType->recursive = -1;
//
//        $fields[] = array();
//        $fields = array(
//            'id',
//            'name'
//        );
//
//        $audit_question_category_types = $this->AuditQuestionCategoryType->find('all', array('fields' => $fields));
//
//        $this->respondAsJSON(STATUS_CODE_OK, Hash::extract($audit_question_category_types, '{n}.AuditQuestionCategoryType'));
//    }

//    public function view($id = null) {
//        $this->AuditQuestionCategoryType->recursive = -1;
//
//        if (!$this->AuditQuestionCategoryType->exists($id)) {
//            throw new NotFoundException(__('Invalid AuditQuestionCategoryType'));
//        }
//        $category_type = $this->AuditQuestionCategoryType->findById($id);
//
//        $this->respondAsJSON(STATUS_CODE_OK, $category_type['AuditQuestionCategoryType']);
//    }

//    public function add() {
//        if ($this->request->is('post') || $this->request->is('put')) {
//            $new_category_type = $this->request->data;
//            $this->AuditQuestionCategoryType->create();
//            if ($this->AuditQuestionCategoryType->save($new_category_type)) {
//
//                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'New AuditQuestionCategoryType Added'));
//            } else {
//                $errors = $this->validationErrorsToString($this->AuditQuestionCategoryType->validationErrors);
//                throw new BadRequestException(__($errors));
//            }
//        }
//    }

//    public function edit($id = null) {
//        $this->AuditQuestionCategoryType->id = $id;
//        if (!$this->AuditQuestionCategoryType->exists($id)) {
//            throw new NotFoundException(__('Invalid Company Site id'));
//        }
//        if ($this->request->is('post') || $this->request->is('put')) {
//            if ($this->AuditQuestionCategoryType->save($this->request->data)) {
//                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'AuditQuestionCategoryType Edited'));
//            } else {
//                $errors = $this->validationErrorsToString($this->AuditQuestionCategoryType->validationErrors);
//                throw new BadRequestException(__($errors));
//            }
//        }
//    }

//    public function delete($id = null) {
//        $this->AuditQuestionCategoryType->id = $id;
//        if (!$this->AuditQuestionCategoryType->exists($id)) {
//            throw new NotFoundException(__('Invalid AuditQuestionCategoryType id'));
//        }
//
//        if ($this->AuditQuestionCategoryType->delete($id)) {
//            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'AuditQuestionCategoryType Deleted'));
//        } else {
//                throw new BadRequestException(__('Error'));
//        }
//    }
//}
