<?php

App::uses('AppController', 'Controller');

/**
 * Sessions Controller
 *
 * @property Session $Session
 */
class SessionsController extends AppController {

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Session->recursive = 0;
        $this->set('sessions', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Session->exists($id)) {
            throw new NotFoundException(__('Invalid session'));
        }
        $options = array('conditions' => array('Session.' . $this->Session->primaryKey => $id));
        $this->set('session', $this->Session->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Session->create();
            if ($this->Session->save($this->request->data)) {
                $this->Session->setFlash(__('The session has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The session could not be saved. Please, try again.'));
            }
        }
        $users = $this->Session->User->find('list');
        $companies = $this->Session->Company->find('list');
        $this->set(compact('users', 'companies'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Session->exists($id)) {
            throw new NotFoundException(__('Invalid session'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Session->save($this->request->data)) {
                $this->Session->setFlash(__('The session has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The session could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Session.' . $this->Session->primaryKey => $id));
            $this->request->data = $this->Session->find('first', $options);
        }
        $users = $this->Session->User->find('list');
        $companies = $this->Session->Company->find('list');
        $this->set(compact('users', 'companies'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Session->id = $id;
        if (!$this->Session->exists()) {
            throw new NotFoundException(__('Invalid session'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Session->delete()) {
            $this->Session->setFlash(__('Session deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Session was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
