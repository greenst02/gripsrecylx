<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Acl',
        'RequestHandler',
        'Session',
//        'Security',
        'Cookie',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'User'
                ),
//                'Basic'
//                    'fields' => array(
//                        
//                    ),
//                    'scope' => array('User.enabled_status_id' => 1)
//                )
//                'Digest' => array(
//                    'userModel' => 'User',
////                    'scope' => array('User.user_enabled_status_id' => 1),
////                    'realm' => 'somerealm',
//                    'fields' => array('password' => 'digest_hash')
//                )
            ),
            'authorize' => array(
                'Actions' => array(
                    'actionPath' => 'Controllers',
                )
            )
        )
    );
    public $helpers = array('Html', 'Form', 'Session');

    public function beforeFilter() {

//        $sslPath = Router::url("/",true);
//        $sslPath = str_replace('http','https',$sslPath);
//        echo pr(env);
//        die;
        // Codes added for SSL security
        // $this->Security->validatePost=false;
        // $this->Security->csrfCheck=false;
        // $this->Security->csrfUseOnce=false;
        // $sslnotallowed_url  = array('beta_user','terms','privacy','security');
        // $this->Security->blackHoleCallback = 'forceSSL';
        // if(!in_array($this->params['action'],$sslnotallowed_url)){
        // $this->Security->requireSecure('*');
        // }

        $this->Auth->unauthorizedRedirect = false;
        $this->Auth->redirect = FALSE;

//        $this->Auth->loginAction = ('https://' . env('SERVER_NAME') . Router::url("/") . 'app/webroot/bin/#/login');
        $this->Auth->loginAction = ('https://' . env('SERVER_NAME') . Router::url("/") . 'app/webroot/dist/gripsrecylx/');
        $this->Auth->authError = FALSE;
        // By default, the AuthComponent looks for 'Auth.User'
        AuthComponent::$sessionKey = 'User';

//        parent::beforeFilter();

    }

    /**
     * respondAsJSON
     *
     * @param integer $statusCode - HTTP Status Code
     * @param array $dataArray - Array of data to send in JSON response
     * @return void
     */
    protected function respondAsJSON($statusCode, $dataArray, $itemCount = null) {

        if ($itemCount) {
            $this->response->header(array(
                'X-Count' => $itemCount
            ));
        }
        
        $this->response->header('Access-Control-Allow-Origin','*');
        $this->response->header('Access-Control-Allow-Methods','*');
        $this->response->header('Access-Control-Allow-Headers','X-Requested-With');
        $this->response->header('Access-Control-Allow-Headers','Content-Type, x-xsrf-token');
        $this->response->header('Access-Control-Max-Age','172800');


        $this->response->type('json');
        $this->response->statusCode($statusCode);
        $this->response->body(json_encode($dataArray));
        $this->response->send();
        $this->_stop();
    }

    /*
     * Fix for IE caching issues using Cakephp Controllers.
     * Anywhere we're having caching issues use
     * $this->disableCache();
     */

    public function disableCache() {
        $this->response->header(array(
            'Pragma' => 'no-cache',
        ));
        return parent::disableCache();
    }

// function forceSSL() {
// $this->redirect('https://'.env('SERVER_NAME'). $this->here);
// }

    protected function validationErrorsToString($validationErrors = array()) {
        $errors = '';
        foreach ($validationErrors as $assoc) {
            foreach ($assoc as $k => $v) {
                $errors .= $v . '<br/>';
            }
        }

        return $errors;
    }

}
