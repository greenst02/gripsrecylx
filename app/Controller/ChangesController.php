<?php

App::uses('AppController', 'Controller');

/**
 * Changes Controller
 *
 * @property Change $Change
 */
class ChangesController extends AppController {

    private $_default_changes_query = "SELECT 
                changes.id AS id,
                changes.event AS event,
                changes.model AS model,
                changes.entity_id AS entity_id,
                changes.json_object AS json_object,
                changes.user_id AS user_id,
                changes.created AS created,
                users.firstname AS user_firstname,
                users.lastname AS user_lastname,
                change_deltas.old_value AS old_value,
                change_deltas.new_value AS new_value,
                change_deltas.property_name AS property_name
            FROM
                changes
            LEFT JOIN
                users
            ON
                changes.user_id = users.id
            LEFT JOIN
                change_deltas
            ON
                changes.id = change_deltas.change_id ";

    public function index() {

        $sql_query = $this->_default_changes_query .
                "WHERE 
                changes.model != 'BatchemailToSupplier' ";

        $count_result = $this->Change->query("SELECT COUNT(id) FROM changes WHERE model != 'BatchemailToSupplier'");
        $total_items = $count_result[0][0]['COUNT(id)'];

        if (!empty($this->request->query['sorting'])) {
            $sorting = preg_replace(array("/{/", "/}/", '/"/'), "", $this->request->query['sorting']);
            $sorting = preg_replace("/:/", " ", $sorting);
            $sql_query = $sql_query . ' ORDER BY ' . $sorting;
        }

        if (!empty($this->request->query['limit'])) {
            $sql_query = $sql_query . ' LIMIT ' . ($this->request->query['page'] * $this->request->query['limit']) . ', ' . $this->request->query['limit'];
        }

        $change_logs_results = $this->Change->query($sql_query);

        $a1 = Hash::extract($change_logs_results, '{n}.changes');
        $a2 = Hash::extract($change_logs_results, '{n}.users');
        $a3 = Hash::extract($change_logs_results, '{n}.change_deltas');

        $change_logs = Hash::merge($a1, $a2, $a3);

        $this->loadModel('AuditQuestionResponse');
        $html_cleaned = array();
        foreach ($change_logs as $change_logs_item) {
            $change_logs_item['old_value'] = $this->AuditQuestionResponse->htmlDecoder($change_logs_item['old_value']);
            $change_logs_item['new_value'] = $this->AuditQuestionResponse->htmlDecoder($change_logs_item['new_value']);
            array_push($html_cleaned, $change_logs_item);
        }

        $this->respondAsJSON(STATUS_CODE_OK, $html_cleaned, $total_items);
    }

//    public function count() {
//        $count_result = $this->Change->query("SELECT COUNT(id) FROM changes");
//        $count = $count_result[0][0]['COUNT(id)'];
//
//        $this->respondAsJSON(STATUS_CODE_OK, (int) $count);
//    }

    public function audit_details_history() {
        $audit_id = $this->request->data['audit_id'];

        $count_result = $this->Change->query("SELECT COUNT(id) FROM changes WHERE model = 'CompanyAudit' AND entity_id = $audit_id");
        $total_items = $count_result[0][0]['COUNT(id)'];

        $sql_query = $this->_default_changes_query .
                "WHERE 
                changes.model = 'CompanyAudit'
            AND 
                changes.entity_id = " . $audit_id . " ";

        $sorting = '';
        foreach ($this->request->data['sorting'] as $key => $value) {
            $sorting = $sorting . $key . ' ' . $value;
        }

        $sql_query = $sql_query . ' ORDER BY ' . $sorting;

        if (!empty($this->request->data['limit'])) {
            $sql_query = $sql_query . ' LIMIT ' . ($this->request->data['page'] * $this->request->data['limit']) . ', ' . $this->request->data['limit'];
        }

        $audit_history_results = $this->Change->query($sql_query);

        $a1 = Hash::extract($audit_history_results, '{n}.changes');
        $a2 = Hash::extract($audit_history_results, '{n}.users');
        $a3 = Hash::extract($audit_history_results, '{n}.change_deltas');

        $audit_history = Hash::merge($a1, $a2, $a3);

        $this->respondAsJSON(STATUS_CODE_OK, $audit_history, $total_items);
    }

    public function company_response_history() {
        $response_id = $this->request->data['response_id'];

        $sql_query = $this->_default_changes_query .
                "WHERE
                    `model` = 'AuditQuestionResponse'
                AND
                    `entity_id` = " . $response_id;

        $response_history_results = $this->Change->query($sql_query);

        $a1 = Hash::extract($response_history_results, '{n}.changes');
        $a2 = Hash::extract($response_history_results, '{n}.users');
        $a3 = Hash::extract($response_history_results, '{n}.change_deltas');

        $response_history = Hash::merge($a1, $a2, $a3);

        $this->loadModel('AuditQuestionResponse');
        $html_cleaned = array();
        foreach ($response_history as $response_history_item) {
            $response_history_item['old_value'] = $this->AuditQuestionResponse->htmlDecoder($response_history_item['old_value']);
            $response_history_item['new_value'] = $this->AuditQuestionResponse->htmlDecoder($response_history_item['new_value']);
            array_push($html_cleaned, $response_history_item);
        }

        $this->respondAsJSON(STATUS_CODE_OK, $html_cleaned);
    }

    public function email_to_supplier_status_history() {
        $batch_email_to_supplier_id = $this->request->data['batch_email_to_supplier_id'];

        $count_result = $this->Change->query("SELECT COUNT(id) FROM changes WHERE model = 'BatchemailToSupplier' AND entity_id = $batch_email_to_supplier_id");
        $total_items = $count_result[0][0]['COUNT(id)'];

        $sql_query = $this->_default_changes_query . "
            WHERE 
                changes.model = 'BatchemailToSupplier'
            AND 
                changes.entity_id = " . $batch_email_to_supplier_id . " ";

        $sorting = '';
        foreach ($this->request->data['sorting'] as $key => $value) {
            $sorting = $sorting . $key . ' ' . $value;
        }

        $sql_query = $sql_query . ' ORDER BY ' . $sorting;

        if (!empty($this->request->data['limit'])) {
            $sql_query = $sql_query . ' LIMIT ' . ($this->request->data['page'] * $this->request->data['limit']) . ', ' . $this->request->data['limit'];
        }

        $email_to_supplier_status_history_results = $this->Change->query($sql_query);

        $a1 = Hash::extract($email_to_supplier_status_history_results, '{n}.changes');
        $a2 = Hash::extract($email_to_supplier_status_history_results, '{n}.users');
        $a3 = Hash::extract($email_to_supplier_status_history_results, '{n}.change_deltas');

        $email_to_supplier_status_history = Hash::merge($a1, $a2, $a3);

        $this->respondAsJSON(STATUS_CODE_OK, $email_to_supplier_status_history, $total_items);
    }

}
