<?php

App::uses('AppController', 'Controller');

/**
 * Companies Controller
 *
 * @property Company $Company
 */
class CompaniesController extends AppController {

    var $name = 'Companies';
    private $_count = 0;

    /**
     * index method
     * 
     * return a list of Companies
     * used by dashboard/manager/companies
     * 
     * ACL - Restricted to admin and manager
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array of all companies
     */
    public function index() {
        $this->Company->recursive = 0;

        $fields = array(
            'Company.id AS id',
            'Company.trading_name AS trading_name',
            'Company.legal_name AS legal_name',
            'Company.company_reg_number AS company_reg_number',
            'AustralianState.name AS state_name',
            'IndustryType.name AS industry_type',
            'IndustrySector.name AS industry_sector',
            'CompanyEnabledStatus.id AS company_enabled_status_id',
            'CompanyEnabledStatus.status AS enabled_status'
        );

        /**
         * The setting required to filter, sort and limit the data returned to the client side.
         */
        $order = array();   // sort order
        $limit = NULL;        // items per page, default 25
        $offset = 0;        // calculated by $limit * the page number
        $filter = '';       // find items based on the string entered in the search box

        $conditions = array();

        if (!empty($this->request->query['filter'])) {
            $filter = '%' . $this->request->query['filter'] . '%';
            $conditions = array(
                "OR" => array(
                    'trading_name LIKE ' => $filter,
                    'company_reg_number LIKE ' => $filter,
                    'AustralianState.name LIKE ' => $filter,
                    'IndustryType.name LIKE' => $filter,
                    'IndustrySector.name LIKE' => $filter,
                    'CompanyEnabledStatus.status LIKE' => $filter
                )
            );
        }

        if (!empty($this->request->query['sorting'])) {
            $sorting = preg_replace(array("/{/", "/}/", '/"/'), "", $this->request->query['sorting']);
            $sorting = preg_replace("/:/", " ", $sorting);
            array_push($order, $sorting);
        }

        if (!empty($this->request->query['limit'])) {
            $limit = $this->request->query['limit'];
        }

        if (!empty($this->request->query['page']) && ($this->request->query['page'] > 0)) {
            $offset = $limit * $this->request->query['page'];
        }

        $companies_result = $this->Company->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $limit,
            'offset' => $offset
        ));

        $this->_set_count($conditions);

        $a1 = Hash::extract($companies_result, '{n}.Company');
        $a2 = Hash::extract($companies_result, '{n}.AustralianState');
        $a3 = Hash::extract($companies_result, '{n}.IndustryType');
        $a4 = Hash::extract($companies_result, '{n}.IndustrySector');
        $a5 = Hash::extract($companies_result, '{n}.CompanyEnabledStatus');
        $results = Hash::merge($a1, $a2, $a3, $a4, $a5);

        $this->respondAsJSON(STATUS_CODE_OK, $results, $this->_count);
    }
    
    private function _set_count($conditions = NULL) {
        $count = $this->Company->find('count', array(
            'conditions' => $conditions
        ));
        $this->_count = (int) $count;
    }

    /**
     * view method
     * 
     * return a JSON object of Company, always uses the company_id of the session
     * if current user is a supplier, only the company's trading name is returned
     * 
     * @param int $company_id The company_id The id that is sent from client side is always 0. This is overwritten by the company_id from the session
     * @param int $group_id The group_id is read is from the session.
     * @throws  NotFoundException
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return object JSON object of company
     */
    public function view($id = null) {

        $this->disableCache();
        $company_id = CakeSession::read('User.company_id');
        $group_id = CakeSession::read('User.group_id');

        $this->Company->recursive = -1;

        if ($group_id === '5') {
            // if supplier, only return the company trading name
            $fields = array(
                'Company.id AS id',
                'Company.trading_name AS trading_name',
            );
            $company = $this->Company->findById($company_id, $fields);
        } else {
            $company = $this->Company->findById($company_id);
        }

        if ($company) {
            $this->respondAsJSON(STATUS_CODE_OK, $company['Company']);
        } else {
            throw new NotFoundException(__('Company Not Found'));
        }
    }

    /**
     * add method
     * 
     * Create a new Company and return the new company's id.
     * 
     * ACL - Restricted to Manager and Admin.
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return int The id of the new company
     */
    public function add() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Company->create();
            if ($this->Company->save($this->request->data)) {
                $this->switch_company($this->Company->id);
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'New Company Added'));
            } else {
                $errors = $this->validationErrorsToString($this->Company->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

    /**
     * add method
     * 
     * Create a new Company and return the new company's id.
     * 
     * ACL - Restricted to Manager and Admin.
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array Array of validation errors or success message
     */
    public function edit($id = null) {
        $this->Company->id = $id;

        if (!$this->Company->exists($id)) {
            throw new NotFoundException(__('Invalid Company'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $updated_company_data = $this->request->data;

            /*
             * The field 'industry_sector_other' should be empty unless 'Other' has been selected from the IndustrySectorDropdown
             * industry_sector_id === '10' means 'Other' is selected
             */
            if (!($updated_company_data['industry_sector_id'] === '10')) {
                $updated_company_data['industry_sector_other'] = '';
            }
            /*
             * The field 'industry_type_other' should be empty unless 'Other' has been selected from the IndustryTypeDropdown
             * industry_type_id === '10' means 'Other' has been selected
             */
            if (!($updated_company_data['industry_type_id'] === '14')) {
                $updated_company_data['industry_type_other'] = '';
            }

            if ($this->Company->save($updated_company_data)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Company Edited'));
            } else {
                $errors = $this->validationErrorsToString($this->Company->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

    /**
     * delete method
     * 
     * Not Currently Used - 14/12/02
     * 
     * ACL - Restricted to Admin and Manager
     * 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
//    public function delete($id = null) {
//
//        throw new NotImplementedException(__('TO DO: Not implemented - restrict to admins'));
//        $this->Company->id = $id;
//
//        if (!$this->Company->exists($id)) {
//            throw new NotFoundException(__('Invalid company id'));
//        }
//        if ($this->request->is('delete')) {
//            Configure::write('debug', 0);
//            $this->disableCache();
//
//            if ($this->Company->delete($id)) {
//                $message = 'Deleted';
//            } else {
//                $message = 'Error';
//            }
//            $this->set(array(
//                'message' => $message,
//                '_serialize' => array('message')
//            ));
//        }
//    }

    /*
     * method switch_company
     * 
     * Restricted to Admin, Manager.
     * Accessed from the companies list view. 
     * When the Admin clicks on a company from the list, that companies_id is written to the session.
     * After the company_id is written to the session the admin can work as normal with that company.
     * 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function switch_company($id = null) {
        $this->disableCache();
        if ($id) {
            $this->Session->write('User.company_id', $id);
            return;
        } else {
            $data = $this->request->input('json_decode');
            $company_id = $data->company_id;
            if (!$this->Company->exists($company_id)) {
                throw new NotFoundException(__('Company Not Found'));
            } else {
                $this->Session->write('User.company_id', $company_id);
                $this->Company->recursive = -1;
                $company = $this->Company->findById($company_id, array(
                    'fields' => 'trading_name'
                ));

                $trading_name = $company['Company']['trading_name'];
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Active company is now: ' . $trading_name));
            }
        }
    }

    /*
     * method change_company_enabled_status
     * 
     * Restricted to Admin, Manager.
     * At present there are only 2 statuses; enabled and disabled, more could be added later.
     * This method allows admin users to disable / enable a company.
     * The enabled_status can then be checked when a normal user is logging in.
     * 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

    public function change_company_enabled_status() {
        if ($this->request->is('post')) {
            $data = $this->request->input('json_decode');
            $this->Company->id = $data->company_id;
            if (!$this->Company->exists($data->company_id)) {
                throw new NotFoundException(__('Invalid Company'));
            }

            if ($this->Company->save($this->request->data)) {
                switch ($data->company_enabled_status_id) {
                    case 1:
                        $message = 'Company enabled';
                        break;
                    case 2:
                        $message = 'Company disabled';
                        break;
                    default:
                        break;
                }

                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => $message));
            } else {
                $errors = $this->validationErrorsToString($this->Company->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

}
