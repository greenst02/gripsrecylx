<?php

App::uses('AppController', 'Controller');

/**
 * AustralianStates Controller
 *
 * @property AustralianState $AustralianState
 */
class AustralianStatesController extends AppController {

    var $name = 'AustralianStates';

    /**
     * index method
     * 
     * return a list of Australian States - used in the companyDetails form
     * 
     * @param string $company_id The company_id is read from the Session.
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all Australian States
     */
    public function index() {
        $this->AustralianState->recursive = -1;
        $australian_states_list = $this->AustralianState->find('all');
        $australian_states = Hash::extract($australian_states_list, '{n}.AustralianState');
        $this->respondAsJSON(STATUS_CODE_OK, $australian_states);
    }
}
