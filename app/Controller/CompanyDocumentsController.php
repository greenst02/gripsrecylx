<?php

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * CompanyDocuments Controller
 * 
 * Main controller for listing, uploading, viewing and deleting documents
 *
 * @property CompanyDocument $CompanyDocument
 */
class CompanyDocumentsController extends AppController {

    var $name = 'CompanyDocuments';
    public $helpers = array('Media.Media');

    /**
     * index method
     * 
     * Return a list of Documents associated with a company.
     * Excludes documents associated with an audit
     * 
     * @uses CompanyDocument::get_documents 
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing Documents associated with a Company and Not associated with an Audit
     */
    public function index() {
        $this->disableCache();

        $audit_to_doc_ids = $this->_get_all_audit_to_doc_id();

        $conditions = array(
            'CompanyDocument.company_document_type_id !=' => 13, // not an email_attachment 
            'NOT' => array(
                'CompanyDocument.id' => $audit_to_doc_ids
            )
        );

        $documents = $this->CompanyDocument->get_documents(null, $conditions);
        $this->respondAsJSON(STATUS_CODE_OK, $documents);
    }

    /**
     * all_audit_docs_index method
     * Return a list of Documents associated with a Company AND associated a Company Audit.
     * Excludes documents uploaded by Suppliers
     * 
     * @uses CompanyDocument::get_documents 
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing Documents associated with a Company AND Audit
     */
    public function all_audit_docs_index() {
        $audit_to_doc_ids = $this->_get_all_audit_to_doc_id();

        $fields = array(
            'CompanyAudit.spg_audit_code AS spg_audit_code'
        );

        $conditions = array(
            'CompanyDocument.id' => $audit_to_doc_ids
        );

        $this->respondAsJSON(STATUS_CODE_OK, $this->CompanyDocument->get_documents($fields, $conditions));
    }

    private function _get_all_audit_to_doc_id() {
        $this->loadModel('AuditToDocument');
        $audit_to_doc_ids_query = $this->AuditToDocument->find('all', array(
            'fields' => 'AuditToDocument.company_document_id'
        ));

        return Hash::extract($audit_to_doc_ids_query, '{n}.AuditToDocument.company_document_id');
    }

    /**
     * audit_company_documents method
     * Return a list of Documents associated with an Audit and Not a Supplier.
     * 
     * @param int $audit_id
     * 
     * @uses CompanyDocument::get_documents 
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing Documents associated with an Audit and Not associated with a Supplier
     */
    public function audit_company_documents($audit_id = null) {
        $audit_to_doc_ids = $this->_get_audit_to_doc_ids($audit_id);

        $conditions = array(
            'CompanyDocument.supplier_id' => 0,
            'CompanyDocument.id' => $audit_to_doc_ids
        );

        $this->respondAsJSON(STATUS_CODE_OK, $this->CompanyDocument->get_documents(null, $conditions));
    }

    /**
     * audit_supplier_documents method
     * Return a list of Documents associated with an Audit and a Supplier.
     * Used by Supplier.
     * Used to populate the list of audit documents the currently logged in supplier has uploaded
     * 
     * @param int $audit_id 
     * @param int supplier_id The supplier_id is read from the Session.
     * 
     * @uses CompanyDocument::get_documents 
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing Documents associated with an Audit and Not associated with a Supplier
     */
    public function audit_supplier_documents($audit_id = null) {
        $audit_to_doc_ids = $this->_get_audit_to_doc_ids($audit_id);

        $conditions = array(
            'CompanyDocument.supplier_id' => CakeSession::read('User.id'),
            'CompanyDocument.id' => $audit_to_doc_ids
        );

        $this->respondAsJSON(STATUS_CODE_OK, $this->CompanyDocument->get_documents(null, $conditions));
    }

    private function _get_audit_to_doc_ids($audit_id) {
        $this->loadModel('AuditToDocument');
        $audit_to_doc_ids_query = $this->AuditToDocument->find('all', array(
            'fields' => array('AuditToDocument.company_document_id'),
            'conditions' => array(
                'AuditToDocument.company_audit_id' => $audit_id
            )
        ));

        return Hash::extract($audit_to_doc_ids_query, '{n}.AuditToDocument.company_document_id');
    }

    /**
     * audit_images method
     * 
     * Return a list of Images associated with an Audit.
     * Excludes documents associated with a Supplier
     * 
     * @param int $audit_id The id of the audit
     * 
     * @uses CompanyDocument::get_documents 
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing Documents of type image associated with Company and Audit and not uploaded by a supplier
     */
    public function audit_images($audit_id = null) {
        $audit_to_doc_ids = $this->_get_audit_to_doc_ids($audit_id);

        $conditions = array(
            'CompanyDocument.supplier_id' => 0,
            'CompanyDocument.id' => $audit_to_doc_ids,
            'dirname LIKE' => 'img%' // this line will return only images from the list of documents
        );

        $this->respondAsJSON(STATUS_CODE_OK, $this->CompanyDocument->get_documents(null, $conditions));
    }

    /**
     * audit_suppliers_documents method
     * Return a list of all Documents associated with an Audit that have been uploaded by a Supplier.
     * For each document the supplier's details (supplier_company) is also returned
     * 
     * @param int $company_id The company id is read from the session
     * @param int $audit_id The id of the audit
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing Documents of type image associated with Company and Audit and not uploaded by a supplier
     */
    public function audit_suppliers_documents($audit_id = null) {
        $company_id = CakeSession::read('User.company_id');

        $this->loadModel('AuditToDocument');
        $audit_to_doc_ids_query = $this->AuditToDocument->find('all', array(
            'fields' => array('AuditToDocument.company_document_id'),
            'conditions' => array(
                'AuditToDocument.company_audit_id' => $audit_id
            )
        ));

        if (!empty($audit_to_doc_ids_query)) {
            $audit_to_doc_ids = Hash::extract($audit_to_doc_ids_query, '{n}.AuditToDocument.company_document_id');
            $audit_to_doc_ids_to_string = "(" . implode(', ', $audit_to_doc_ids) . ")";

            $this->CompanyDocument->recursive = -1;
            $documents_result = $this->CompanyDocument->query(
                    "SELECT 
                company_documents.id AS id,
                company_documents.company_id AS company_id,
                company_documents.created AS created,
                company_documents.modified AS modified,
                company_documents.display_name AS display_name,
                company_documents.description AS description,
                company_documents.audit_id AS audit_id,
                company_documents.supplier_id AS supplier_id,
                users.firstname AS supplier_firstname,
                users.lastname AS supplier_lastname,
                users.username AS username,
                users.supplier_company AS supplier_company
            FROM 
                company_documents 
            LEFT JOIN 
                users 
            ON 
                users.id = company_documents.supplier_id 
            WHERE 
                company_documents.company_id = $company_id
            AND
                company_documents.supplier_id != 0
            AND
                company_documents.id IN $audit_to_doc_ids_to_string;"
            );
//                company_documents.audit_id = $audit_id;"

            $a1 = Hash::extract($documents_result, '{n}.company_documents');
            $a2 = Hash::extract($documents_result, '{n}.users');
            $documents = Hash::merge($a1, $a2);

            $this->respondAsJSON(STATUS_CODE_OK, $documents);
        } else {
            $this->respondAsJSON(STATUS_CODE_OK, array());
        }
    }

    /**
     * view_document_info method
     * Used to view data related to a document
     * 
     * @param int $id The id of the Document
     * 
     * @uses CompanyDocument::get_document get a single document's data
     * 
     * @access public
     * @version 0.0.1
     * @throws NotFoundException
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return object json object of info related to specified document / image
     */
    public function view_document_info($document_id = null) {
        $this->disableCache();

        $fields = array(
            'CompanyDocument.audit_id as audit_id',
            'CompanyAudit.spg_audit_code as spg_audit_code',
        );

        $document = $this->CompanyDocument->get_document($document_id, $fields, null);
        if ($document) {
            $this->respondAsJSON(STATUS_CODE_OK, $document);
        } else {
            throw new NotFoundException(__('Document Not Found'));
        }
    }

    /**
     * view method
     * Used to downoad a document
     * 
     * @param int $document_id The id of the Document
     * @param int $company_id is added to the find() method in appModel::beforeFind()
     * 
     * @uses CompanyDocumentsController::_get_file_response modifies response to type of file
     * 
     * @access public
     * @version 0.0.1
     * @throws NotFoundException
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return file 
     */
    public function view($document_id = null) {
        $this->CompanyDocument->recursive = -1;
        $company_document = $this->CompanyDocument->findById($document_id);
        if ($company_document) {
            $file_path = $this->CompanyDocument->get_file_path($document_id, 'original');
            return $this->_get_file_response($file_path);
        } else {
            throw new NotFoundException(__('Document Not Found'));
        }
    }

    /**
     * view_thumbnail method
     * Used to view thumbnails of images
     * 
     * @param int $document_id The id of the Document
     * 
     * @uses CompanyDocumentsController::_get_file_response modifies response to type of file
     * 
     * @access public
     * @version 0.0.1
     * @throws NotFoundException
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return image file
     */
    public function view_thumbnail($document_id = null) {
        $file_path = $this->CompanyDocument->get_file_path($document_id, 'medium');
        if ($file_path) {
            return $this->_get_file_response($file_path);
        } else {
            throw new NotFoundException(__('Image Not Found'));
        }
    }

    /**
     * view_thumbnail method
     * Used to view thumbnails of images
     * 
     * @param int $document_id The id of the Document
     * @access public
     * 
     * @uses CompanyDocumentsController::_get_file_response modifies response to type of file
     * 
     * @version 0.0.1
     * @throws NotFoundException
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return image file
     */
    public function view_preview_of_image($document_id = null) {
        $file_path = $this->CompanyDocument->get_file_path($document_id, 'large');
        if ($file_path) {
            return $this->_get_file_response($file_path);
        } else {
            throw new NotFoundException(__('Image Not Found'));
        }
    }

    /**
     * _get_file_response method
     * 
     * Modifies response to type of file
     * 
     * @param array $file_path the file's data required to dowload
     * 
     * @access private
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return file
     */
    private function _get_file_response($file_path = null) {
        //fix to display .msg files in outlook
        $this->response->type(array('msg' => 'application/vnd.ms-outlook'));
        $this->response->file($file_path['path'], array('download' => true, 'name' => $file_path['name']));
        return $this->response;
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $file['name'] = $this->params['form']['file']['name'];
            $file['type'] = $this->params['form']['file']['type'];
            $file['tmp_name'] = $this->params['form']['file']['tmp_name'];
            $file['error'] = $this->params['form']['file']['error'];
            $file['size'] = $this->params['form']['file']['size'];

            $new_doc['dirname'] = '';
            $new_doc['basename'] = '';
            $new_doc['display_name'] = $this->params['form']['file']['name'];

            $new_doc['company_id'] = CakeSession::read('User.company_id');
            if (isset($this->request->data['document_description'])) {
                $new_doc['description'] = $this->request->data['document_description'];
            }

            // audit_id will be posted if upload is from audit_documents section of front end app
//            if (isset($new_doc['audit_id'])) {
//                $new_doc['audit_id'] = $this->request->data['audit_id'];
//            }

            if (isset($this->request->data['associated_audit_ids']) && strlen($this->request->data['associated_audit_ids']) > 2) {
                $str = ltrim($this->request->data['associated_audit_ids'], '[');
                $str = rtrim($str, ']');
                $audit_ids = explode(',', $str);
                
                $audit_ids_to_associate = array();
                foreach ($audit_ids as $audit_id) {
                    list($key, $val) = explode(':', $audit_id);
                    $val = str_replace('"', '', $val);
                    $key = str_replace('"', '', $key);

                    if ($val === 'true') {
                        array_push($audit_ids_to_associate, $key);
                    }
                }
            }

            if (empty($this->request->data['document_type_id'])) {
                $new_doc['company_document_type_id'] = 10; // 10 = Not Selected
            } else {
                $new_doc['company_document_type_id'] = $this->request->data['document_type_id'];
            }

            // batchemail_id will be posted if upload is an attachment to an email
            if (isset($this->request->data['batchemail_id'])) {
                $new_doc['batchemail_id'] = $this->request->data['batchemail_id'];
                $new_doc['company_document_type_id'] = 13; // 14 = email_attachment
            }

            if (CakeSession::read('User.group_id') === '5') {
                $new_doc['supplier_id'] = CakeSession::read('User.id');
                $new_doc['company_document_type_id'] = 11;
            }

            $new_doc['file'] = $file;

            $this->CompanyDocument->create();
            if ($this->CompanyDocument->save($new_doc)) {
                if (isset($audit_ids_to_associate)) {
                    
                    $associations = array();
                    foreach ($audit_ids_to_associate as $audit_id) {
                        array_push($associations, array('company_document_id' => $this->CompanyDocument->id, 'company_audit_id' => $audit_id, 'company_id' => $new_doc['company_id']));
                    }
                    
                    $this->loadModel('AuditToDocument');
                    $this->AuditToDocument->saveMany($associations);
                }
                if (isset($new_doc['batchemail_id'])) {
                    $this->loadModel('BatchemailToDocument');
                    $association = array(
                        'company_document_id' => $this->CompanyDocument->id,
                        'batch_email_id' => $new_doc['batchemail_id'],
                        'company_id' => $new_doc['company_id']
                    );

                    $this->BatchemailToDocument->create();
                    $this->BatchemailToDocument->save($association);
                }

                $this->respondAsJSON(STATUS_CODE_OK, array(
                    'flash' => 'The file, ' . $new_doc['display_name'] . ' has been uploaded!!'
                ));
            } else {
                $this->response->type('json');
                $this->response->statusCode(300);
                $this->response->body(json_encode(array(
                    'flash' => 'Error, the file could not be uploaded!!'
                )));
                $this->response->send();
                $this->_stop();
            }
        }
    }

    /**
     * edit method
     * 
     * Used to edit data related to a document.
     * Uses  CompanyDocument::get_document to check if the current User's company_id and the Document's company_id match.
     * i.e. isOwner
     * 
     * @param int $id The id of the Document
     * 
     * @uses CompanyDocument::get_document get a single document
     * 
     * @access public
     * @version 0.0.1
     * @throws NotFoundException
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return object json object of info related to specified document / image
     */
    public function edit($id = null) {
        $document = $this->CompanyDocument->get_document($id, null, null);
        if ($document) {
            $this->CompanyDocument->id = $id;
            if ($this->CompanyDocument->save($this->request->data)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Document Information Edited'));
            } else {
                $errors = $this->validationErrorsToString($this->CompanyDocument->validationErrors);
                throw new BadRequestException(__($errors));
            }
        } else {
            throw new NotFoundException(__('Document Not Found'));
        }
    }

    /**
     * delete method
     * 
     * AppModel::beforeFind() checks if current user's company_id === document's company_id.
     * i.e. isOwner check
     * 
     * After above check is complete deletes specified document.
     * File deletion is handled by media plugin 
     * - Media/Model/Behaviour/CouplerBehavior::beforeDelete()
     * 
     * @param int $id The id of the Company Site
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array array with flash message.
     */
    public function delete($id = null, $cascade = true) {
        $company_document = $this->CompanyDocument->findById($id);
        if ($company_document) {
            if ($this->CompanyDocument->delete($id)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Document deleted'));
            } else {
                $errors = $this->validationErrorsToString($this->CompanyDocument->validationErrors);
                throw new BadRequestException(__($errors));
            }
        } else {
            throw new NotFoundException(__('Document Not Found'));
        }
    }

    /*
     * method change_company_document_type
     * 
     * Change the Document Type for a document.
     * Used on the CompanyDocumentsList and the AuditDocumentsList pages
     * when a user changes the doc type from the CompanyDocumentType Dropdown.
     * 
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

    public function change_company_document_type() {
        if ($this->request->is('post')) {
            $data = $this->request->input('json_decode');

            $this->CompanyDocument->id = $data->company_document_id;

            if (!$this->CompanyDocument->exists($data->company_document_id)) {
                throw new NotFoundException(__('Invalid CompanyDocument'));
            }

            if ($this->CompanyDocument->save($this->request->data)) {
                switch ($data->company_document_type_id) {
                    case 1:
                        $message = "Document Type set to 'Other'";
                        break;
                    case 2:
                        $message = "Document Type set to 'Sent to Supplier'";
                        break;
                    case 3:
                        $message = "Document Type set to 'Received from Supplier'";
                        break;
                    case 4:
                        $message = "Document Type set to 'Product Specification'";
                        break;
                    case 5:
                        $message = "Document Type set to 'Product Image'";
                        break;
                    case 6:
                        $message = "Document Type set to 'Email Correspondence'";
                        break;
                    case 7:
                        $message = "Document Type set to 'Action Plan'";
                        break;
                    case 8:
                        $message = "Document Type set to 'Annual Report'";
                        break;
                    case 9:
                        $message = "Document Type set to 'Board Notes'";
                        break;
                }
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => $message));
            } else {
                $errors = $this->validationErrorsToString($this->CompanyDocument->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

    /**
     * transfer_audit_to_doc_association method
     * 
     * This is a temporary method.
     * It will be used to modify the Audit To Document relationships.
     * It finds all CompanyDocuments where audit_id != 0 and
     * creates a record for each in the AuditToDocument table.
     * 
     * NOTE:
     * AppModel->beforeFind() must be commented out for this method to work.
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     */
//    public function transfer_audit_to_doc_association() {
//        $this->CompanyDocument->recursive = -1;
//        $docs_result = $this->CompanyDocument->find('all', array(
//            'conditions' => array(
//                'audit_id !=' => 0
//            ),
//            'fields' => array(
//                'id AS company_document_id',
//                'audit_id AS company_audit_id',
//                'company_id AS company_id'
//            )
//        ));
//
//        $docs = Hash::extract($docs_result, '{n}.CompanyDocument');       
//
//        $this->loadModel('AuditToDocument');
//        if($this->AuditToDocument->saveMany($docs)) {
//            $this->respondAsJSON(200, array('flash' => 'Check the database'));
//        }
//
//        die();
//    }
}
