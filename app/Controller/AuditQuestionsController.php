<?php

App::uses('AppController', 'Controller');

/**
 * AuditQuestions Controller
 * 
 * Only index() and view() methods are currently used.
 * There is no ability in the application to add new questions or edit existing questions.
 *
 * @property AuditQuestion $AuditQuestion
 */
class AuditQuestionsController extends AppController {

    var $name = 'AuditQuestions';

    /**
     * index method
     * Returns a list of questions.
     * Questions returned differ for supplier (23 questions) and other users (70 questions)
     * 
     * AuditToQuestion.id as id - This is used to populate responses section on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
     * audit_question_id is used by navigation on the page dashboard/companyAudits/editAudit/184/editAuditQuestions/1
     *
     * @uses AuditQueston::getQuestionsForType
     * @uses AuditQueston::getSupplierQuestionsForType
     *     
     * @param int $audit_questions_category_type_id The id of the audit type to find associated questions
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @edit    2015-02-01<br />
     *          Keith Burton <keith.burton@greenstreets.ie><br />
     *          Refactored finds into model
     *          and added parameter for audit_question_category_type_id<br/>
     * @return array JSON object array of questions
     */
    public function index() {
        $group_id = CakeSession::read('User.group_id');

		
        $audit_question_category_type_id = $this->request->query('audit_question_category_type_id');
		
		if (!$audit_question_category_type_id){
			$audit_question_category_type_id=1;
		}
		


        //get the questions based on the users group type.
       if ($group_id === '5') {
            $audit_questions = $this->AuditQuestion->getSupplierQuestionsForType($audit_question_category_type_id);
        } else {
            $audit_questions = $this->AuditQuestion->getQuestionsForType($audit_question_category_type_id);
        }


        if ($this->request->is('get')) {
            $this->respondAsJSON(STATUS_CODE_OK, Hash::extract($audit_questions, '{n}.AuditQuestion'));
        } else {
            echo $audit_questions;
        }

    }

    /**
     * view method
     * 
     * Not Currently Used - 14/12/02
     * 
     * @param int $id The id of the Category
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return json object.
     */
//    public function view($id = null) {
//        $this->AuditQuestion->recursive = -1;
//        if (!$this->AuditQuestion->exists($id)) {
//            throw new NotFoundException(__('Invalid AuditQuestion'));
//        }
//        $question = $this->AuditQuestion->findById($id);
//        $this->respondAsJSON(STATUS_CODE_OK, $question['AuditQuestion']);
//    }

    /**
     * add method
     * 
     * Not Currently Used - 14/12/02
     * 
     * @access public
     * @throws  BadRequestException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return json object.
     */
//    public function add() {
//        if ($this->request->is('post') || $this->request->is('put')) {
//            $new_question = $this->request->data;
//            $this->AuditQuestion->create();
//            if ($this->AuditQuestion->save($new_question)) {
//                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'New AuditQuestion Added'));
//            } else {
//                $errors = $this->validationErrorsToString($this->AuditQuestion->validationErrors);
//                throw new BadRequestException(__($errors));
//            }
//        }
//    }

    /**
     * edit method
     * 
     * Not Currently Used - 14/12/02
     * 
     * @param int $id The id of the question to edit
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return string Success or Error message
     */
//    public function edit($id = null) {
//        $this->AuditQuestion->id = $id;
//
//        if (!$this->AuditQuestion->exists($id)) {
//            throw new NotFoundException(__('Invalid Company Site id'));
//        }
//        if ($this->request->is('post') || $this->request->is('put')) {
//            Configure::write('debug', 0);
//            $this->disableCache();
//
//            if ($this->AuditQuestion->save($this->request->data)) {
//                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'AuditQuestion Edited'));
//            } else {
//                $errors = $this->validationErrorsToString($this->AuditQuestion->validationErrors);
//                throw new BadRequestException(__($errors));
//            }
//        }
//    }

    /**
     * edit method
     * 
     * Not Currently Used - 14/12/02
     * 
     * @param int $id The id of the question to delete
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return string Success or Error message
     */
//    public function delete($id = null) {
//        $this->AuditQuestion->id = $id;
//        if (!$this->AuditQuestion->exists($id)) {
//            throw new NotFoundException(__('Invalid AuditQuestion id'));
//        }
//
//        if ($this->AuditQuestion->delete($id)) {
//            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'AuditQuestion Deleted'));
//        } else {
//                throw new BadRequestException(__('Error'));
//        }
//    }
}
