<?php

App::uses('AppController', 'Controller');

/**
 * BrandActivityTypes Controller
 *
 * @property BrandActivityType $BrandActivityType
 */
class BrandActivityTypesController extends AppController {

    var $name = 'BrandActivityTypes';

    /**
     * index method
     * 
     * return a list of Brand Activity Types - used in the Compamy Brand Edit form.
     * dashboard/companyProfile/companyBrands/brand
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all Australian States
     */
    public function index() {
        $this->BrandActivityType->recursive = -1;
        $brand_activity_types_list = $this->BrandActivityType->find('all');
        $brand_activity_types = Hash::extract($brand_activity_types_list, '{n}.BrandActivityType');
        $this->respondAsJSON(STATUS_CODE_OK, $brand_activity_types);
    }
}
