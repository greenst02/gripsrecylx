<?php

App::uses('AppController', 'Controller');

/**
 * CompanySuppliers Controller
 *
 * @property CompanySupplier $CompanySupplier
 */
class CompanySuppliersController extends AppController {

    var $name = 'CompanySuppliers';
    private $_count = 0;

    /**
     * index method
     * The company_id is checked in the active session 
     * The Suppliers owned by that company are returned
     * 
     * @param int $company_id company_id is read from the current session
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all Company Suppliers
     */
    public function index() {
        $company_id = CakeSession::read('User.company_id');

        $this->loadModel('User');

        $fields = array(
            'User.id AS id',
            'User.firstname AS firstname',
            'User.lastname AS lastname',
            'User.username AS username',
            'User.supplier_company AS supplier_company',
            'User.email_error AS email_error',
            'User.vendor_code AS vendor_code'
        );

        /**
         * The setting required to filter, sort and limit the data returned to the client side.
         */
        $order = array();   // sort order
        $limit = 25;        // items per page, default 25
        $offset = 0;        // calculated by $limit * the page number
        $filter = '';       // find items based on the string entered in the search box

        $conditions = array(
            'User.group_id' => 5,
            'User.company_id' => $company_id,
        );
        
        if (!empty($this->request->query['filter'])) {
            $filter = '%' . $this->request->query['filter'] . '%';
            $filter_conditions = array(
                "OR" => array(
                    'firstname LIKE ' => $filter,
                    'lastname LIKE ' => $filter,
                    'username LIKE' => $filter,
                    'supplier_company LIKE' => $filter,
                    'vendor_code LIKE' => $filter,

                )
            );
            array_push($conditions, $filter_conditions);
        }

        if (!empty($this->request->query['sorting'])) {
            $sorting = preg_replace(array("/{/", "/}/", '/"/'), "", $this->request->query['sorting']);
            $sorting = preg_replace("/:/", " ", $sorting);
            array_push($order, $sorting);
        }

        if (!empty($this->request->query['limit'])) {
            $limit = $this->request->query['limit'];
        }

        if (!empty($this->request->query['page']) && ($this->request->query['page'] > 0)) {
            $offset = $limit * $this->request->query['page'];
        }

        $this->User->recursive = -1;
        $company_suppliers_list = $this->User->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $limit,
            'offset' => $offset
        ));

        $this->_set_count($conditions);

        $company_suppliers = Hash::extract($company_suppliers_list, '{n}.User');
        $this->respondAsJSON(STATUS_CODE_OK, $company_suppliers, $this->_count);
    }

    private function _set_count($conditions = NULL) {
        $this->loadModel('User');

        $count = $this->User->find('count', array(
            'conditions' => $conditions
        ));
        $this->_count = (int) $count;
    }

    /**
     * view method
     * 
     * Finds the supplier by user_id
     * Ownership must be checked first.
     * 
     * @uses CompanySuppliersController::_find_supplier to check if the current User's company_id and Supplier's company_id match.
     * 
     * @param int $id The supplier's user_id that is sent from client side.
     * @throws NotFoundException if not owner
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return object json object.
     */
    public function view($id = null) {
        $company_supplier = $this->_find_supplier($id);
        if ($company_supplier) {
            $this->respondAsJSON(STATUS_CODE_OK, $company_supplier['User']);
        } else {
            throw new NotFoundException(__('Supplier Not Found'));
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $company_id = CakeSession::read('User.company_id');

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->loadModel('User');

            $new_supplier = array(
                'User' => array(
                    'username' => $this->request->data['username'],
                    'company_id' => $company_id,
                    'group_id' => 5,
                    'firstname' => $this->request->data['firstname'],
                    'lastname' => $this->request->data['lastname'],
                    //fixes to enable the supplier by default but put a random password so
                    //supplier cannot be accessed.
                    'user_enabled_status_id' => 1,
                    'password' => wordwrap(sha1($this->request->data['username'] . rand(0, 100)), 20),
                    'supplier_company' => $this->request->data['supplier_company'],
                    'vendor_code' => $this->request->data['vendor_code']
                )
            );

            $this->User->create();
//                $supplier_added = $this->User->saveAssociated($new_supplier, array('deep' => true));
            //bug fix added test for validation 
            $this->User->set($new_supplier);
            if ($this->User->validates()) {
                $supplier_added = $this->User->save($new_supplier);
            } else {
                $supplier_added = false;
            }

            if ($supplier_added) {
                $message = 'New Supplier Added';
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => $message));
            } else {
                $errors = $this->validationErrorsToString($this->User->validationErrors);
                throw new BadRequestException(__($errors));
            }
        }
    }

    /**
     * edit method
     * 
     * Update a specified Supplier's details.
     * Ownership must be checked first.
     * 
     * @uses CompanySuppliersController::_find_supplier to check if the current User's company_id and Supplier's company_id match.
     * 
     * @param int $id The supplier's user_id that is sent from client side.
     * @throws NotFoundException if not owner
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array Array with success message
     */
    public function edit($id = null) {
        $company_supplier = $this->_find_supplier($id);
        if ($company_supplier) {
            $this->User->id = $id;
            if ($this->User->save($this->request->data)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Supplier Edited'));
            } else {
                $errors = $this->validationErrorsToString($this->User->validationErrors);
                throw new BadRequestException(__($errors));
            }
        } else {
            throw new NotFoundException(__('Supplier Not Found'));
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        throw new NotImplementedException(__('TO DO: User::Delete'));

//        $this->loadModel('User');
//        $this->User->id = $id;
//        if (!$this->User->exists($id)) {
//            throw new NotFoundException(__('Invalid company brand id'));
//        }
//
//        if ($this->User->delete($id)) {
//            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Supplier Deleted'));
//        } else {
//            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Error'));
//        }
    }

    /**
     * _find_supplier method
     * 
     * Used to find the specified supplier, checks if supplier's company_id is === to this user's company_id
     * i.e. isOwner
     * 
     * returns company_supplier array or false if not found
     * 
     * @param int $supplier_id the id of the supplier
     * @param int $user_company_id The User's company_id is read from the session
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array $company_supplier The Company Supplier data as an array or false if not found
     */
    private function _find_supplier($supplier_id) {
        $company_id = CakeSession::read('User.company_id');

        $this->loadModel('User');
        $this->User->recursive = -1;

        $conditions = array(
            'User.id' => $supplier_id,
            'User.company_id' => $company_id
        );

        $fields = array(
            'User.id  as id',
            'User.username AS username',
            'User.firstname AS firstname',
            'User.lastname AS lastname',
            'User.supplier_company AS supplier_company',
            'User.vendor_code AS vendor_code'
        );

        $company_supplier = $this->User->find('first', array(
            'fields' => $fields,
            'conditions' => $conditions
        ));


        if ($company_supplier) {
            return $company_supplier;
        } else {
            return false;
        }
    }

}
