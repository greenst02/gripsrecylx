<?php

App::uses('AppController', 'Controller');

/**
 * HelpSections Controller
 *
 * @property HelpSection $HelpSection
 */
class HelpSectionsController extends AppController {

    /**
     * index method
     * return a list of audit enable statuses - used only by admins and is accessed from the companyAuditsList and companyAuditDetails form.
     * At this time there are only 2 statuses; enabled and admin_only.
     * More could be added later if required i.e. manager access only, supplier access only etc.
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all audit_enabled_statuses
     */
    public function index() {
        $this->HelpSection->recursive = 1;
        $help_setions = $this->HelpSection->find('all');

        $this->respondAsJSON(STATUS_CODE_OK, $help_setions);
    }

    /**
     * index method
     * return a list of audit enable statuses - used only by admins and is accessed from the companyAuditsList and companyAuditDetails form.
     * At this time there are only 2 statuses; enabled and admin_only.
     * More could be added later if required i.e. manager access only, supplier access only etc.
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all audit_enabled_statuses
     */
    public function update_help_subsection() {

        $data = $this->request->input('json_decode');
        $this->loadModel('HelpSubsection');

        if ($this->HelpSubsection->save($data)) {
            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Help Updated.'));
        } else {
            $errors = $this->validationErrorsToString($this->HelpSubsection->validtionErrors);
            throw new BadRequestException(__($errors));
        }
    }

    /**
     * index method
     * return a list of audit enable statuses - used only by admins and is accessed from the companyAuditsList and companyAuditDetails form.
     * At this time there are only 2 statuses; enabled and admin_only.
     * More could be added later if required i.e. manager access only, supplier access only etc.
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all audit_enabled_statuses
     */
    public function add_new_subsection() {

        $data = $this->request->input('json_decode');
        $this->loadModel('HelpSubsection');

        if ($this->HelpSubsection->save($data)) {
            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Help Updated.'));
        } else {
            $this->respondAsJSON(STATUS_CODE_BAD_REQUEST, array('flash' => 'Something went wrong, please try again.'));
        }
    }

    /**
     * index method
     * return a list of audit enable statuses - used only by admins and is accessed from the companyAuditsList and companyAuditDetails form.
     * At this time there are only 2 statuses; enabled and admin_only.
     * More could be added later if required i.e. manager access only, supplier access only etc.
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all audit_enabled_statuses
     */
    public function add_new_help_section() {

        $data = $this->request->input('json_decode');

        $this->HelpSection->create();

        $new_help_section = array(
            'HelpSection' => array(
                'title' => $data->title
            ),
            'HelpSubsection' => array(
                array(
                    'title' => $data->subsection->title
                )
            )
        );

        $new_help_section_result = $this->HelpSection->saveAssociated($new_help_section, $option = array(
            'validate' => true,
            'atomic' => true
        ));

        if ($new_help_section_result) {
            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Help Updated.'));
        } else {
            throw new BadRequestException(__('All Fields Required.'));
        }
    }
    
    /**
     * index method
     * return a list of audit enable statuses - used only by admins and is accessed from the companyAuditsList and companyAuditDetails form.
     * At this time there are only 2 statuses; enabled and admin_only.
     * More could be added later if required i.e. manager access only, supplier access only etc.
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing all audit_enabled_statuses
     */
    public function update_section_title() {

        $data = $this->request->input('json_decode');
        $this->loadModel('HelpSubsection');

        if ($this->HelpSection->save($data)) {
            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Title Updated.'));
        } else {
            $this->respondAsJSON(STATUS_CODE_BAD_REQUEST, array('flash' => 'Something went wrong, please try again.'));
        }
    }
}
