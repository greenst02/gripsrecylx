<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

//require_once(APP.DS.'Vendor'.DS.'php'.DS.'pseudocrypt'.DS.'PseudoCrypt.php');

/**
 * BatchEmails Controller
 *
 * @property BatchEmail $BatchEmail
 */
class BatchEmailsController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('opened');
    }

    var $name = 'BatchEmails';
    private $_count = 0;

    private function _set_count($conditions = NULL) {
        $count = $this->BatchEmail->find('count', array(
            'conditions' => $conditions
        ));
        $this->_count = (int) $count;
    }

    private static function _email_connection() {
        $Email_object = new CakeEmail();

        /* SMTP Options */
        $Email_object->config(array(
            'port' => '25',
            'timeout' => '30',
            'host' => 'smtp.greenstreets.ie',
            'username' => 'greenst32',
            'password' => 'GSsupport2014#!',
            'transport' => 'Smtp'
        ));

        return $Email_object;
    }

    public function emails() {
        $fields = array(
            'BatchEmail.id AS id',
            'BatchEmail.subject AS subject',
            'BatchEmail.created AS created',
            'BatchEmail.modified AS modified',
            'BatchEmail.email_sent AS email_sent',
            'BatchEmail.due_date AS due_date',
        );

        /**
         * The setting required to filter, sort and limit the data returned to the client side.
         */
        $order = array();   // sort order
        $limit = 10;        // items per page, default 10
        $offset = 0;        // calculated by $limit * the page number
        $filter = '';       // find items based on the string entered in the search box

        $conditions = array();
        
        if (!empty($this->request->data['type'])) {
            if ($this->request->data['type'] === 'sent') {
                array_push($conditions, array(
                    'BatchEmail.email_sent !=' => NULL
                ));
            }
            if ($this->request->data['type'] === 'draft') {
                array_push($conditions, array(
                    'BatchEmail.email_sent' => NULL
                ));
            }
        }
        
        if (!empty($this->request->data['filter'])) {
            $filter = '%' . $this->request->data['filter'] . '%';
            $filter_conditions = array(
                "OR" => array(
                    'BatchEmail.subject LIKE ' => $filter,
                    'BatchEmail.created LIKE ' => $filter
                )
            );
            array_push($conditions, $filter_conditions);
        }

        if (!empty($this->request->data['sorting'])) {
            $sorting = preg_replace(array("/{/", "/}/", '/"/'), "", $this->request->data['sorting']);
            $sorting = preg_replace("/:/", " ", $sorting);
            array_push($order, $sorting);
        }

        if (!empty($this->request->data['limit'])) {
            $limit = $this->request->data['limit'];
        }

        if (!empty($this->request->data['page']) && ($this->request->data['page'] > 0)) {
            $offset = $limit * $this->request->data['page'];
        }

        $this->BatchEmail->recursive = -1;
        $company_batch_emails_list = $this->BatchEmail->find('all', array(
            'fields' => $fields,
            'conditions' => $conditions,
            'order' => $order,
            'limit' => $limit,
            'offset' => $offset
        ));

        $this->_set_count($conditions);

        $batch_emails = Hash::extract($company_batch_emails_list, '{n}.BatchEmail');
        $this->respondAsJSON(STATUS_CODE_OK, $batch_emails, $this->_count);
    }

    /**
     * ***********************************************************************************
     * ***********************************************************************************
     */
    public function add() {
        $company_id = CakeSession::read('User.company_id');
        $draft_email_data = array(
            'subject' => 'Draft Subject',
            'message' => 'Draft Message',
            'layout_name'=>'musgrave_layout',
            'template_name'=>'musgrave_suppliers',
            'company_id' => $company_id
        );

        $this->BatchEmail->create();
        if ($this->BatchEmail->save($draft_email_data)) {
            $this->respondAsJSON(200, array('id' => $this->BatchEmail->id));
        } else {
            $errors = $this->validationErrorsToString($this->BatchEmail->validationErrors);
            throw new BadRequestException(__($errors));
        }
    }

    /**
     * edit method
     * 
     * Finds the contact by id and also checks the session company_id
     * to ensure the contact is only accessible to owners.
     * 
     * After above check is complete, updates existing contact with new data
     * 
     * @param int $id The id of the Company Contact
     * @param int $company_id is added to the find() method in appModel::beforeFind()
     * 
     * @uses AppModel::beforeFind() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return object json object.
     */
    public function edit($id = null) {
        $this->BatchEmail->recursive = -1;
        $batch_email = $this->request->data;
        $batch_email['modified'] = date('Y-m-d H:i:s');
        if ($this->BatchEmail->findById($id)) {
            $this->BatchEmail->id = $id;
            if ($this->BatchEmail->save($batch_email)) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Draft Email Edited'));
            } else {
                $errors = $this->validationErrorsToString($this->BatchEmail->validationErrors);
                throw new BadRequestException(__($errors));
            }
        } else {
            throw new NotFoundException(__('Draft Email Not Found'));
        }
    }

    public function send_all() {
        $this->BatchEmail->id = $this->request->data['id'];

        $this->BatchEmail->recursive = -1;
        $email_content_result = $this->BatchEmail->findById($this->BatchEmail->id);

        //use PseudoCrypt to create a two-way hash of the id as the tracking_code for batch
        //$tracking_code_update = $this->BatchEmail->saveField('tracking_code', PseudoCrypt::hash($this->BatchEmail->id));
        //$tracking_code_update = $this->BatchEmail->saveField('tracking_code',base_convert($this->BatchEmail->id, 10, 36));
        $tracking_code_update = $this->BatchEmail->saveField('tracking_code',substr(md5($this->BatchEmail->id),0,8));
 

        $tracking_code = $tracking_code_update['BatchEmail']['tracking_code'];
        $email_content = $email_content_result['BatchEmail'];
        $email_layout_name = $email_content['layout_name'];
        $email_template_name = $email_content['template_name'];
        $check_individual_attachment = $email_content['check_individual_attachment'];



        $this->loadModel('BatchemailToSupplier');
        $this->BatchemailToSupplier->recursive = -1;
        $supplier_ids_result = $this->BatchemailToSupplier->find('all', array(
            'fields' => array('id', 'user_id'),
            'conditions' => array(
                'batch_email_id' => $this->BatchEmail->id
            )
        ));

        $supplier_ids = hash::extract($supplier_ids_result, '{n}.BatchemailToSupplier.user_id');
        $batchemail_to_supplier_ids = hash::extract($supplier_ids_result, '{n}.BatchemailToSupplier.id');

        $company_id = CakeSession::read('User.company_id');
        $prepared_data = array();
        foreach ($batchemail_to_supplier_ids as $key => $batchemail_to_supplier_id) {
            array_push($prepared_data, array(
                'id' => $batchemail_to_supplier_id,
                'user_id' => $supplier_ids[$key],
                'status' => 'Queued',
                'company_id' => $company_id,
                //two way hash-code of the ID field
                //'tracker' => PseudoCrypt::hash($batchemail_to_supplier_id)
                'tracker' => substr(md5($batchemail_to_supplier_id),0,8)
                ));
        }

        $this->BatchemailToSupplier->saveMany($prepared_data, array(
            'callbacks' => true
        ));

        /**
         * Retrieve the email addresses for the suppliers
         */

        // $this->loadModel('User');
        // $this->User->recursive = -1;
        // $email_addresses_result = $this->User->find('all', array(
        //     'fields' => array(
        //         'username',
        //         'vendor_code',
        //     ),
        //     'conditions' => array(
        //         'id' => $supplier_ids
        //     )
        // ));


        // $recipients = hash::extract($email_addresses_result, '{n}.User');


        $this->BatchemailToSupplier->recursive=1;
        $recipients = $this->BatchemailToSupplier->find('all', array(
            'fields' => array(
                'User.username',
                'User.vendor_code',
                'User.supplier_company',
                'BatchemailToSupplier.tracker',
				'BatchemailToSupplier.id'
            ),
            'conditions' => array(
                'batch_email_id' => $this->BatchEmail->id
            )
        ));

        $recipients = hash::merge(hash::extract($recipients,'{n}.User'),
                                  hash::extract($recipients,'{n}.BatchemailToSupplier'));


        $this->loadModel('Company');
        $this->Company->recursive = -1;
        $company_name_result = $this->Company->find('first', array(
            'fields' => array('trading_name'),
            'conditions' => array(
                'id' => CakeSession::read('User.company_id')
            )
        ));

        $company_name = $company_name_result['Company']['trading_name'];

        $this->loadModel('BatchemailToDocument');
        $this->BatchemailToDocument->recursive = -1;
        $attached_doc_ids_result = $this->BatchemailToDocument->find('all', array(
            'fields' => array('company_document_id'),
            'conditions' => array(
                'batch_email_id' => $this->BatchEmail->id
            )
        ));

        $attached_doc_ids = hash::extract($attached_doc_ids_result, '{n}.BatchemailToDocument.company_document_id');

        $this->loadModel('CompanyDocument');
        $this->CompanyDocument->recursive = -1;

        $attachments = array();

        foreach ($attached_doc_ids as $document_id) {
            $attachment = $this->CompanyDocument->get_file_path($document_id, 'original');
            array_push($attachments, array($attachment['name'] => APP . $attachment['path']));
        }




      //  $email_template_name = 'musgrave_suppliers';
      //  $email_layout = 'musgrave_layout';        


        foreach ($recipients as $key => $recipient) {



            $individual_attachments = $attachments;
            //check to see if we use individual attachments and add them in
            if($check_individual_attachment) {

                $individual_attachment = $this->__getIndividualAttachment($recipient);
                if (isset($individual_attachment)){

                    array_push($individual_attachments,array($individual_attachment['name']=>APP.$individual_attachment['path']));
                }

            }


            $this->_send_email($email_template_name,
                               $email_layout_name,
                                '', 
                                $recipient, 
                               $email_content['subject'], 
                               $email_content['message'], 
                               $company_name, 
                               $bcc = NULL, 
                               $this->BatchEmail->id, 
                               $individual_attachments, 
                               $recipient['tracker'],
                               $email_content['due_date']);
            usleep(1000);
        }

        $this->BatchEmail->saveField('email_sent', date('Y-m-d H:i:s'));

        $this->respondAsJSON(200, array('id' => $this->BatchEmail->id, 'flash' => 'email ' . $this->BatchEmail->id . ' sent to your suppliers.'));
    }


    private function __getIndividualAttachment($recipient) {

        //Check for individual attachment
        $individual_attachment_name = 'MWPI_SupplierSpecificationsTemplate_'.$recipient['supplier_company'].'.xlsx';        
        $attachment = $this->CompanyDocument->get_file_path_by_name($individual_attachment_name,'original');        
        return $attachment;


    }





    public function view($id = null) {
        $this->BatchEmail->recursive = -1;
        $email = $this->BatchEmail->findById($id);
        if ($email) {
            $this->respondAsJSON(STATUS_CODE_OK, $email['BatchEmail']);
        } else {
            throw new NotFoundException(__('Email Not Found'));
        }
    }

    /**
     * view_recipients method
     * Return a list of suppliers associated with an Email.
     * 
     * @param int $batch_email_id
     * 
     * @uses CompanyDocument::get_documents 
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing Documents associated with an Audit and Not associated with a Supplier
     */
    public function view_recipients() {
        $batch_email_id = $this->request->data['batch_email_id'];

        $this->loadModel('BatchemailToSupplier');
        $conditions = array(
            'BatchemailToSupplier.batch_email_id' => $batch_email_id
        );

        $fields = array(
            'BatchemailToSupplier.id AS id',
            'BatchemailToSupplier.opened AS opened',
            'BatchemailToSupplier.status AS status',
            'BatchemailToSupplier.tracker AS tracker',
            'User.username AS username',
            'User.firstname AS firstname',
            'User.lastname AS lastname',
            'User.supplier_company AS supplier_company',
            'User.vendor_code AS vendor_code',
            'User.email_error AS email_error'
        );

        /**
         * The setting required to filter, sort and limit the data returned to the client side.
         */
        $order = array();   // sort order
        $limit = 10;        // items per page, default 10
        $offset = 0;        // calculated by $limit * the page number
        $filter = '';       // find items based on the string entered in the search box

        if (!empty($this->request->data['filter'])) {
            $filter = '%' . $this->request->data['filter'] . '%';
            $filter_conditions = array(
                "OR" => array(
                    'BatchemailToSupplier.tracker LIKE ' => $filter,
                    'User.username LIKE ' => $filter,
                    'User.firstname LIKE ' => $filter,
                    'User.lastname LIKE ' => $filter,
                    'User.username LIKE ' => $filter,
                    'User.supplier_company LIKE ' => $filter,
                    'User.vendor_code LIKE ' => $filter
                )
            );
            array_push($conditions, $filter_conditions);
        }

        if (!empty($this->request->data['sorting'])) {
            $sorting = preg_replace(array("/{/", "/}/", '/"/'), "", $this->request->data['sorting']);
            $sorting = preg_replace("/:/", " ", $sorting);
            array_push($order, $sorting);
        }

        if (!empty($this->request->data['limit'])) {
            $limit = $this->request->data['limit'];
        }

        if (!empty($this->request->data['page']) && ($this->request->data['page'] > 0)) {
            $offset = $limit * $this->request->data['page'];
        }

        $batch_email_to_suppliers_query = $this->BatchemailToSupplier->find('all', array(
            'conditions' => $conditions,
            'fields' => $fields,
            'order' => $order,
            'limit' => $limit,
            'offset' => $offset
        ));

        $a1 = Hash::extract($batch_email_to_suppliers_query, '{n}.BatchemailToSupplier');
        $a2 = Hash::extract($batch_email_to_suppliers_query, '{n}.User');

        $batch_email_to_suppliers = Hash::merge($a1, $a2);
        $total_items = sizeof($batch_email_to_suppliers);

        $this->respondAsJSON(STATUS_CODE_OK, $batch_email_to_suppliers, $total_items);
    }

    public function modify_recipients() {
        $company_id = CakeSession::read('User.company_id');

        $batch_email_id = $this->request->data['batch_email_id'];
        $sql_query = 'SELECT
                    users.id AS supplier_id, 
                    users.username AS username,
                    users.firstname AS firstname,
                    users.lastname AS lastname,
                    users.email_error AS email_error,
                    users.supplier_company AS supplier_company,
                    users.vendor_code AS vendor_code,
                    (
                        SELECT 
                            batchemail_to_supplier.id
                        FROM 
                            batchemail_to_supplier
                        WHERE 
                            batchemail_to_supplier.user_id = users.id
                        AND
                            batchemail_to_supplier.batch_email_id = ' . $batch_email_id . '
                    )
                    AS batchemail_to_supplier_id,
                    ' . $batch_email_id . ' AS batch_email_id,
                    (
                        SELECT 
                            batchemail_to_supplier.opened
                        FROM 
                            batchemail_to_supplier
                        WHERE 
                            batchemail_to_supplier.user_id = users.id 
                        AND
                            batchemail_to_supplier.batch_email_id = ' . $batch_email_id . '
                    )
                    AS email_opened
                    FROM
                        users
                    WHERE
                        users.company_id = ' . $company_id . '
                    AND
                        users.email_error = 0
                    AND
                        users.group_id = 5';

        if (!empty($this->request->data['filter'])) {
            $filter = '%' . $this->request->data['filter'] . '%';
            $filter_conditions = ' AND (users.username LIKE "' . $filter . '"'
                    . ' OR users.firstname LIKE "' . $filter . '"'
                    . ' OR users.lastname LIKE "' . $filter . '"'
                    . ' OR users.supplier_company LIKE "' . $filter . '"'
                    . ' OR users.vendor_code LIKE "' . $filter . '"'
                    . ')';

            $sql_query = $sql_query . $filter_conditions;
        }

        $sorting = '';
        foreach ($this->request->data['sorting'] as $key => $value) {
            $sorting = $sorting . $key . ' ' . $value;
        }

        $sql_query = $sql_query . ' ORDER BY ' . $sorting;

        if (!empty($this->request->data['limit'])) {
            $sql_query = $sql_query . ' LIMIT ' . ($this->request->data['page'] * $this->request->data['limit']) . ', ' . $this->request->data['limit'];
        }

        $this->loadModel('User');
        $total_items = $this->User->find('count', array(
            'conditions' => array(
                'User.company_id' => $company_id,
                'User.group_id' => 5
            )
        ));

        $query_result = $this->User->query($sql_query);

        $a1 = Hash::extract($query_result, '{n}.users');
        $a2 = Hash::extract($query_result, '{n}.0');

        $batchemail_to_suppliers = Hash::merge($a1, $a2);

        $batchemail_to_suppliers_update = array();
        foreach ($batchemail_to_suppliers as $batchemail_to_supplier) {
            if ($batchemail_to_supplier['batchemail_to_supplier_id']) {
                $batchemail_to_supplier['checked'] = 1;
                array_push($batchemail_to_suppliers_update, $batchemail_to_supplier);
            } else {
                $batchemail_to_supplier['checked'] = 0;
                array_push($batchemail_to_suppliers_update, $batchemail_to_supplier);
            }
        }

        $this->respondAsJSON(STATUS_CODE_OK, $batchemail_to_suppliers_update, $total_items);
    }

    /**
     * view_attachments method
     * Return a list of Documents associated with an Audit and Not a Supplier.
     * 
     * @param int $batch_email_id
     * 
     * @uses CompanyDocument::get_documents 
     * 
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array JSON object array listing Documents associated with an Audit and Not associated with a Supplier
     */
    public function view_attachments() {
        $batch_email_id = $this->request->data['batch_email_id'];

        $this->loadModel('CompanyDocument');
        $doc_ids = $this->_get_ids_of_attached_files($batch_email_id);

        $conditions = array(
            'CompanyDocument.id' => $doc_ids,
            'CompanyDocument.company_document_type_id' => 13
        );

        $this->respondAsJSON(STATUS_CODE_OK, $this->CompanyDocument->get_documents(null, $conditions));
    }

    private function _get_ids_of_attached_files($batch_email_id) {
        $this->loadModel('BatchemailToDocument');
        $doc_ids_query = $this->BatchemailToDocument->find('all', array(
            'fields' => array('BatchemailToDocument.company_document_id'),
            'conditions' => array(
                'BatchemailToDocument.batch_email_id' => $batch_email_id
            )
        ));

        return Hash::extract($doc_ids_query, '{n}.BatchemailToDocument.company_document_id');
    }

    /**
     * Sends an single email.
     * Called repeatedly by send_all method
     * 
     * 
     * @param type $template_name
     * @param type $layout_name
     * @param type $from
     * @param type $to
     * @param type $subject
     * @param type $message
     * @param type $company_name
     * @param type $bcc
     * @param type $message_id
     * @return boolean
     */
    private function _send_email($template_name = NULL, 
                                 $layout_name=NULL, 
                                 $from = NULL, 
                                 $to = NULL, 
                                 $subject = NULL, 
                                 $message = NULL, 
                                 $company_name = NULL, 
                                 $bcc = NULL, 
                                 $message_id = NULL, 
                                 $attachments = NULL, 
                                 $tracking_code, 
                                 $due_date
                                 ) {

        /*        $Email_object = $this->_email_connection();

          $Email_object->template($template_name);
          $Email_object->from('bandq@greenstreets.ie');
          $Email_object->to($to['username']);
          //        $Email_object->bcc($bcc);
          $Email_object->subject($subject . '. VID:[' . $to['vendor_code'] . ']' . ' BID:[' . $tracking_code . ']');
          $Email_object->messageId(true, $message_id);
          $Email_object->message($message);
          $Email_object->addHeaders(array('X-GSERBETSID' => $tracking_code));

          foreach ($attachments as $attachment) {
          $Email_object->addAttachments($attachment);
          }

          $Email_object->returnPath('bandq@greenstreets.ie');
          $Email_object->emailFormat('html');

          $Email_object->viewVars(array(
          'subject' => $subject,
          'message' => $message,
          'message_id' => $message_id,
          'tracking_code' => $tracking_code,
          'to' => $to['username']
          ));
         */


        $fromEmail = 'musgraves@greenstreets.ie';
     //   $fromEmail = 'bandq@greenstreets.ie';
     //   $username = 'greenst32';
     //   $password = 'GSsupport2014#!';
        $username = 'greenst25';
        $password = 'Durpood9';

        $settings = array('settings' => array('config' => array(
                    'port' => '25',
                    'timeout' => '30',
                    'host' => 'smtp.greenstreets.ie',
                    'username' => $username,
                    'password' => $password,
                    'transport' => 'Smtp'),
                'template' => $template_name,
                'layout' =>$layout_name,
                'from' => $fromEmail,
                'to' => $to['username'],
                'subject' => $subject . '. [Vendor Code: ' . $to['vendor_code'] . ']' . ' [Request #: ' . $tracking_code . ']',
                'messageId' => array(true, $message_id),
                'message' => $message,
                'addHeaders' => array('X-GSERBETSID' => $tracking_code),
                'returnPath' => $fromEmail,
                'emailFormat' => 'html',
                'vars' => array('attachments' => $attachments,
                    'tracking_code' => $tracking_code,
                    'message_id' => $message_id,
                    'vendor_code'=> $to['vendor_code'],
					'batchemail_to_supplier_id'=>$to['id'],
                    'due_date' => $due_date
                )
        ));
        //Add Email as QueuedTask for process async in queuing system
        $this->loadModel('Queue.QueuedTask');
        $this->QueuedTask->createJob('GripsEmail', $settings);
//        $Email_object->send();

        return true;
    }

    /**
     * Accessed when a user opens an email.
     * Records the user and time they opened the email to database.
     */
    public function opened($tracking_code,$imagename) {



        //
        // $id = PseudoCrypt::unhash($tracking_code);
        // $id = intval($tracking_code,36);

        // $tracking_code = $this->request->query['tracking_code'];
        // $message_id = $this->request->query['message_id'];
        // $user_email = $this->request->query['user_email'];

        // $this->loadModel('User');
        // $this->User->recursive = -1;
        // $user = $this->User->find('first', array(
        //     'conditions' => array(
        //         'username' => $user_email
        //     ),
        //     'fields' => array('id')
        // ));

        // $user_id = $user['User']['id'];

        $this->loadModel('BatchemailToSupplier');
        $this->BatchemailToSupplier->recursive = -1;
        $check_opened_result = $this->BatchemailToSupplier->find('first',array(
                                                                 'conditions'=>array('tracker'=>$tracking_code,
                                                                                     'opened' => null)));
        

        if ($check_opened_result) {
            $this->BatchemailToSupplier->id = $check_opened_result['BatchemailToSupplier']['id'];
			$saveData = array('id'=>$check_opened_result['BatchemailToSupplier']['id'],'opened'=>date('Y-m-d H:i:s'));
            $this->BatchemailToSupplier->save($saveData);
			//if ($this->BatchemailToSupplier->saveField('opened', date('Y-m-d H:i:s'))) {
               // $message = 'Message with id:' . $message_id . ' and tracking code: ' . $tracking_code . ' opened by ' . $user_email;
            //} else {
               // $message = $this->validationErrorsToString($this->BatchEmail->validationErrors);
            //}
        }

//            $Email = new CakeEmail();
//
//            /* SMTP Options */
//            $Email->config(array(
//                'port' => '25',
//                'timeout' => '30',
//                'host' => 'smtp.greenstreets.ie',
//                'username' => 'greenst32',
//                'password' => 'GSsupport2014#!',
//                'transport' => 'Smtp'
//            ));
//
//            $Email->template('email_opened');
//            $Email->from('bandq@greenstreets.ie');
//            $Email->to('bandq@greenstreets.ie');
//            $Email->subject('Email Opened by ' . $user_email . '. Message ID: ' . $message_id . '. Tracking Header ID: ' . $tracking_code . '.');
//            $Email->messageId(true, $message_id);
//            $Email->message($message);
////        $Email->attachments('attachments');
//            $Email->returnPath('bandq@greenstreets.ie');
//            $Email->emailFormat('html');
//
//            $Email->viewVars(array(
//                'message' => $message,
//                'message_id' => $message_id
//            ));
//
//            $Email->send();

        
        //return the image to the browser
        $this->autoRender = false;
        $filePath = WWW_ROOT.'bin/assets/images/'.$imagename; 
        $this->response->file(
                $filePath,
                array(
                    'name' => $filePath
                )
            );
        return $this->response;
    
    }

    /**
     * modify_batchemail_to_suppliers method
     * 
     * On the client side, this method is called from 
     * dashboard.documents module 
     * -> BatchEmailToSuppliersModalCtrl
     * 
     * This method is called when the user clicks "Update Recipients".
     * It updates calls other methods to check BatchemailToSuppliers relationship exists and 
     * create or delete the BatchemailToSupplier relationships depending on submitted data.
     * 
     * @uses BatchemailToSupplier::save_many 
     * @uses BatchemailToSupplier::delete_many 
     * 
     * @param int $batch_email_id The id of the document to check associations against.
     * @param array $batch_email_to_suppliers An array with the users_id as the key and a value of true or false
     * @access public
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array array with flash message stating 'Recipients updated'
     */
    public function modify_batchemail_to_suppliers() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $batch_email_to_suppliers = $this->request->data;

            $batch_email_id = $batch_email_to_suppliers['batch_email_id'];
            $suppliers = $batch_email_to_suppliers['batch_email_to_suppliers'];

            $create_many = array();
            $delete_many = array();

            $company_id = CakeSession::read('User.company_id');

            $this->loadModel('BatchemailToSupplier');

            foreach ($suppliers as $supplier_id => $val) {
                $exists = $this->BatchemailToSupplier->batch_email_to_supplier_exists($batch_email_id, $supplier_id);
                if ($exists) {
                    if (!$val) {
                        array_push($delete_many, $exists);
                    }
                } else {
                    if ($val) {
                        $batch_email_to_supplier = array(
                            'company_id' => $company_id,
                            'user_id' => $supplier_id,
                            'batch_email_id' => $batch_email_id
                        );
                        array_push($create_many, $batch_email_to_supplier);
                    }
                }
            }

            if (!$create_many && !$delete_many) {
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'No changes detected'));
            } else {
                if ($create_many) {
                    $this->BatchemailToSupplier->save_many($create_many);
                }
                if ($delete_many) {
                    $this->BatchemailToSupplier->delete_many($delete_many);
                }
                $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Recipients updated'));
            }
        }
    }

    /**
     * *********************************************************************************************
     * **************************************** IMAP ***********************************************
     * *********************************************************************************************
     */
//    public function mail_boxes() {
//        $hostname = "{connect.mailwall.com:143}";
//        $username = "greenst32";
//        $password = "GSsupport2014#!";
//
//        /** connect to imap */
////        $inbox = imap_open($hostname, $username, $password) or die(‘Cannot connect to Gmail: ‘ . imap_last_error());
//        $imap = imap_open($hostname, $username, $password) or die('cannot connect: ' . imap_last_error());
//
//        $list = imap_list($imap, "{connect.mailwall.com:143}", "*");
//
//        pr($list);
//        die();
//    }

    /**
     * Connect to imap and download last 24 hours of emails.
     * search for returned emails and save the intended recipient as a bad email address
     * 
     * @return type
     */
    public function check_emails() {
        $this->loadModel('EmailMonitor');
        $count = $this->EmailMonitor->check_emails();
        $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Mail box proccessed..found' . $count . ' emails'));
    }

    /**
     * delete method
     * 
     * AppModel:: beforeDelete() checks if current user's company_id === contact's company_id.
     * i.e. isOwner check
     * 
     * After above check is complete, deletes specified contact
     * 
     * @param int $id The id of the Company Contact
     * 
     * @uses AppModel::beforeDelete() $company_id is added to the find() method
     * 
     * @access public
     * @throws  NotFoundException
     * @version 0.0.1
     * @author Patrick Ward <patrick.ward@greenstreets.ie>
     * @return array array with flash message.
     */
    public function delete($id = null) {
        $this->BatchEmail->id = $id;
        if ($this->BatchEmail->delete($id)) {
            $this->respondAsJSON(STATUS_CODE_OK, array('flash' => 'Draft Email Deleted'));
        } else {
            throw new BadRequestException(__('Sent emails cannot be deleted'));
        }
    }

}
