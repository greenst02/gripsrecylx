import { Injectable } from '@angular/core';

@Injectable()
export class AppInfoService {
  constructor() {}

  public get title() {
    return 'GRIPSRecylx';
  }

  public get company() {
    return 'Greenstreets'
  }

  public get currentYear() {
    return new Date().getFullYear();
  }
}
