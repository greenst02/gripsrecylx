import { Component } from '@angular/core';
import { DxDataGridModule,
  DxBulletModule,
  DxTemplateModule,DxSelectBoxModule, DxButtonModule } from 'devextreme-angular';
import DataSource from 'devextreme/data/data_source';
import { AuditService } from '../../shared/services/audit.service'
import {Router, ActivatedRoute} from '@angular/router';


@Component({
  templateUrl: 'audits.component.html',
  providers: [AuditService]
})

export class AuditsComponent {

    audit_type_name: string;
    category_id: number;
    dataSource: any;
    categoriesData: any;
    collapsed = false;  
    refreshModes: string[];
    refreshMode: string;
    contentReady = (e) => {
        if(!this.collapsed) {
            this.collapsed = true;
          //  e.component.expandRow(["EnviroCare"]);
        }
    };

    rowClicked = (e) => {
        console.log("============row clicked");
        console.log(e);

        this.navigateTo('/audit',e.data.id);
    };

    cellDblClicked = (e) => {
      console.log("============cell double clicked");
      console.log(e);

      this.navigateTo('/audit',e.row.data.id);
  };

    constructor(service: AuditService, private router: Router, private actRoute: ActivatedRoute) {

      //alert (this.actRoute.snapshot.params.catid);
      this.audit_type_name = 'Saved Recycling Assessments';

      this.category_id = this.actRoute.snapshot.params.catid;

      if (isNaN(this.category_id)) {
        this.category_id = 9;
      } else {

        this.audit_type_name = 'EPR Assessments';
      }


       this.dataSource = service.getDataSource(this.category_id);
        console.log('calling service.getCategoryTypes');
        this.categoriesData = service.getCategoryTypes();
    }

    navigateTo  (url: string,id: any){

      this.router.navigate([url,id]);
    }

}
