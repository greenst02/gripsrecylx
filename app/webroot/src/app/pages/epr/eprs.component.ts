import { Component } from '@angular/core';
import { DxDataGridModule,
  DxBulletModule,
  DxTemplateModule,DxSelectBoxModule, DxButtonModule,
DxVectorMapModule,DxPieChartModule } from 'devextreme-angular';
import DataSource from 'devextreme/data/data_source';
import { AuditService } from '../../shared/services/audit.service';
import {Router, ActivatedRoute} from '@angular/router';
import * as mapsData from 'devextreme/dist/js/vectormap-data/world.js';
import { Marker, MapService } from '../../shared/services/map.service';

import { DecimalPipe } from '@angular/common';
 
import { Injectable } from '@angular/core';
import { T } from 'node_modulesxxx/@angular/cdk/keycodes';

export class GdpInfo {
  name: string;

  value: number;
}



@Component({
  templateUrl: 'eprs.component.html',
  styleUrls: [ './eprs.component.scss'],
  providers: [AuditService,MapService]
})

export class EPRSComponent {


  worldMap: any = mapsData.world;

  markers: Marker[];

  gdpData: Object;

  toolTipData: Object;

  pipe: any = new DecimalPipe('en-US');


  customizeLayers(elements) {
    elements.forEach((element) => {
      const countryGDPData = this.gdpData[element.attribute('name')];
      element.attribute('total', countryGDPData && countryGDPData.total || 0);
    });
  }

  customizeText = (arg) => `${this.pipe.transform(arg.start, '1.0-0')} to ${this.pipe.transform(arg.end, '1.0-0')}`;

  customizePieLabel(info) {
    return `${info.argument[0].toUpperCase()
        + info.argument.slice(1)
    }: $${info.value}M`;
  }

  getPieData(name): GdpInfo[] {
    return this.gdpData[name] ? [
      { name: 'industry', value: this.gdpData[name].industry },
      { name: 'services', value: this.gdpData[name].services },
      { name: 'agriculture', value: this.gdpData[name].agriculture },
    ] : null;
  }




    audit_type_name: string;
    category_id: number;
    dataSource: any;
    categoriesData: any;
    collapsed = false;  
    refreshModes: string[];
    refreshMode: string;
    contentReady = (e) => {
        if(!this.collapsed) {
            this.collapsed = true;
          //  e.component.expandRow(["EnviroCare"]);
        }
    };

    rowClicked = (e) => {
        console.log("============row clicked");
        console.log(e);

        this.navigateTo('/audit',e.data.id);
    };

    cellDblClicked = (e) => {
      console.log("============cell double clicked");
      console.log(e);

      this.navigateTo('/audit',e.row.data.id);
  };

  markerClick(e) {
    if (e.target && e.target.layer.type === 'marker') {
     // e.component.center(e.target.coordinates()).zoomFactor(10);
  //    console.log(e.target);

      this.navigateTo('/audit',e.target.attribute('audit_id')); 
//     alert('Marker::' + e.target.attribute('audit_id'));

    }
  }



    constructor(service: AuditService, mapService: MapService, private router: Router, private actRoute: ActivatedRoute) {

      //alert (this.actRoute.snapshot.params.catid);
      this.audit_type_name = 'EPR Knowledge Base';

      this.category_id = this.actRoute.snapshot.params.catid;

      if (isNaN(this.category_id)) {
        this.category_id = 9;
      } else {

        this.audit_type_name = 'EPR Knowledge Base';
      }


       this.dataSource = service.getDataSource(this.category_id);
        console.log('calling service.getCategoryTypes');
        this.categoriesData = service.getCategoryTypes();

//        this.gdpData = service.getCountriesGDP();
        this.gdpData = [];
        this.customizeLayers = this.customizeLayers.bind(this);
        this.markers = mapService.getMarkers();

        console.log('================World Map============');
        console.log(this.worldMap);

    }

    navigateTo  (url: string,id: any){

      this.router.navigate([url,id]);
    }

}
