
<!--[if (gte mso 9)|(IE)]>
  <table width="425" align="left" cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td>
<![endif]-->
<table class="col425" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 450px;">  
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					 <td class="bodycopy" style="font-size: 12px;font-family: Myriad,Helvetica,Tahoma,Arial; line-height: 22px;">
					               <h3 style="color:#000000;">
                    <a href="<?php echo $url; ?>">Australian Packaging Covenant - Information Request</a>
                </h3>
				</td>
				</tr>
	
				<tr>
                    <td class="bodycopy" style="font-size: 12px;font-family: Myriad,Helvetica,Tahoma,Arial; line-height: 22px;">
						<?php echo $message; ?>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 20px 0 0 0;">
                        <table class="buttonwrapper" bgcolor="#7f9293" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="button" style="text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;" height="45">
                                    <a style="color: #ffffff; text-decoration: none;" href="<?php echo $url; ?>">Create Password</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--[if (gte mso 9)|(IE)]>
      </td>
    </tr>
</table>
<![endif]-->
