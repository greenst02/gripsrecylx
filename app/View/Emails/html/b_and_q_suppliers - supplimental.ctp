<?php  


//create a "block" that is output by the layout.  The tracking code is used create call back to website
//when the user opens the email.
	$this->startIfEmpty('gripsHeaderImage');
	echo "<img src=\"http://dataservice.greenstreets.ie/batch_emails/opened/".$tracking_code."/greenstreets-ds-header.png\" style=\"max-width: 600px; height: auto; line-height: 100%; outline: none; text-decoration: none; border: 0;\" id=\"headerImage campaign-icon\"/>";
	$this->end();

//create a "block" to output the vendor code in layout.
	$this->startIfEmpty('gripsVendorCode');
		echo $vendor_code;
	$this->end();

//create a "block" to output the tracking code in layout.
	$this->startIfEmpty('gripsTrackingCode');
		echo $tracking_code;
	$this->end();
?>

<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
	<td valign="top" style="border-collapse: collapse;">&#13;
			<div mc:edit="std_content00" style="color: #505050; font-family: Arial; font-size: 12px; line-height: 150%; text-align: left;" align="left">&#13;
				&#13;
				Dear B&amp;Q Supplier&#13;
				&#13;
				<p>Please see below the original email that was sent to B&amp;Q Suppliers on 14th of April. We have been provided with a supplemental list of suppliers which included your company.</p>
				<p>Could you please send on the information as requested below for our new deadline of <strong>30th June</strong>. 
				<p>Thank you in advance for your assistance.</p>

				
				<p><hr /></p>
				<p>Original Email Request</p>
				<p><hr /><p>
				<p>Please find attached a letter from Ms Mandy Winser, Head of Omni Logistics &amp; CSR at B&amp;Q, advising that Greenstreets will be assisting the company in collecting&#13;
				and collating packaging information.&#13;
				<br />We would be grateful if you could complete the attached Packaging Weights Template and email the information to bandq@greenstreets.ie on or before 30th April 2015. </p>&#13;
&#13;
				<p><strong><u>New Template for 2015</u></strong><br />We are using a New Template so:&#13;
				</p><ul style="list-style: square outside none;"><li>Please ensure you use this format and do not alternate the template layout in any way to facilitate uploading to B&amp;Q database.</li>&#13;
				<li>Please also note we require some additional information regarding timber/paper used in your packaging.</li></ul><p><strong><u>Need Help?</u></strong><br />&#13;
				Please see the Guidance Document attached or contact the Data Services Team (Keith Burton or Iga Sitna) at Greenstreets on +353-1-4100618 or e-mail at <a href="mailto:bandq@greenstreets.ie" style="color: #336699; font-weight: normal; text-decoration: underline;">bandq@greenstreets.ie</a>. &#13;
				</p><p>Please include your Vendor Number in all correspondence.&#13;
				<br />[Vendor Code: <?php echo $vendor_code; ?>]<br />[Request #:<?php echo $tracking_code;?>]</p>&#13;
				<p><br /><br />Kind Regards,<br /><br />Iga Sitna</p>&#13;
			</div>&#13;
		</td>&#13;
		</tr>
</table>

