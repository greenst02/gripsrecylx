<?php  


//create a "block" that is output by the layout.  The tracking code is used create call back to website
//when the user opens the email.
	$this->startIfEmpty('gripsHeaderImage');
	echo "<img src=\"http://dataservice.greenstreets.ie/batch_emails/opened/".$tracking_code."/greenstreets-musgrave-header.png\" style=\"max-width: 600px; height: auto; line-height: 100%; outline: none; text-decoration: none; border: 0;\" id=\"headerImage campaign-icon\"/>";
	$this->end();

//create a "block" to output the vendor code in layout.
	$this->startIfEmpty('gripsVendorCode');
		echo $vendor_code;
	$this->end();

//create a "block" to output the tracking code in layout.
	$this->startIfEmpty('gripsTrackingCode');
		echo $tracking_code;
	$this->end();
?>

<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
	<td valign="top" style="border-collapse: collapse;">&#13;
			<div mc:edit="std_content00" style="color: #505050; font-family: Arial; font-size: 12px; line-height: 150%; text-align: left;" align="left">&#13;
				&#13;
				Dear Musgrave Supplier&#13;
				&#13;

			<p>This is a reminder of email below.  If you have not already done so, could you please ensure you respond by 25th September.</p>
			<hr/>



			<p>Greenstreets has been appointed by Musgrave Wholesale Partners (MWP) to implement a system to manage their obligations with respect to the Packaging Regulations in Ireland.</p> 
			<p>We have been advised by Musgrave Wholesale Partners that you supply packaging and/or packaged products to them in the course of business.  Therefore we wish to request that you provide information on the specifications (type and weight) of packaging supplied to Musgrave.</p>
			<p>We would be grateful if you could complete the attached Packaging Weights Template with the packaging specification data for all products listed.  Please email the information to musgraves@greenstreets.ie on or before <strong>25th September</strong>. </p>
			<p>Should you wish to supply data in your own format then we are happy to accept it, provided that it is in .csv or .xls format. Please do not alter the format of the template provided.</p>
			<p>If you have any questions about this request and / or require assistance in completing the Packaging Weights Template, please do not hesitate to contact Iga Sitna in the Data Services Team (Iga Sitna) at Greenstreets on +353-1-4100618 or e-mail at musgraves@greenstreets.ie.</p>
			
			<br />[Supplier Code: <?php echo $vendor_code; ?>]<br />[Request #:<?php echo $tracking_code;?>]</p>&#13;
				<p><br /><br />Kind Regards,<br /><br />Iga Sitna</p>&#13;
			</div>&#13;
		</td>&#13;
		</tr>
</table>

