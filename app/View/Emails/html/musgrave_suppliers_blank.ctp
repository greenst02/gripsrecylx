<?php  


//create a "block" that is output by the layout.  The tracking code is used create call back to website
//when the user opens the email.
	$this->startIfEmpty('gripsHeaderImage');
	echo "<img src=\"http://dataservice.greenstreets.ie/batch_emails/opened/".$tracking_code."/greenstreets-musgrave-header.png\" style=\"max-width: 600px; height: auto; line-height: 100%; outline: none; text-decoration: none; border: 0;\" id=\"headerImage campaign-icon\"/>";
	$this->end();

//create a "block" to output the vendor code in layout.
	$this->startIfEmpty('gripsVendorCode');
		echo $vendor_code;
	$this->end();

//create a "block" to output the tracking code in layout.
	$this->startIfEmpty('gripsTrackingCode');
		echo $tracking_code;
	$this->end();

//create a "block" to output the subject in layout.
	$this->startIfEmpty('gripsSubject');
		echo $subject;
	$this->end();


//create a "block" to output the subject in layout.
	$this->startIfEmpty('gripsDueDate');
		echo $due_date;
	$this->end();

?>




<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
	<td valign="top" style="border-collapse: collapse;">&#13;
			<div mc:edit="std_content00" style="color: #505050; font-family: Arial; font-size: 12px; line-height: 150%; text-align: left;" align="left">
				&#13;
				&#13;


			<?php 
			//the message text passed through will be inserted here.
			echo $message; 
			?>			
			<br />[Supplier Code: <?php echo $vendor_code; ?>]<br />[Request #:<?php echo $tracking_code;?>]</p>&#13;
				<p><br /><br />Kind Regards,<br /><br />Iga Sitna</p>&#13;
			</div>&#13;
		</td>&#13;
		</tr>
</table>

