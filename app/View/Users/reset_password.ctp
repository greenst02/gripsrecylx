<?php

echo $this->Form->create('User', array(
    'type' => 'post',
    'action' => 'change_password')
);
echo $this->Form->inputs(
        array(
            'legend' => __('Reset Password'),
            'pwd' => array('label' => 'New Password'),
            'pwd_repeat' => array('label' => 'Repeat Password')
        )
);
echo $this->Form->hidden('id', array('value' => $user['id']));
echo $this->Form->hidden('tokenhash', array('value' => $user['tokenhash']));
echo $this->Form->end('Reset Password');
?>