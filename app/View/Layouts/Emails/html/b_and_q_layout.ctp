<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo $this->fetch('title');?></title>
  </head>
  <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="width: 100% !important; -webkit-text-size-adjust: none; background-color: #FAFAFA; margin: 0; padding: 0;" bgcolor="#FAFAFA"><style type="text/css">
.preheaderContent div a:visited { color: #336699 !important; font-weight: normal !important; text-decoration: underline !important; }
.headerContent a:visited { color: #336699 !important; font-weight: normal !important; text-decoration: underline !important; }
.bodyContent div a:visited { color: #336699 !important; font-weight: normal !important; text-decoration: underline !important; }
.footerContent div a:visited { color: #336699 !important; font-weight: normal !important; text-decoration: underline !important; }
&gt;</style>&#13;
    	<center>&#13;
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" style="height: 100% !important; width: 100% !important; background-color: #FAFAFA; margin: 0; padding: 0;" bgcolor="#FAFAFA"><tr><td align="center" valign="top" style="border-collapse: collapse;">&#13;
                        <!-- // Begin Template Preheader \\ -->&#13;
                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader" style="background-color: #FAFAFA;" bgcolor="#FAFAFA"><tr><td valign="top" class="preheaderContent" style="border-collapse: collapse;">&#13;
                                &#13;
                                	<!-- // Begin Module: Standard Preheader \ -->&#13;
                                    <table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td valign="top" style="border-collapse: collapse;">&#13;
                                            	<div mc:edit="std_preheader_content" style="color: #505050; font-family: Arial; font-size: 10px; line-height: 100%; text-align: left;" align="left">&#13;
                                                	 B&amp;Q Packaging Information Request<br />Due date 30th April 2015.&#13;
                                                </div>&#13;
                                            </td>&#13;
                                            <!-- *|IFNOT:ARCHIVE_PAGE|* -->&#13;
											<td valign="top" width="190" style="border-collapse: collapse;">&#13;
                                            	<div mc:edit="std_preheader_links" style="color: #505050; font-family: Arial; font-size: 10px; line-height: 100%; text-align: left;" align="left">&#13;
													[Vendor Code: <?php echo $this->fetch('gripsVendorCode');?>]
                                                    <br />[Request #:<?php echo $this->fetch('gripsTrackingCode');?>]
                                                    &#13;
                                                </div>&#13;
                                            </td>&#13;
											<!-- *|END:IF|* -->&#13;
                                        </tr></table><!-- // End Module: Standard Preheader \ --></td>&#13;
                            </tr></table><!-- // End Template Preheader \\ --><table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer" style="background-color: #FFFFFF; border: 1px solid #dddddd;" bgcolor="#FFFFFF"><tr><td align="center" valign="top" style="border-collapse: collapse;">&#13;
                                    <!-- // Begin Template Header \\ -->&#13;
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader" style="background-color: #FFFFFF; border-bottom-width: 0;" bgcolor="#FFFFFF"><tr><td class="headerContent" style="border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; text-align: center; vertical-align: middle; padding: 0;" align="center" valign="middle">&#13;
                                            &#13;
                                            	<!-- // Begin Module: Standard Header Image \\ -->&#13;
                                                <?php echo $this->fetch("gripsHeaderImage");?>
                                                <!-- // End Module: Standard Header Image \\ --></td>&#13;
                                        </tr></table><!-- // End Template Header \\ --></td>&#13;
                            </tr><tr><td align="center" valign="top" style="border-collapse: collapse;">&#13;
                                    <!-- // Begin Template Body \\ -->&#13;
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody"><tr><td valign="top" class="bodyContent" style="border-collapse: collapse; background-color: #FFFFFF;" bgcolor="#FFFFFF">&#13;
                                &#13;
                                                <!-- // Begin Module: Standard Content \\ -->&#13;
												
                                                <?php echo $this->fetch('content'); ?>

												<!-- // End Module: Standard Content \\ --></td>&#13;
                                        </tr></table><!-- // End Template Body \\ --></td>&#13;
                            </tr><tr><td align="center" valign="top" style="border-collapse: collapse;">&#13;
                                    <!-- // Begin Template Footer \\ -->&#13;
                                	<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter" style="background-color: #FFFFFF; border-top-width: 0;" bgcolor="#FFFFFF"><tr><td valign="top" class="footerContent" style="border-collapse: collapse;">&#13;
                                            &#13;
                                                <!-- // Begin Module: Standard Footer \\ -->&#13;
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td colspan="2" valign="middle" id="social" style="border-collapse: collapse; background-color: #FAFAFA; border: 0;" bgcolor="#FAFAFA">&#13;
                                                            <div mc:edit="std_social" style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;" align="center">&#13;
                                                                 You are receiving this email because B&amp;Q have indicated you are the vendor contact. &#13;
                                                            </div>&#13;
                                                        </td>&#13;
                                                    </tr><tr><td valign="top" id="signature" style="border-collapse: collapse;">                                                     &#13;
															<div style="color: #707070; font-family: Arial; font-size: 11px; line-height: 125%; text-align: left;" align="left"><p>&#13;
																<br />Greenstreets Data Service| Greenstreets Environmental Resources Ltd&#13;
																<br />Unit 25, Guinness Enterprise Centre, Taylors Lane, Dublin 8, Republic of Ireland&#13;
																<br />P + 353 (0)1 4100 618 | F + 353 (0)1 4100 996 | W www.greenstreets.ie</p>&#13;
																<p>Operating in Ireland, UK, Europe and Australia, Greenstreets is a leading environmental consulting firm, specialising in Waste Compliance. We are ISO 9001 accredited and proud to be associated with many of Europe’s top companies. </p>&#13;
															</div>&#13;
														</td>&#13;
													</tr><tr><td valign="top" id="disclaimer" style="border-collapse: collapse;">&#13;
                                                            <div mc:edit="std_footer" style="color: #707070; font-family: Arial; font-size: 10px; line-height: 125%; text-align: left;" align="left">&#13;
																<p>This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. &#13;
																   E-mail transmission cannot be guaranteed to be secure or error-free as information could be intercepted, corrupted, lost, destroyed, arrive late or incomplete, or contain viruses. The sender therefore does not accept liability for errors or omissions in this message.If verification is required please request a hard-copy version. This message is provided for informational purposes and should not be construed as a solicitation/offer.</p>&#13;
																<p><strong>Registered Office</strong>: Greenstreets Environmental Resources Ltd | Unit 25, Guinness Enterprise Centre, Taylors Lane, Dublin 8, Republic of Ireland &#13;
																 <br />Registered in Ireland Number: 323971</p>&#13;
                                                            </div>&#13;
                                                        </td>&#13;
                                                        &#13;
                                                    </tr><tr><td colspan="2" valign="middle" id="utility" style="border-collapse: collapse; background-color: #FFFFFF; border: 0;" bgcolor="#FFFFFF">&#13;
                                                            <div mc:edit="std_utility" style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;" align="center">&#13;
                                                                 <a href="" style="color: #336699; font-weight: normal; text-decoration: underline;">unsubscribe from this list</a> | <a href="*|UPDATE_PROFILE|*" style="color: #336699; font-weight: normal; text-decoration: underline;">update subscription preferences</a> &#13;
                                                            </div>&#13;
                                                        </td>&#13;
                                                    </tr></table><!-- // End Module: Standard Footer \\ --></td>&#13;
                                        </tr></table><!-- // End Template Footer \\ --></td>&#13;
                            </tr></table><br /></td>&#13;
                </tr></table></center>&#13;
    </body>
</html>
