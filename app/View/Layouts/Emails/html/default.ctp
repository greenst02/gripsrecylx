<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <?php echo $title_for_layout;?>
    </title>
  </head>
  <body yahoo="" bgcolor="#f6f8f1" style="min-width: 100% !important; margin: 0; padding: 0;">
<table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0"><tr><td>
    <!--[if (gte mso 9)|(IE)]>
      <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
    <![endif]-->     
    <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width: 600px;"><tr><td bgcolor="#b0cbde" class="header" style="padding: 40px 30px 20px;">
          <table width="70" align="left" border="0" cellpadding="0" cellspacing="0"><tr>
            <td height="70" style="padding: 0 20px 20px 0;">

            <img src="<?php echo $this->Html->url('/bin/assets/images/grips_logo_small.png', true); ?>" width="70" height="70" alt="" border="0" style="height: auto;" />
                
            </td>
            </tr></table><!--[if (gte mso 9)|(IE)]>
            <table width="425" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]--><table class="col425" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 425px;"><tr><td height="70">
                <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="subhead" style="font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px; padding: 0 0 0 3px;">
                      GRIPS Australia
                    </td>
                  </tr></table></td>
            </tr></table><!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]--></td>
      </tr><tr><td class="innerpadding borderbottom" style="border-bottom-width: 1px; border-bottom-color: #f2eeed; border-bottom-style: solid; padding: 30px;">
      <?php echo $this->fetch('content'); ?>
        </td>
      </tr><tr><td class="footer" bgcolor="#44525f" style="padding: 20px 30px 15px;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center" class="footercopy" style="font-family: sans-serif; font-size: 14px; color: #ffffff;">
                Greenstreets Consulting Australia<br /></td>
            </tr><tr><td align="center" style="padding: 20px 0 0;">
                <table border="0" cellspacing="0" cellpadding="0"><tr><td width="37" style="text-align: center; padding: 0 10px;" align="center">
                      <a href="http://www.facebook.com/GreenstreetsConsulting">
                        <img src="<?php echo $this->Html->url('/bin/assets/images/facebook.png', true); ?>" width="37" height="37" alt="Facebook" border="0" style="height: auto;" /></a>
                    </td>
                    <td width="37" style="text-align: center; padding: 0 10px;" align="center">
                      <a href="http://www.twitter.com/GreenstreetsAus">
                        <img src="<?php echo $this->Html->url('/bin/assets/images/twitter.png', true); ?>" width="37" height="37" alt="Twitter" border="0" style="height: auto;" /></a>
                    </td>
                  </tr></table></td>
            </tr></table></td>
      </tr></table><!--[if (gte mso 9)|(IE)]>
          </td>
        </tr>
    </table>
    <![endif]--></td>
  </tr></table></body>
</html>