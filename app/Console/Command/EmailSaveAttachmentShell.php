<?php

class EmailSaveAttachmentShell extends AppShell {
	public $uses = array('EmailMonitor');

    public function main() {
		$mailbox = new EmailMonitor();
 
		$this->out("starting...\r\n");
		$this->out($mailbox->saveValidatedAttachments());

		}
}

?>