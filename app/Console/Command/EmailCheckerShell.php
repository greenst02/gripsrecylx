<?php

class EmailCheckerShell extends AppShell {
	public $uses = array('EmailMonitor');

    public function main() {
		$mailbox = new EmailMonitor();
 
		$this->out("starting...\r\n");
//		$this->out($mailbox->check_bounces());
		$this->out($mailbox->check_emails());
		$this->out($mailbox->check_folders());
//		$this->out($mailbox->saveValidatedAttachments());

		}
}

?>