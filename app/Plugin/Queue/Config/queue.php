<?php

/**
 * This file configures default behavior for all workers
 *
 * To modify these parameters, copy this file into your own CakePHP APP/Config directory.
 *
 */
$config['Queue'] = array('sleeptime' => 10, 'gcprob' => 10, 'defaultworkertimeout' => 75, 'defaultworkerretries' => 4, 'workermaxruntime' => 60, 'cleanuptimeout' => 2000, 'exitwhennothingtodo' => false, 'pidfilepath' => TMP . 'queue' . DS, 'log' => true, 'notify' => 'tmp');