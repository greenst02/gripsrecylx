<?php
App::uses('QuestionManagerAppController', 'QuestionManager.Controller');
/**
 * Questions Controller
 *
 */
class QuestionCategoryTypesController extends QuestionManagerAppController {

/**
 * Scaffold
 *
 * @var mixed
 */
	public $scaffold;

	var $uses = array('AuditQuestionCategoryType','Company');
	
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->AuditQuestionCategoryType->recursive = 0;
		$this->set('questionCategoryTypes', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->AuditQuestionCategoryType->exists($id)) {
			throw new NotFoundException(__('Invalid audit question category type'));
		}
		$options = array('conditions' => array('AuditQuestionCategoryType.' . $this->AuditQuestionCategoryType->primaryKey => $id));
		$this->set('auditQuestionCategoryType', $this->AuditQuestionCategoryType->find('first', $options));

		$customCompanies = $this->Company->find('list');
		$this->set(compact('customCompanies'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->AuditQuestionCategoryType->create();
			if ($this->AuditQuestionCategoryType->save($this->request->data)) {
				$this->Session->setFlash(__('The audit question category type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The audit question category type could not be saved. Please, try again.'));
			}
		}
		$customCompanies = $this->Company->find('list');
		$this->set(compact('customCompanies'));

	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->AuditQuestionCategoryType->exists($id)) {
			throw new NotFoundException(__('Invalid audit question category type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AuditQuestionCategoryType->save($this->request->data)) {
				$this->Session->setFlash(__('The audit question category type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The audit question category type could not be saved. Please, try again.'));
			}
		} else {
			$customCompanies = $this->Company->find('list');
			$this->set(compact('customCompanies'));

		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->AuditQuestionCategoryType->id = $id;
		if (!$this->AuditQuestionCategoryType->exists()) {
			throw new NotFoundException(__('Invalid audit question category type'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AuditQuestionCategoryType->delete()) {
			$this->Session->setFlash(__('Audit question category type deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Audit question category type was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
