<?php
App::uses('QuestionManagerAppController', 'QuestionManager.Controller');
/**
 * Questions Controller
 *
 */
class QuestionCategoriesController extends QuestionManagerAppController {

/**
 * Scaffold
 *
 * @var mixed
 */
	public $scaffold;

	var $uses = array('AuditQuestionCategory');
	
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->AuditQuestionCategory->recursive = 0;
		$this->set('questionCategories', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->AuditQuestionCategory->exists($id)) {
			throw new NotFoundException(__('Invalid audit question category type'));
		}
		$options = array('conditions' => array('AuditQuestionCategory.' . $this->AuditQuestionCategory->primaryKey => $id));
		$this->set('auditQuestionCategory', $this->AuditQuestionCategory->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->AuditQuestionCategory->create();
			if ($this->AuditQuestionCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The audit question category type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The audit question category type could not be saved. Please, try again.'));
			}
		}
		$auditQuestionCategoryTypes = $this->AuditQuestionCategory->AuditQuestionCategoryType->find('list');
		$this->set(compact('auditQuestionCategoryTypes'));

	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->AuditQuestionCategory->exists($id)) {
			throw new NotFoundException(__('Invalid audit question category type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AuditQuestionCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The audit question category type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The audit question category type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AuditQuestionCategory.' . $this->AuditQuestionCategory->primaryKey => $id));
			$this->request->data = $this->AuditQuestionCategory->find('first', $options);
		}
		$auditQuestionCategoryTypes = $this->AuditQuestionCategory->AuditQuestionCategoryType->find('list');
		$this->set(compact('auditQuestionCategoryTypes'));

}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->AuditQuestionCategory->id = $id;
		if (!$this->AuditQuestionCategory->exists()) {
			throw new NotFoundException(__('Invalid audit question category type'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AuditQuestionCategory->delete()) {
			$this->Session->setFlash(__('Audit question category type deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Audit question category type was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}
