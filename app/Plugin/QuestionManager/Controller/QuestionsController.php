<?php
App::uses('QuestionManagerAppController', 'QuestionManager.Controller');
/**
 * Questions Controller
 *
 */
class QuestionsController extends QuestionManagerAppController {

/**
 * Scaffold
 *
 * @var mixed
 */
	public $scaffold;

	var $uses = array('AuditQuestion');
	
	public $components = array('Paginator');

	
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
//		$this->AuditQuestion->recursive = 0;
//		$this->set('questions', $this->paginate());
//		$auditQuestionCategories = $this->AuditQuestion->AuditQuestionCategory->find('list');
//		$auditQuestionCategoryTypes = $this->AuditQuestion->AuditQuestionCategory->AuditQuestionCategoryType->find('list');
//		$this->set(compact('auditQuestionCategories','auditQuestionCategoryTypes'));


		$conditions = array();
		//Transform POST into GET
		if(($this->request->is('post') || $this->request->is('put')) && isset($this->data['Filter'])){
			$filter_url['controller'] = $this->request->params['controller'];
			$filter_url['action'] = $this->request->params['action'];
			// We need to overwrite the page every time we change the parameters
			$filter_url['page'] = 1;

			// for each filter we will add a GET parameter for the generated url
			foreach($this->data['Filter'] as $name => $value){
				if($value){
					// You might want to sanitize the $value here
					// or even do a urlencode to be sure
					$filter_url[$name] = urlencode($value);
				}
			}	
			// now that we have generated an url with GET parameters, 
			// we'll redirect to that page
			return $this->redirect($filter_url);
		} else {
			// Inspect all the named parameters to apply the filters
			foreach($this->params['named'] as $param_name => $value){
				// Don't apply the default named parameters used for pagination
				if(!in_array($param_name, array('page','sort','direction','limit'))){
					// You may use a switch here to make special filters
					// like "between dates", "greater than", etc
					if($param_name == "search"){
						//$conditions['OR'] = array(
						$conditions =
							array('AuditQuestionCategory.AuditQuestionCategoryType.name LIKE' => '%' . $value . '%'//)
							//,
    						//array('Movie.description LIKE' => '%' . $value . '%')
						);
					} else {
						$conditions['AuditQuestion.'.$param_name] = $value;
					}					
					$this->request->data['Filter'][$param_name] = $value;
				}
			}
		}
		$this->AuditQuestion->recursive = 0;
		$this->paginate = array(
			'limit' => 20,
			'conditions' => $conditions
		);
		$this->set('questions', $this->paginate());

		// get the possible values for the filters and 
		// pass them to the view
		$auditQuestionCategories = $this->AuditQuestion->AuditQuestionCategory->find('list');
		$auditQuestionCategoryTypes = $this->AuditQuestion->AuditQuestionCategory->AuditQuestionCategoryType->find('list');
		$this->set(compact('auditQuestionCategories','auditQuestionCategoryTypes'));

		// Pass the search parameter to highlight the text
		$this->set('search', isset($this->params['named']['search']) ? $this->params['named']['search'] : "");



	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->AuditQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid audit question'));
		}
		$options = array('conditions' => array('AuditQuestion.' . $this->AuditQuestion->primaryKey => $id));
		$this->set('question', $this->AuditQuestion->find('first', $options));
		$auditQuestionCategories = $this->AuditQuestion->AuditQuestionCategory->find('list');
		$auditQuestionCategoryTypes = $this->AuditQuestion->AuditQuestionCategory->AuditQuestionCategoryType->find('list');
		$this->set(compact('auditQuestionCategories','auditQuestionCategoryTypes'));

	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->AuditQuestion->create();
			if ($this->AuditQuestion->save($this->request->data)) {
				$this->Session->setFlash(__('The audit question has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The audit question could not be saved. Please, try again.'));
			}
		}
		$auditQuestionCategories = $this->AuditQuestion->AuditQuestionCategory->find('list');
		$auditQuestionCategoryTypes = $this->AuditQuestion->AuditQuestionCategory->AuditQuestionCategoryType->find('list');
		$this->set(compact('auditQuestionCategories','auditQuestionCategoryTypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->AuditQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid audit question'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AuditQuestion->save($this->request->data)) {
				$this->Session->setFlash(__('The audit question has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The audit question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AuditQuestion.' . $this->AuditQuestion->primaryKey => $id));
			$this->request->data = $this->AuditQuestion->find('first', $options);
		}
		
		
		$auditQuestionCategories = $this->AuditQuestion->AuditQuestionCategory->find('list');
		$auditQuestionCategoryTypes = $this->AuditQuestion->AuditQuestionCategory->AuditQuestionCategoryType->find('list');
		$this->set(compact('auditQuestionCategories','auditQuestionCategoryTypes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->AuditQuestion->id = $id;
		if (!$this->AuditQuestion->exists()) {
			throw new NotFoundException(__('Invalid audit question'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AuditQuestion->delete()) {
			$this->Session->setFlash(__('Audit question deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Audit question was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	
}
