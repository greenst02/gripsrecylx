<div class="questionCategoryTypes form">
<?php echo $this->Form->create('AuditQuestionCategoryType'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Question Category Type'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('custom_company_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'question_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'question_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
