<div class="questionCategoryTypes view">
<h2><?php  echo __('Question Category Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($auditQuestionCategoryType['AuditQuestionCategoryType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($auditQuestionCategoryType['AuditQuestionCategoryType']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($auditQuestionCategoryType['AuditQuestionCategoryType']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($auditQuestionCategoryType['AuditQuestionCategoryType']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Type'), array('action' => 'edit', $auditQuestionCategoryType['AuditQuestionCategoryType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Type'), array('action' => 'delete', $auditQuestionCategoryType['AuditQuestionCategoryType']['id']), null, __('Are you sure you want to delete # %s?', $auditQuestionCategoryType['AuditQuestionCategoryType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'question_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'question_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Audit Questions'); ?></h3>
	<?php if (!empty($auditQuestionCategoryType['AuditQuestion'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Reference'); ?></th>
		<th><?php echo __('Question'); ?></th>
		<th><?php echo __('Category'); ?></th>
		<th><?php echo __('Supplier'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($auditQuestionCategoryType['AuditQuestion'] as $auditQuestion): ?>
		<tr>
			<td><?php echo $auditQuestion['id']; ?></td>
			<td><?php echo $auditQuestion['reference']; ?></td>
			<td><?php echo $auditQuestion['question']; ?></td>
			<td><?php echo $auditQuestion['audit_question_category_id']; ?></td>
			<td><?php echo ($auditQuestion['supplier'] ? "Yes" : "No"); ?></td>
			<td><?php echo $auditQuestion['audit_question_category_type_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'questions', 'action' => 'view', $auditQuestion['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $auditQuestion['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions', 'action' => 'delete', $auditQuestion['id']), null, __('Are you sure you want to delete # %s?', $auditQuestion['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Categories'); ?></h3>
	<?php if (!empty($auditQuestionCategoryType['AuditQuestionCategory'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sort Order'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Open'); ?></th>
		<th><?php echo __('Supplier'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($auditQuestionCategoryType['AuditQuestionCategory'] as $auditQuestionCategory): ?>
		<tr>
			<td><?php echo $auditQuestionCategory['id']; ?></td>
			<td><?php echo $auditQuestionCategory['sort_order']; ?></td>
			<td><?php echo $auditQuestionCategory['name']; ?></td>
			<td><?php echo ($auditQuestionCategory['open'] ? "Yes" : "No"); ?></td>
			<td><?php echo ($auditQuestionCategory['supplier'] ? "Yes" : "No"); ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'question_categories', 'action' => 'view', $auditQuestionCategory['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'question_categories', 'action' => 'edit', $auditQuestionCategory['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'question_categories', 'action' => 'delete', $auditQuestionCategory['id']), null, __('Are you sure you want to delete # %s?', $auditQuestionCategory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'question_categories', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
