<div class="questions view">
<h2><?php  echo __('Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($question['AuditQuestion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reference'); ?></dt>
		<dd>
			<?php echo h($question['AuditQuestion']['reference']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo h($question['AuditQuestion']['question']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($question['AuditQuestionCategory']['name'], array('controller' => 'question_categories', 'action' => 'view', $question['AuditQuestionCategory']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($question['AuditQuestion']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($question['AuditQuestion']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Default Response'); ?></dt>
		<dd>
			<?php echo h($question['AuditQuestion']['default_response']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Supplier'); ?></dt>
		<dd>
			<?php echo h(($question['AuditQuestion']['supplier'] ? "Yes" : "No")); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Audit Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($question['AuditQuestionCategoryType']['name'], array('controller' => 'question_category_types', 'action' => 'view', $question['AuditQuestionCategoryType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('In Use?'); ?></dt>
		<dd>
			<?php echo h(!empty($question['AuditToQuestion']) ? "Yes" : "No"); ?>
			&nbsp;
		</dd>

	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Question'), array('action' => 'edit', $question['AuditQuestion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Question'), array('action' => 'delete', $question['AuditQuestion']['id']), null, __('Are you sure you want to delete # %s?', $question['AuditQuestion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'audit_question_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'audit_question_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
