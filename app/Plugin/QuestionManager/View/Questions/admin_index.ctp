<div class="questions index">
	<h2><?php echo __('Questions'); ?></h2>

	<div class="filters">
		Filters
		<?php
		// The base url is the url where we'll pass the filter parameters
		$base_url = array('controller' => 'questions', 'action' => 'index');
		echo $this->Form->create("Filter",array('url' => $base_url, 'class' => 'filter'));
		// add a select input for each filter. It's a good idea to add a empty value and set
		// the default option to that.
	echo '<table cellpadding="0" cellspacing="0">
	<tr>
		<td>';
	echo $this->Form->input("audit_question_category_id", array('label' => 'Category', 'options' => $auditQuestionCategories, 'empty' => '-- All Categories --', 'default' => ''));
	echo'</td>
		<td>';
		echo $this->Form->input("audit_question_category_type_id", array('label' => 'Audit Types', 'options' => $auditQuestionCategoryTypes, 'empty' => '-- All Types --', 'default' => ''));
	echo'</td>';
		// Add a basic search 
//		echo $this->Form->input("search", array('label' => 'Search', 'placeholder' => "Search..."));
	echo '</tr>';


		// To reset all the filters we only need to redirect to the base_url
	echo '<tr><td>';
//		echo "<div class='submit actions'>";
		echo $this->Form->submit("Search");
	echo '</td><td>';
		echo $this->Html->link("Reset",$base_url);
//		echo "</div>";
		echo $this->Form->end();
	echo '</td></tr></table>';

		?>
	</div>



	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('reference'); ?></th>
			<th><?php echo $this->Paginator->sort('question'); ?></th>
			<th><?php echo $this->Paginator->sort('supplier'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php 
		$currCategory=0;
		foreach ($questions as $auditQuestion): 
			if($currCategory != $auditQuestion['AuditQuestionCategory']['id']){
			
				echo '<tr><td colspan="9">';
				echo $this->Html->link($auditQuestion['AuditQuestionCategory']['name'], array('controller' => 'question_categories', 'action' => 'view', $auditQuestion['AuditQuestionCategory']['id']));
				echo '</td></tr>';
					
			}
	?>

	<tr>
		<td><?php echo h($auditQuestion['AuditQuestion']['id']); ?>&nbsp;</td>
		<td><?php echo h($auditQuestion['AuditQuestion']['reference']); ?>&nbsp;</td>
		<td><?php echo h($auditQuestion['AuditQuestion']['question']); ?>&nbsp;</td>
		<td><?php echo h(($auditQuestion['AuditQuestion']['supplier'] ? "Yes":"No")); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $auditQuestion['AuditQuestion']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $auditQuestion['AuditQuestion']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $auditQuestion['AuditQuestion']['id']), null, __('Are you sure you want to delete # %s?', $auditQuestion['AuditQuestion']['id'])); ?>
		</td>
	</tr>
<?php $currCategory = $auditQuestion['AuditQuestionCategory']['id'];
	endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Question'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Question Categories'), array('controller' => 'question_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question Category'), array('controller' => 'question_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
