<div class="questions form">
<?php echo $this->Form->create('AuditQuestion'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Question'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('reference');
		echo $this->Form->input('question');
		echo $this->Form->input('audit_question_category_id');
		echo $this->Form->input('default_response');
		echo $this->Form->input('supplier');
		echo $this->Form->input('audit_question_category_type_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AuditQuestion.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('AuditQuestion.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'question_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'question_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
