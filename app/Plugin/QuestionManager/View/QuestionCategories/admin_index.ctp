<div class="questionCategories index">
	<h2><?php echo __('Question Categories'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('sort_order'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('audit_question_category_type_id'); ?></th>
			<th><?php echo $this->Paginator->sort('open'); ?></th>
			<th><?php echo $this->Paginator->sort('supplier'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($questionCategories as $auditQuestionCategory): ?>
	<tr>
		<td><?php echo h($auditQuestionCategory['AuditQuestionCategory']['id']); ?>&nbsp;</td>
		<td><?php echo h($auditQuestionCategory['AuditQuestionCategory']['sort_order']); ?>&nbsp;</td>
		<td><?php echo h($auditQuestionCategory['AuditQuestionCategory']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($auditQuestionCategory['AuditQuestionCategoryType']['name'], array('controller' => 'audit_question_category_types', 'action' => 'view', $auditQuestionCategory['AuditQuestionCategoryType']['id'])); ?>
		</td>
		<td><?php echo h(($auditQuestionCategory['AuditQuestionCategory']['open'] ? "Yes" : "No")); ?>&nbsp;</td>
		<td><?php echo h(($auditQuestionCategory['AuditQuestionCategory']['supplier'] ? "Yes" : "No")); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $auditQuestionCategory['AuditQuestionCategory']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $auditQuestionCategory['AuditQuestionCategory']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $auditQuestionCategory['AuditQuestionCategory']['id']), null, __('Are you sure you want to delete # %s?', $auditQuestionCategory['AuditQuestionCategory']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Category'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'question_category_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'question_category_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
