<div class="questionCategories view">
<h2><?php  echo __('Question Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($auditQuestionCategory['AuditQuestionCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sort Order'); ?></dt>
		<dd>
			<?php echo h($auditQuestionCategory['AuditQuestionCategory']['sort_order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($auditQuestionCategory['AuditQuestionCategory']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($auditQuestionCategory['AuditQuestionCategory']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($auditQuestionCategory['AuditQuestionCategory']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($auditQuestionCategory['AuditQuestionCategoryType']['name'], array('controller' => 'question_category_types', 'action' => 'view', $auditQuestionCategory['AuditQuestionCategoryType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Open'); ?></dt>
		<dd>
			<?php echo h(($auditQuestionCategory['AuditQuestionCategory']['open'] ? "Yes" : "No"));?>
			&nbsp;
		</dd>
		<dt><?php echo __('Supplier'); ?></dt>
		<dd>
			<?php echo h(($auditQuestionCategory['AuditQuestionCategory']['supplier'] ? "Yes" : "No")); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Category'), array('action' => 'edit', $auditQuestionCategory['AuditQuestionCategory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Category'), array('action' => 'delete', $auditQuestionCategory['AuditQuestionCategory']['id']), null, __('Are you sure you want to delete # %s?', $auditQuestionCategory['AuditQuestionCategory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'question_category_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'question_category_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Questions'); ?></h3>
	<?php if (!empty($auditQuestionCategory['AuditQuestion'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Ref.'); ?></th>
		<th><?php echo __('Question'); ?></th>
		<th><?php echo __('Supplier'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($auditQuestionCategory['AuditQuestion'] as $auditQuestion): ?>
		<tr>
			<td><?php echo $auditQuestion['id']; ?></td>
			<td><?php echo $auditQuestion['reference']; ?></td>
			<td><?php echo $auditQuestion['question']; ?></td>
			<td><?php echo ($auditQuestion['supplier'] ? "Yes" : "No"); ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'questions', 'action' => 'view', $auditQuestion['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $auditQuestion['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions', 'action' => 'delete', $auditQuestion['id']), null, __('Are you sure you want to delete # %s?', $auditQuestion['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Audit Question'), array('controller' => 'audit_questions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
