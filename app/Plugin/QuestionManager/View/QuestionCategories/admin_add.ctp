<div class="questionCategories form">
<?php echo $this->Form->create('AuditQuestionCategory'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Question Category'); ?></legend>
	<?php
		echo $this->Form->input('sort_order');
		echo $this->Form->input('name');
		echo $this->Form->input('audit_question_category_type_id');
		echo $this->Form->input('open');
		echo $this->Form->input('supplier');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Categories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'question_category_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'question_category_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
