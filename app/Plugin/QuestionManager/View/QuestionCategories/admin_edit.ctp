<div class="questionCategories form">
<?php echo $this->Form->create('AuditQuestionCategory'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Question Category'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('sort_order');
		echo $this->Form->input('name');
		echo $this->Form->input('audit_question_category_type_id');
		echo $this->Form->input('open');
		echo $this->Form->input('supplier');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AuditQuestionCategory.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('AuditQuestionCategory.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Categories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'question_category_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'question_category_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
