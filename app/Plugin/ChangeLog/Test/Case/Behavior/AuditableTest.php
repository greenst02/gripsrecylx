<?php

App::uses( 'Model', 'Model' );
App::uses( 'AppModel', 'Model' );

/**
 * Article class
 *
 * @package       cake
 * @subpackage    cake.tests.cases.libs.model
 */
class Article extends CakeTestModel {
  public $name = 'Article';
  public $actsAs = array(
    'ChangeLog.Changeable' => array(
      'ignore' => array( 'ignored_field' ),
    )
  );
  public $belongsTo = array( 'Author' );
}

/**
 * Author class
 *
 * @package       cake
 * @subpackage    cake.tests.cases.libs.model
 */
class Author extends CakeTestModel {
  public $name = 'Author';
  public $actsAs = array(
    'ChangeLog.Changeable'
  );
  public $hasMany = array( 'Article' );
}

class Change extends CakeTestModel {
  public $hasMany = array(
    'ChangeDelta'
  );
}

class ChangeDelta extends CakeTestModel {
  public $belongsTo = array(
    'Change'
  );
}

/**
 * ChangeableBehavior test class.
 */
class ChangeableBehaviorTest extends CakeTestCase {
  /**
   * Fixtures associated with this test case
   *
   * @var array
   * @access public
   */
  public $fixtures = array(
    'plugin.change_log.article',
    'plugin.change_log.author',
    'plugin.change_log.change',
    'plugin.change_log.change_delta',
  );
  
  /**
   * Method executed before each test
   *
   * @access public
   */
  public function setUp() {
    $this->Article = ClassRegistry::init( 'Article' );
  }
  
  /**
   * Method executed after each test
   *
   * @access public
   */
  public function tearDown() {
    unset( $this->Article );

    ClassRegistry::flush();
  }
  
  /**
   * Test the action of creating a new record.
   *
   * @todo  Test HABTM save
   */
  public function testCreate() {
    $new_article = array(
      'Article' => array(
        'user_id'   => 1,
        'author_id' => 1,
        'title'     => 'First Test Article', 
        'body'      => 'First Test Article Body', 
        'published' => 'N', 
      ),
    );
    
    $this->Article->save( $new_article );
    $change = ClassRegistry::init( 'Change' )->find(
      'first',
      array(
        'recursive'  => -1,
        'conditions'        => array(
          'Change.event'     => 'CREATE',
          'Change.model'     => 'Article',
          'Change.entity_id' => $this->Article->getLastInsertId()
        )
      )
    );
    $article = json_decode( $change['Change']['json_object'], true );
    
    $deltas = ClassRegistry::init( 'ChangeDelta' )->find(
      'all',
      array(
        'recursive' => -1,
        'conditions' => array( 'ChangeDelta.change_id' => $change['Change']['id'] ),
      )
    );
    
    # Verify the change record
    $this->assertEqual( 1, $article['Article']['user_id'] );
    $this->assertEqual( 'First Test Article', $article['Article']['title'] );
    $this->assertEqual( 'N', $article['Article']['published'] );
    
    #Verify that no delta record was created.
    $this->assertTrue( empty( $deltas ) );
  }

  /** 
   * Test saving multiple records with Model::saveAll()
   */
  public function testSaveAll() {
    # TEST A MODEL AND A SINGLE ASSOCIATED MODEL
    $data = array(
      'Article' => array(
        'user_id'   => 1,
        'title'     => 'Rob\'s Test Article', 
        'body'      => 'Rob\'s Test Article Body', 
        'published' => 'Y', 
      ),
      'Author' => array(
        'first_name' => 'Rob',
        'last_name' => 'Wilkerson',
      ),
    );

    $this->Article->saveAll( $data );
    $article_change = ClassRegistry::init( 'Change' )->find(
      'first',
      array(
        'recursive'  => -1,
        'conditions'        => array(
          'Change.event'     => 'CREATE',
          'Change.model'     => 'Article',
          'Change.entity_id' => $this->Article->getLastInsertId()
        )
      )
    );
    $article = json_decode( $article_change['Change']['json_object'], true );

    # Verify the change record
    $this->assertEqual( 1, $article['Article']['user_id'] );
    $this->assertEqual( 'Rob\'s Test Article', $article['Article']['title'] );
    $this->assertEqual( 'Y', $article['Article']['published'] );
    
    # Verify that no delta record was created.
    $this->assertTrue( !isset( $article_change['ChangeDelta'] ) );

    $author_change = ClassRegistry::init( 'Change' )->find(
      'first',
      array(
        'recursive'  => -1,
        'conditions'        => array(
          'Change.event'     => 'CREATE',
          'Change.model'     => 'Author',
          'Change.entity_id' => $this->Article->Author->getLastInsertId()
        )
      )
    );
    $author = json_decode( $author_change['Change']['json_object'], true );

    # Verify the change record
    $this->assertEqual( $article['Article']['author_id'], $author['Author']['id'] );
    $this->assertEqual( 'Rob', $author['Author']['first_name'] );
    
    # Verify that no delta record was created.
    $this->assertTrue( !isset( $author_change['ChangeDelta'] ) );

    # TEST MULTIPLE RECORDS OF ONE MODEL

    $data = array(
        array(
          'Article' => array(
            'user_id'   => 1,
            'author_id' => 1,
            'title'     => 'Multiple Save 1 Title', 
            'body'      => 'Multiple Save 1 Body', 
            'published' => 'Y', 
          ),
        ),
        array(
          'Article' => array(
            'user_id'       => 2,
            'author_id'     => 2,
            'title'         => 'Multiple Save 2 Title', 
            'body'          => 'Multiple Save 2 Body', 
            'published'     => 'N', 
            'ignored_field' => 1,
          )
        ),        
        array(
          'Article' => array(
            'user_id'   => 3,
            'author_id' => 3,
            'title'     => 'Multiple Save 3 Title', 
            'body'      => 'Multiple Save 3 Body', 
            'published' => 'Y', 
          )
        ),
    );
    $this->Article->create();
    $this->Article->saveAll( $data ); 

    # Retrieve the changes for the last 3 articles saved
    $changes = ClassRegistry::init( 'Change' )->find(
      'all',
      array(
        'conditions'        => array(
          'Change.event'     => 'CREATE',
          'Change.model'     => 'Article',
        ),
        'order' => array( 'Change.entity_id DESC' ),
        'limit' => 3
      )
    );

    $article_1 = json_decode( $changes[2]['Change']['json_object'], true );
    $article_2 = json_decode( $changes[1]['Change']['json_object'], true );
    $article_3 = json_decode( $changes[0]['Change']['json_object'], true );

    # Verify the change records
    $this->assertEqual( 1, $article_1['Article']['user_id'] );
    $this->assertEqual( 'Multiple Save 1 Title', $article_1['Article']['title'] );
    $this->assertEqual( 'Y', $article_1['Article']['published'] );
    
    $this->assertEqual( 2, $article_2['Article']['user_id'] );
    $this->assertEqual( 'Multiple Save 2 Title', $article_2['Article']['title'] );
    $this->assertEqual( 'N', $article_2['Article']['published'] );
    
    $this->assertEqual( 3, $article_3['Article']['user_id'] );
    $this->assertEqual( 'Multiple Save 3 Title', $article_3['Article']['title'] );
    $this->assertEqual( 'Y', $article_3['Article']['published'] );
    
    # Verify that no delta records were created.
    $this->assertTrue( empty( $changes[0]['ChangeDelta'] ) );
    $this->assertTrue( empty( $changes[1]['ChangeDelta'] ) );
    $this->assertTrue( empty( $changes[2]['ChangeDelta'] ) );
  }
  
  /**
   * Test editing an existing record.
   *
   * @todo  Test change to ignored field
   * @todo  Test HABTM save
   */
  public function testEdit() {
    $this->Change      = ClassRegistry::init( 'Change' );
    $this->ChangeDelta = ClassRegistry::init( 'ChangeDelta' );
    
    $new_article = array(
      'Article' => array(
        'user_id'       => 1,
        'author_id'     => 1,
        'title'         => 'First Test Article', 
        'body'          => 'First Test Article Body',
        'ignored_field' => 1,
        'published'     => 'N', 
      ),
    );
    
    # TEST SAVE WITH SINGLE PROPERTY UPDATE
    
    $this->Article->save( $new_article );
    $this->Article->saveField( 'title', 'First Test Article (Edited)' );
    
    $change_records = $this->Change->find(
      'all',
      array(
        'recursive' => 0,
        'conditions' => array(
          'Change.model' => 'Article',
          'Change.entity_id' => $this->Article->getLastInsertId()
        )
      )
    );
    $delta_records = $this->ChangeDelta->find(
      'all',
      array(
        'recursive' => -1,
        'conditions' => array( 'ChangeDelta.change_id' => Set::extract( '/Change/id', $change_records ) ),
      )
    );
    
    $create_change = Set::extract( '/Change[event=CREATE]', $change_records );
    $update_change = Set::extract( '/Change[event=EDIT]', $change_records );
    
    # There should be 1 CREATE and 1 EDIT record
    $this->assertEqual( 2, count( $change_records ) );
    
    # There should be one change record for each event.
    $this->assertEqual( 1, count( $create_change ) );
    $this->assertEqual( 1, count( $update_change ) );
    
    # Only one property was changed
    $this->assertEqual( 1, count( $delta_records ) );
    
    $delta = array_shift( $delta_records );
    $this->assertEqual( 'First Test Article', $delta['ChangeDelta']['old_value'] );
    $this->assertEqual( 'First Test Article (Edited)', $delta['ChangeDelta']['new_value'] );

    # TEST UPDATE OF MULTIPLE PROPERTIES
    # Pause to simulate a gap between edits
    # This also allows us to retrieve the last edit for the next set
    # of tests.
    $this->Article->create(); # Clear the article id so we get  a new record.
    $new_article = array(
      'Article' => array(
        'user_id'       => 1,
        'author_id'     => 1,
        'title'         => 'Second Test Article', 
        'body'          => 'Second Test Article Body',
        'ignored_field' => 1,
        'published'     => 'N', 
      ),
    );
    $this->Article->save( $new_article );

    $updated_article = array(
      'Article' => array(
        'user_id'       => 1,
        'author_id'     => 1,
        'title'         => 'Second Test Article (Newly Edited)', 
        'body'          => 'Second Test Article Body (Also Edited)',
        'ignored_field' => 0,
        'published'     => 'Y', 
      ),
    );
    $this->Article->save( $updated_article );
    
    $last_change = $this->Change->find(
      'first',
      array(
        'contain'    => array( 'ChangeDelta' ),
        'conditions' => array(
          'Change.event'     => 'EDIT',
          'Change.model'     => 'Article',
          'Change.entity_id' => $this->Article->id
        ),
        'order' => 'Change.created DESC',
      )
    );

    # There are 4 changes, but one to an ignored field
    $this->assertEqual( 3, count( $last_change['ChangeDelta'] ) );
    $result = Set::extract( '/ChangeDelta[property_name=title]/old_value', $last_change );
    $this->assertEqual( 'Second Test Article', array_shift( $result ) );

    $result = Set::extract( '/ChangeDelta[property_name=title]/new_value', $last_change );
    $this->assertEqual( 'Second Test Article (Newly Edited)', array_shift( $result ) );

    $result = Set::extract( '/ChangeDelta[property_name=body]/old_value', $last_change );
    $this->assertEqual( 'Second Test Article Body', array_shift( $result ) );

    $result = Set::extract( '/ChangeDelta[property_name=body]/new_value', $last_change );
    $this->assertEqual( 'Second Test Article Body (Also Edited)', array_shift( $result ) );

    $result = Set::extract( '/ChangeDelta[property_name=published]/old_value', $last_change );
    $this->assertEqual( 'N', array_shift( $result ) );

    $result = Set::extract( '/ChangeDelta[property_name=published]/new_value', $last_change );
    $this->assertEqual( 'Y', array_shift( $result ) );

    # No delta should be reported against the ignored field.
    $this->assertIdentical( array(), Set::extract( '/ChangeDelta[property_name=ignored_field]', $last_change ) );
  }
  
  public function testIgnoredField() {
    $this->Change      = ClassRegistry::init( 'Change' );
    $this->ChangeDelta = ClassRegistry::init( 'ChangeDelta' );
    
    $new_article = array(
      'Article' => array(
        'user_id'       => 1,
        'author_id'     => 1,
        'title'         => 'First Test Article', 
        'body'          => 'First Test Article Body',
        'ignored_field' => 1,
        'published'     => 'N', 
      ),
    );
    
    # TEST NO AUDIT RECORD IF ONLY CHANGE IS IGNORED FIELD
    
    $this->Article->save( $new_article );
    $this->Article->saveField( 'ignored_field', '5' );
    
    $last_change = $this->Change->find(
      'count',
      array(
        'contain'    => array( 'ChangeDelta' ),
        'conditions' => array(
          'Change.event'     => 'EDIT',
          'Change.model'     => 'Article',
          'Change.entity_id' => $this->Article->id
        ),
        'order' => 'Change.created DESC',
      )
    );
    
    $this->assertEqual( 0, $last_change );
  }

  public function testDelete() {
    $this->Change      = ClassRegistry::init( 'Change' );
    $this->ChangeDelta = ClassRegistry::init( 'ChangeDelta' );
    $article = $this->Article->find(
      'first',
      array(
        'contain' => false,
        'order'   => array( 'rand()' ),
      )
    );
    
    $id = $article['Article']['id'];
    
    $this->Article->delete( $id );
    
    $last_change = $this->Change->find(
      'all',
      array(
        //'contain'    => array( 'ChangeDelta' ), <-- What does this solve?
        'conditions' => array(
          'Change.event'     => 'DELETE',
          'Change.model'     => 'Article',
          'Change.entity_id' => $id,
        ),
        'order' => 'Change.created DESC',
      )
    );
    
    $this->assertEqual( 1, count( $last_change ) );
  }
}