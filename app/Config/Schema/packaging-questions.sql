
INSERT INTO `audit_question_categories`(id,`sort_order`, `name`,  `audit_question_category_type_id`, `supplier`)  VALUES (101,1,'Legislation',101,0);
INSERT INTO `audit_question_categories`(id,`sort_order`, `name`,  `audit_question_category_type_id`, `supplier`)  VALUES (102,2,'Obligation',101,0);
INSERT INTO `audit_question_categories`(id,`sort_order`, `name`,  `audit_question_category_type_id`, `supplier`)  VALUES (103,3,'Compliance And Reporting',101,0);
INSERT INTO `audit_question_categories`(id,`sort_order`, `name`,  `audit_question_category_type_id`, `supplier`)  VALUES (104,4,'Labelling',101,0);
INSERT INTO `audit_question_categories`(id,`sort_order`, `name`,  `audit_question_category_type_id`, `supplier`)  VALUES (105,5,'Other',101,0);



INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1001,1,'Legislative Reference',101,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1002,2,'Obligated Parties',102,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1003,3,'Detailed definition of the obligated party, especially regards to imports
',102,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1004,4,' Can foreign companies comply
',102,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1005,5,'obligted by article',102,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1006,6,'Obligation for online retailers from foreign countries
',102,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1007,7,'Is there an official register
',103,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1008,8,'Compliance schemes
',103,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1009,9,'Competitive compliance schemes
',103,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1010,10,'Thresholds',102,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1011,11,'Affected types of packagings (primary, secondary, tertiary)
',102,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1012,12,'Frequency of Reporting/ Payments
',103,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1013,13,'Reporting Requirements
',103,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1014,14,'Label Requirements
',104,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1015,15,'Green Dot
',104,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1016,16,'Non-Compliance
',103,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1017,17,'Methodologies, standards for the assessment of recyclability
',105,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1018,18,'Sources
',105,0,101);
INSERT INTO `audit_questions`(`id`, `reference`, `question`, `audit_question_category_id`, `supplier`, `audit_question_category_type_id`) VALUES (1019,19,'Last Updated',101,0,101);





update audit_question_responses set consumer_note = 'The Environmental code Articles R543-42 onwards' where id=19202;
2Branch, distributor, importer
3" - Those rebranding or importing packaged goods for sale onto the French market
- Those selling / distributing packaged goods.
- Companies which sell directly to consumers via distance communication eg. online
"
4Yes
5
6
7Yes
8Citeo for all household packagings
9Yes
10No Threshold
11"Household Packaging"
12Annual
13"Obligated placers of household packaging on the market have two options: 
(1) joining an existing monopolistic scheme 
(2) establishing a private packaging return scheme. 

The existing monopolistic scheme is divided in three: Packaging which was in direct contact with chemical products - Eco-DDS. 
Packaging of wine and spirits producers is to be licensed at Adelphe
All other packaging - Eco-Emballage"
14Triman and Sorting instructions
15Not mandatory
16"There are sensitive penalties when the legal provisions and obligations are violated
Possible prison sentences lasting several years."


175/6/2021





