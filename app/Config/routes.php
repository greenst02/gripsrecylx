<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

Router::mapResources('users');
Router::mapResources('companies');
Router::mapResources('company_brands');
Router::mapResources('company_sites');
Router::mapResources('company_contacts');
Router::mapResources('company_documents');
Router::mapResources('company_suppliers');
Router::mapResources('company_audits');

Router::mapResources('company_document_types');
Router::mapResources('industry_sectors');
Router::mapResources('industry_types');
Router::mapResources('australian_states');
Router::mapResources('australian_states');

Router::mapResources('audit_questions');
Router::mapResources('audit_to_questions');
Router::mapResources('audit_question_categories');
Router::mapResources('audit_question_responses');
Router::mapResources('company_enabled_statuses');
Router::mapResources('user_enabled_statuses');
Router::mapResources('changes');
Router::mapResources('audit_to_documents');
Router::mapResources('help_sections');
Router::mapResources('batch_emails');

/*
 * added route to get documents associated with a specified audit_id
 */
//Router::connect('/company_documents/audit_docs/:id',
//                array('controller' => 'CompanyDocuments', 'action' => 'audit_documents'),
//                array('id' => '[0-9]+')
//        );

/*
 * added route to get documents associated with a specified audit_id
 */
//Router::connect('/company_documents/audit_images/:id',
//                array('controller' => 'CompanyDocuments', 'action' => 'audit_images'),
//                array('id' => '[0-9]+')
//        );
/*
 * added route to get documents associated with a specified audit_id
 */
//Router::connect('/company_documents/index_images/:id',
//                array('controller' => 'CompanyDocuments', 'action' => 'index_images'),
//                array('id' => '[0-9]+')
//        );


//Router::parseExtensions('json');


/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
