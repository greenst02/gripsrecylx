<?php

App::uses('ExceptionRenderer', 'Error');

class AppExceptionRenderer extends ExceptionRenderer {

    public function badRequest($error) {
        $this->controller->beforeFilter();
        $this->controller->response->type('json');
        $this->controller->response->statusCode(400);
        $this->controller->response->body(json_encode(array('flash' => 'Bad Request')));
        $this->controller->response->send();
    }
    
    public function unauthorized($error) {
        $this->controller->beforeFilter();
        $this->controller->response->type('json');
        $this->controller->response->statusCode(401);
        $this->controller->response->body(json_encode(array('flash' => 'Unauthorized')));
        $this->controller->response->send();
    }
    
    public function forbidden($error) {
        $this->controller->beforeFilter();
        $this->controller->response->type('json');
        $this->controller->response->statusCode(403);
        $this->controller->response->body(json_encode(array('flash' => 'Forbidden')));
        $this->controller->response->send();
    }

    public function notFound($error) {
        $this->controller->beforeFilter();
        $this->controller->response->type('json');
        $this->controller->response->statusCode(404);
        $this->controller->response->body(json_encode(array('flash' => 'Not Found')));
        $this->controller->response->send();
    }

    public function methodNotAllowed($error) {
        $this->controller->beforeFilter();
        $this->controller->response->type('json');
        $this->controller->response->statusCode(405);
        $this->controller->response->body(json_encode(array('flash' => 'Method Not Allowed')));
        $this->controller->response->send();
    }

    public function internalError($error) {
        $this->controller->beforeFilter();
        $this->controller->response->type('json');
        $this->controller->response->statusCode(500);
        $this->controller->response->body(json_encode(array('flash' => 'Internal Server Error')));
        $this->controller->response->send();
    }

    public function notImplemented($error) {
        $this->controller->beforeFilter();
        $this->controller->response->type('json');
        $this->controller->response->statusCode(501);
        $this->controller->response->body(json_encode(array('flash' => 'Not Found')));
        $this->controller->response->send();
    }

    /* ------------------------------------------------------------------------- */
    /* -- Other
      /*------------------------------------------------------------------------- */

    public function missingController($error) {
        $this->notFound($error);
    }

    public function missingAction($error) {
        $this->notFound($error);
    }

    public function missingView($error) {
        $this->notFound($error);
    }

    public function missingLayout($error) {
        $this->internalError($error);
    }

    public function missingHelper($error) {
        $this->internalError($error);
    }

    public function missingBehavior($error) {
        $this->internalError($error);
    }

    public function missingComponent($error) {
        $this->internalError($error);
    }

    public function missingTask($error) {
        $this->internalError($error);
    }

    public function missingShell($error) {
        $this->internalError($error);
    }

    public function missingShellMethod($error) {
        $this->internalError($error);
    }

    public function missingDatabase($error) {
        $this->internalError($error);
    }

    public function missingConnection($error) {
        $this->internalError($error);
    }

    public function missingTable($error) {
        $this->internalError($error);
    }

    public function privateAction($error) {
        $this->internalError($error);
    }

}

